export class SearchModel {
    Keyword: string = '';
    CountryID: string;
    CategoryIDs: string;
    OptionIDs: string;
    filters: boolean;
}

export class EmailModel {
    FullName: string;
    Subject: string;
    ProductID: string;
    ProductName:string;
    Message: string;
    PhoneNumber: string;
    EmailAddress: string;
    SerialNumber: string;
}

export class ModalData {
    title: string = '';
    content: string = '';
}

export class ComparedProduct {
    ScreenSize: string = '';
    Resolution: string = '';
    PanelType: string = '';
    Brightness: string = '';
    SmartTV: string = '';
    ProcessType: string = '';
    TotalPictureQuality: string = '';
    WideColorGamut: string = '';
    HDR: string = '';
    DolbyVisionHDR: string = '';
    MEMC: string = '';
    UHDUpscaler: string = '';
    PictureModes: string = '';
}
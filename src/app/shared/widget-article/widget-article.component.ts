import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-widget-article',
  templateUrl: './widget-article.component.html',
  styleUrls: ['./widget-article.component.scss']
})
export class WidgetArticleComponent implements OnInit {
  @Input() style: string;
  @Input() data: any;
  @Input() themeColor: string = '';
  constructor() { }

  ngOnInit() {}

}

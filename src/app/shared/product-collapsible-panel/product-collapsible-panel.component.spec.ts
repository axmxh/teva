import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCollapsiblePanelComponent } from './product-collapsible-panel.component';

describe('ProductCollapsiblePanelComponent', () => {
  let component: ProductCollapsiblePanelComponent;
  let fixture: ComponentFixture<ProductCollapsiblePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCollapsiblePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCollapsiblePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

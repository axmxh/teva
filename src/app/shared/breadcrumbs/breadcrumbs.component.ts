import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  @Input() data: any;
  productPage:boolean;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
      this.route.queryParams.subscribe((params)=>{
        if (params.catId) this.productPage = true; else this.productPage = false;
      })
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselComponent } from './carousel/carousel.component';
import { WidgetArticleComponent } from './widget-article/widget-article.component';
import { RouterModule } from '@angular/router';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { ProductCollapsiblePanelComponent } from './product-collapsible-panel/product-collapsible-panel.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SafePipe } from '../pipes/sanitize.pipe';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';

@NgModule({
  declarations: [CarouselComponent, WidgetArticleComponent, BreadcrumbsComponent, ProductCollapsiblePanelComponent, SpinnerComponent, SafePipe],
  imports: [
    CommonModule,
    RouterModule,
    JwSocialButtonsModule
  ],
  exports: [
    CarouselComponent,
    WidgetArticleComponent,
    BreadcrumbsComponent,
    ProductCollapsiblePanelComponent,
    SpinnerComponent,
    SafePipe,
    JwSocialButtonsModule
  ],
  providers: [SafePipe]
})
export class SharedModule { }

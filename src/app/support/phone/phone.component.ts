import { Component, OnInit } from '@angular/core';
import { SupportService } from '../../services/support.service';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.scss']
})
export class PhoneComponent implements OnInit {
  loading: boolean;
  contactInfo: any[] = [];
  lat: number;
  lng: number;
  location: any;
  constructor(private supportService: SupportService) { }

  ngOnInit() {
    this.getInfo();
  }



  getInfo() {
    this.loading = true;
    this.supportService.getContactInfo().subscribe((info: any) => {
      this.loading = false;
      this.contactInfo = info.Data;
      this.location = info.Data[3].Value.split(",");
      this.lat = +this.location[0];
      this.lng = +this.location[1];
    })
  }

}

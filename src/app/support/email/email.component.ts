import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { EmailModel } from '../../shared/shared.models';
import { SupportService } from '../../services/support.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {
  emailFormModel: EmailModel = new EmailModel();
  @ViewChild('myInput') myInputVariable: ElementRef;

  file: FormData;
  loading: boolean;
  onErr: boolean;
  errors: any[] = [];
  successMsg: string;
  onSuccess: boolean;
  fileToUpload: File = null;
  uploadedFile: FormData;
  modelsList: any[] = [];
  onCompleteList: boolean;
  noResult: boolean;
  constructor(private supportService: SupportService) { }

  ngOnInit() {
  }
  sendEmail(emailForm: NgForm) {
    this.onErr = false;
    this.onSuccess = false;
    this.loading = true;
    let obj = {
      "FullName": this.emailFormModel.FullName,
      "Subject": this.emailFormModel.Subject,
      "ProductID": this.emailFormModel.ProductID,
      "Message": this.emailFormModel.Message,
      "PhoneNumber": this.emailFormModel.PhoneNumber,
      "EmailAddress": this.emailFormModel.EmailAddress,
      "SerialNumber": this.emailFormModel.SerialNumber
    }
  
    this.supportService.sendEmail(JSON.stringify(obj), this.fileToUpload).subscribe((data: any) => {
      if (data.Status) {
        this.onSuccess = true;
        this.successMsg = "Email has been sent successfully!"
        this.loading = false;
        emailForm.reset();
        this.fileToUpload = null;
        this.myInputVariable.nativeElement.value = "";
      } else {
        this.onErr = true;
        this.loading = false;
        this.errors = data['Exceptions'];
        this.fileToUpload = null;
        this.myInputVariable.nativeElement.value = "";
      }
    }, (err) => {
      this.loading = false;
      this.onErr = true;
      this.errors = err.statusText;
    })
  }

  handleFileInput(e) {
    this.fileToUpload = e.target.files.item(0);

    let elem = e.target || e.srcElement;

    this.uploadedFile = new FormData();
    this.uploadedFile.append('file', elem.files[0]);
  }


  onModelEnter = () => {
    this.onCompleteList = true;
    this.noResult = false;

    this.supportService.getProductId(this.emailFormModel.ProductName).subscribe((products: any) => {
      this.modelsList = products.Data;
      if (products.Data.length == 0) {
        this.noResult = true;
      }
    })
  }

  debounceTimeOut;
  debounceTime(fn, time) {
    clearTimeout(this.debounceTimeOut);
    this.debounceTimeOut = setTimeout(fn, time);
  }

  onSelect(product?) {
    this.emailFormModel.ProductID = product.ID;
    this.emailFormModel.ProductName = product.Name;
    setTimeout(() => {
      this.onCompleteList = false;
    }, 100);
  }

  resetAutoComplete(type?) {
    if (type === 'blur') {
      setTimeout(() => {
        this.onCompleteList = false;
      }, 200);
    } else {
      this.emailFormModel.ProductID = '';
      this.emailFormModel.ProductName = '';
      this.onCompleteList = false;
      this.noResult = false;
    }
  }
}

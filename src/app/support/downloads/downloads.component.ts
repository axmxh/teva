import { Component, OnInit } from '@angular/core';
import { SupportService } from '../../services/support.service';

@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.scss']
})
export class DownloadsComponent implements OnInit {
  loading: boolean;
  downloadName: string = '';
  onErr: boolean;
  downloads: any[] = [];
  noResult:boolean;
  constructor(private supportService: SupportService) { }

  ngOnInit() {
    this.getDownloads();
  }

  getDownloads = () => {
    this.downloads = [];
    this.onErr = false;
    this.loading = true;
    this.noResult = false;
    this.supportService.getDownloads(this.downloadName).subscribe((downloads: any) => {
      this.loading = false;
      this.downloads = downloads.Data;
      if (downloads.Data.length == 0) this.noResult = true;
    }, (err) => {
      this.loading = false;
      this.onErr = true;
    })
  }

  debounceTimeOut;
  debounceTime(fn, time) {
    clearTimeout(this.debounceTimeOut);
    this.debounceTimeOut = setTimeout(fn, time);
  }

}

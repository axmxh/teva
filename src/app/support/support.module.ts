import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SupportRoutingModule } from './support-routing.module';
import { FaqsComponent } from './faqs/faqs.component';
import { SupportComponent } from './support.component';
import { VideosComponent } from './videos/videos.component';
import { DownloadsComponent } from './downloads/downloads.component';
import { WarrentyComponent } from './warrenty/warrenty.component';
import { EmailComponent } from './email/email.component';
import { PhoneComponent } from './phone/phone.component';
import { SupportService } from '../services/support.service';
import { SharedModule } from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [FaqsComponent, SupportComponent, VideosComponent, DownloadsComponent, WarrentyComponent, EmailComponent, PhoneComponent],
  imports: [
    CommonModule,
    SupportRoutingModule,
    FormsModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74',
      language: 'en'
    })
  ],
  providers: [SupportService]
})
export class SupportModule { }
// AIzaSyB2tKpENK9xz4pEGYN4yGyeGS3NgAiCRIY

/// AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74
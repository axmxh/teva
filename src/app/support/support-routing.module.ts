import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqsComponent } from './faqs/faqs.component';
import { SupportComponent } from './support.component';
import { PhoneComponent } from './phone/phone.component';
import { EmailComponent } from './email/email.component';
import { WarrentyComponent } from './warrenty/warrenty.component';
import { DownloadsComponent } from './downloads/downloads.component';
import { VideosComponent } from './videos/videos.component';

const routes: Routes = [
  {
    path: '', component: SupportComponent,
    children: [
      { path: 'faqs', component: FaqsComponent },
      { path: 'videos', component: VideosComponent },
      { path: 'downloads', component: DownloadsComponent },
      { path: 'warrenty', component: WarrentyComponent },
      { path: 'email', component: EmailComponent },
      { path: 'phone', component: PhoneComponent },
      { path: '', redirectTo: 'faqs', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrentyComponent } from './warrenty.component';

describe('WarrentyComponent', () => {
  let component: WarrentyComponent;
  let fixture: ComponentFixture<WarrentyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrentyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrentyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

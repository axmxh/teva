import { Component, OnInit } from '@angular/core';
import { SupportService } from '../../services/support.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {
  mostPopular: any[] = [];
  faqs: any[] = [];
  loading: boolean;
  loading1: boolean;
  faqName: string = '';
  onErr: boolean;
  noResult: boolean;
  constructor(private supportService: SupportService) { }

  ngOnInit() {
    this.getFaqs();
    this.getMostPopularFaqs();
  }


  getMostPopularFaqs() {
    this.loading1 = true;
    this.supportService.getMostPopularFAQs().subscribe((data: any) => {
      this.mostPopular = data.Data;
      this.loading1 = false;
    })
  }

  getFaqs = () => {
    this.faqs = [];
    this.onErr = false;
    this.loading = true;
    this.noResult = false;
    this.supportService.getFaqs(this.faqName).subscribe((faqs: any) => {
      this.loading = false;
      this.faqs = faqs.Data;
      if (faqs.Data.length == 0) this.noResult = true;
    }, (err) => {
      this.loading = false;
      this.onErr = true;
    })
  }

  debounceTimeOut;
  debounceTime(fn, time) {
    clearTimeout(this.debounceTimeOut);
    this.debounceTimeOut = setTimeout(fn, time);
  }

}

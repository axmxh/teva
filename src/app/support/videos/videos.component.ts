import { Component, OnInit } from '@angular/core';
import { SupportService } from '../../services/support.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  loading: boolean;
  loading1: boolean;
  videoName: string = '';
  onErr: boolean;
  noResult: boolean;
  mostPopular: any[] = [
    {
      title: 'Setting up your smart TV’s Internet connection.',
      subTitle: 'Network connectivity',
      views: '205k',
      url: 'https://player.vimeo.com/video/121653965'
    },
    {
      title: 'Setting up your smart TV’s wireless display connection.',
      subTitle: 'Operations',
      views: '229k',
      url: 'https://player.vimeo.com/video/2259254'
    },
    {
      title: 'Setting up and using Freeview Play catch-up services.',
      subTitle: 'Operations',
      views: '205k',
      url: 'https://player.vimeo.com/video/24905664'
    }
  ]
  videos: any[] = [
    {
      title: 'Setting up your smart TV’s Internet connection.',
      subTitle: 'Network connectivity',
      views: '205k',
      url: 'https://player.vimeo.com/video/121653965'
    },
    {
      title: 'Setting up your smart TV’s wireless display connection.',
      subTitle: 'Operations',
      views: '229k',
      url: 'https://player.vimeo.com/video/2259254'
    },
    {
      title: 'Setting up and using Freeview Play catch-up services.',
      subTitle: 'Operations',
      views: '205k',
      url: 'https://player.vimeo.com/video/24905664'
    },
    {
      title: 'Setting up your TV to enhance the sound performance.',
      subTitle: 'TV performance enhancement',
      views: '205k',
      url: 'https://player.vimeo.com/video/81097964'
    },
  ]
  constructor(private supportService: SupportService) { }

  ngOnInit() {
    this.getMostPopularVideos();
    this.getVideos();
  }


  getMostPopularVideos() {
    this.loading1 = true;
    this.supportService.getMostPopularVideos().subscribe((data: any) => {
      this.mostPopular = data.Data;
      this.loading1 = false;
    })
  }

  getVideos = () => {
    this.videos = [];
    this.onErr = false;
    this.loading = true;
    this.noResult = false;
    this.supportService.getVideos(this.videoName).subscribe((videos: any) => {
      this.loading = false;
      this.videos = videos.Data;
      if (videos.Data.length == 0) this.noResult = true;
    }, (err) => {
      this.loading = false;
      this.onErr = true;
    })
  }

  debounceTimeOut;
  debounceTime(fn, time) {
    clearTimeout(this.debounceTimeOut);
    this.debounceTimeOut = setTimeout(fn, time);
  }

}

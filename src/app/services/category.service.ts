import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ProxyUrl } from '../api';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': "*",
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    'Access-Control-Allow-Headers': "*",
    'Access-Control-Allow-Credentials': 'true',
  })
  constructor(private http: HttpClient, private proxy: ProxyUrl) { }

  // get category items
  getCategoryItems(params) {
    return this.http.get(this.proxy.api + 'Product/GetProductsByCategory?', { headers: this.headers, params: params });
  }

  // get product details
  getProductDetails(params) {
    return this.http.get(this.proxy.api + 'Product/GetProductDetails?', { headers: this.headers, params: params });
  }

  // get product downloads
  getProductDownloads(params) {
    return this.http.get(this.proxy.api + 'Product/GetProductDownloads?', { headers: this.headers, params: params });
  }

  // get product specs
  getProductSpecs(params) {
    return this.http.get(this.proxy.api + 'Product/GetProductSpecs?', { headers: this.headers, params: params });
  }

  // get product features
  getProductFeatures(params) {
    return this.http.get(this.proxy.api + 'Product/GetProductFeatures?', { headers: this.headers, params: params });
  }

  // search products
  searchProducts(params) {
    return this.http.get(this.proxy.api + 'Product/SearchProducts?', { headers: this.headers, params: params });
  }

  // get search attribute
  getAttributes(keyword = '') {
    return this.http.get(this.proxy.api + 'Product/GetAttributes?Keyword=' + keyword, { headers: this.headers })
  }

  // get product to compare
  getProductToCompare(params) {
    return this.http.get(this.proxy.api + 'Product/GetProductData?', { headers: this.headers, params: params });
  }
}

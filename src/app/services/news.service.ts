import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ProxyUrl } from '../api';
@Injectable({
    providedIn: 'root'
})
export class NewsService {
    private headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
        'Access-Control-Allow-Headers': "*",
        'Access-Control-Allow-Credentials': 'true',
    })
    constructor(private http: HttpClient, private proxy: ProxyUrl) { }

    // get news
    getNews(params) {
        return this.http.get(this.proxy.api + 'News/GetNews', { headers: this.headers, params: params });
    }
    // get latest news
    getLatestNews(params) {
        return this.http.get(this.proxy.api + 'News/GetLatestNews', { headers: this.headers, params: params });
    }
    // get news details
    getNewsDetails(params) {
        return this.http.get(this.proxy.api + 'News/GetNewsDetails', { headers: this.headers, params: params });
    }

}

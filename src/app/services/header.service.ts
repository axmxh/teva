import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ProxyUrl } from '../api';
@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': "*",
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    'Access-Control-Allow-Headers': "*",
    'Access-Control-Allow-Credentials': 'true',
  })
  constructor(private http: HttpClient, private proxy: ProxyUrl) { }

  // get countries
  getCountries() {
    return this.http.get(this.proxy.api + 'Country/GetCountry', { headers: this.headers });
  }
  // get Footer
  getFooterLinks() {
    return this.http.get(this.proxy.api + 'Content/GetStaticLinks', { headers: this.headers });
  }
  // get Static Links Data
  getStaticLinksData(id) {
    return this.http.get(this.proxy.api + 'Content/GetContent?ID=' + id, { headers: this.headers });
  }
  // get categories
  getCategories() {
    return this.http.get(this.proxy.api + 'Category/GetCategory', { headers: this.headers });
  }
  // get stores
  getStores(ip, keyword = '') {
    return this.http.get(this.proxy.api + 'Store/GetStores?LocalIP=' + ip + '&Keyword=' + keyword, { headers: this.headers });
  }

  getSocialLinks(){
    return this.http.get(this.proxy.api + 'Content/GetSocialAccounts', { headers: this.headers });
  }

}

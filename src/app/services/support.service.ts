import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ProxyUrl } from '../api';
@Injectable({
  providedIn: 'root'
})
export class SupportService {
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': "*",
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    'Access-Control-Allow-Headers': "*",
    'Access-Control-Allow-Credentials': 'true',
  })
  private headersToUpload = new HttpHeaders({
    'Content-Type': 'multipart/form-data',
    'Access-Control-Allow-Origin': "*",
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    'Access-Control-Allow-Headers': "*",
    'Access-Control-Allow-Credentials': 'true',
  })
  constructor(private http: HttpClient, private proxy: ProxyUrl) { }

  // get downloads
  getDownloads(keyword = '') {
    return this.http.get(this.proxy.api + 'Support/GetDownloads?Keyword=' + keyword, { headers: this.headers });
  }

  // get most popular faqs
  getMostPopularFAQs() {
    return this.http.get(this.proxy.api + 'Support/GetMostPopularFAQs', { headers: this.headers });
  }

  // get faqs
  getFaqs(keyword = '') {
    return this.http.get(this.proxy.api + 'Support/GetFAQs?Keyword=' + keyword, { headers: this.headers });
  }

  // get contact info
  getContactInfo() {
    return this.http.get(this.proxy.api + 'Support/GetContactInfo', { headers: this.headers });
  }

  // get videos
  getVideos(keyword = '') {
    return this.http.get(this.proxy.api + 'Support/GetVideos?Keyword=' + keyword, { headers: this.headers });
  }

  // get most popular videos
  getMostPopularVideos() {
    return this.http.get(this.proxy.api + 'Support/GetMostPopularVideos', { headers: this.headers });
  }

  // send email
  sendEmail(request, files) {
    return this.http.post(this.proxy.api + 'Emailing/SaveEmail', { request, files }, { headers: this.headers });
  }

  // get product id
  getProductId(keyword = '') {
    return this.http.get(this.proxy.api + 'Emailing/GetProducts?Keyword=' + keyword, { headers: this.headers })
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ProxyUrl } from '../api';
@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': "*",
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    'Access-Control-Allow-Headers': "*",
    'Access-Control-Allow-Credentials': 'true',
  })
  private headersToUpload = new HttpHeaders({
    'Content-Type': 'multipart/form-data;boundary=' + Math.random()
  })
  constructor(private http: HttpClient, private proxy: ProxyUrl) { }

  // get home blocks
  getHomeBlocks() {
    return this.http.get(this.proxy.api + 'Home/GetHomeBlocks', { headers: this.headers });
  }

  // become a distributor
  SaveDistributor(request, files) {
    return this.http.post(this.proxy.api + 'Distributor/SaveDistributor', { request, files }, { headers: this.headers });
  }

}

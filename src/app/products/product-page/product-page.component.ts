import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  title: string = 'OLED TV 6';
  galleryImages: any[] = [];
  tabs: any[] = [];
  productId: string;
  loading: boolean;
  productDetails: any;
  productDescriptionList: any[] = [];
  breadcrumbsInfo = {
    'background': 'assets/images/breadcrubs-bg.jpg',
    'paths': [
      { 'name': 'Home', 'url': '/' },
      { 'name': '', 'url': '/products/product-list', 'params': {} },
      { 'name': '', 'url': '/products/product-page', 'params': {} }
    ]
  };
  keyword: string;
  showCompare: boolean;
  catID: number;
  constructor(private catService: CategoryService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.productId = params.ProductID;
      this.catID = params.catId;
    })
    if (!this.productId) this.router.navigate(['/page-not-found']);
    this.getProductDetails();
  }

  getProductDetails() {
    this.loading = true;
    let params = {
      'ProductID': this.productId
    };
    this.galleryImages = [];
    this.catService.getProductDetails(params).subscribe((data) => {
      this.loading = false;
      this.productDetails = data['Data'];
      this.breadcrumbsInfo.paths[1] = { 'name': data['Data'].CategoryName, 'url': '/products/product-list', 'params': { id: this.catID } };
      this.breadcrumbsInfo.paths[2] = { 'name': data['Data'].ProductName, 'url': '/products/product', 'params': { ProductID: this.productId , catId: this.catID } };
      this.productDescriptionList = data['Data'].ShortDescription.split(" - ");
      if (data['Data'].ProductImages) {
        data['Data'].ProductImages.map((img) => {
          let image = {
            small: img,
            medium: img,
            big: img
          };
          this.galleryImages.push(image);
        });
      };
    });
  };


  onSelectCompare() {
    this.showCompare = !this.showCompare;
  }

  onSelectSize(ProductID) {
    this.productId = ProductID;
    this.getProductDetails();
    this.router.navigate(['products/product'], { queryParams: { ProductID: this.productId } });
  }
}

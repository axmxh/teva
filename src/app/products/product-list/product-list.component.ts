import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../../services/category.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  productId: number;
  support = {
    title: "Support",
    description: "Access and download more product information and find quick and easy troubleshooting advice.",
    url: "/support",
    urlTxt: "Get Support",
    image: "assets/images/support-image.jpg"
  };
  loading: boolean;
  noProducts: boolean;
  breadcrumbsInfo = {
    'background': 'assets/images/4-k-smart-t-vs-header.jpg',
    'paths': [
      { 'name': 'Home', 'url': '/' },
      { 'name': '', 'url': '/products/product-list?id=', 'params': {} }
    ],
    'description': ''
  }

  products: any[] = []
  constructor(private route: ActivatedRoute, private catService: CategoryService, @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.productId = params["id"];
      this.getCatItems();
    });
  }

  getCatItems() {
    let localStore;
    if (isPlatformBrowser(this.platformId)) {
      localStore = localStorage.getItem('countryID')
    }
    this.loading = true;
    this.noProducts = false;
    let params = {
      CategoryID: this.productId,
      CountryID: localStore
    }
    this.catService.getCategoryItems(params).subscribe((items: any) => {
      this.loading = false;
      if (items.Data.length == 0) this.noProducts = true;
      this.products = items.Data;
      if (items.CategoryImage != null) this.breadcrumbsInfo.background = items.CategoryImage;
      this.breadcrumbsInfo.paths[1] = { 'name': items.CategoryName, 'url': '/products/product-list', params: { id: this.productId } };

    })
  };


}

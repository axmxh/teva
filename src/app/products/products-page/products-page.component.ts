import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { CategoryService } from '../../services/category.service';
import { ActivatedRoute } from '@angular/router';
import { SearchModel } from '../../shared/shared.models';
@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit {
  keyword: string;
  tags = [
    { id: 1, 'name': '4K Smart' },
    { id: 2, 'name': '49”' }
  ]
  products = [];
  filters: any[] = [];
  loading: boolean;
  noResultsFound: boolean;

  constructor(private catService: CategoryService, private route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.products = [];
      this.keyword = params['Keyword'];
      this.getFilters();
      this.getSearchResults();
    });
  };

  removeTag(index) {
    this.tags.splice(index, 1);
  };

  clearAllTags() {
    this.tags = [];
  };

  getSearchResults(e = new SearchModel()) {
    if (isPlatformBrowser(this.platformId)) {
      e.CountryID = localStorage.getItem('countryID')
    }

    this.products = [];
    this.loading = true;
    this.noResultsFound = false;

    if (e.filters) {
      this.catService.searchProducts(e).subscribe((results) => {
        this.loading = false;
        this.products = results['Data'];
        if (results['Data'].length == 0) {
          this.noResultsFound = true;
        }
      });
    } else {
      let params: SearchModel = new SearchModel();
      params.Keyword = this.route.snapshot.queryParams['Keyword'];
      params.CountryID = this.route.snapshot.queryParams['CountryID'] || e.CountryID;

      this.catService.searchProducts(params).subscribe((results) => {
        this.loading = false;
        this.products = results['Data'];
        if (results['Data'].length == 0) {
          this.noResultsFound = true;
        }
      });
    }
  };

  getFilters() {
    this.catService.getAttributes(this.keyword).subscribe((filters) => {
      this.filters = filters['Data'];
    })
  };


}

import { Component, OnInit, Input, Output, EventEmitter, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { SearchModel } from '../../shared/shared.models';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-products-filters',
  templateUrl: './products-filters.component.html',
  styleUrls: ['./products-filters.component.scss']
})
export class ProductsFiltersComponent implements OnInit {
  @Input() filters: any[] = [];
  @Output() emitFilters: any = new EventEmitter();
  loading: boolean;
  optionIDs: string[] = [];
  constructor(private route: ActivatedRoute, @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
  }

  onFilter(id) {
    let localStore;
    if (isPlatformBrowser(this.platformId)) {
      localStore = localStorage.getItem('countryID')
    }
    if (this.optionIDs.includes(id)) {
      let index = this.optionIDs.indexOf(id);
      this.optionIDs.splice(index, 1)
    } else {
      this.optionIDs.push(id);
    }
    let params: SearchModel = new SearchModel();
    params.CountryID = localStore;
    params.Keyword = this.route.snapshot.queryParams['Keyword'];
    params.OptionIDs = this.optionIDs.toString();
    params.filters = true;

    this.emitFilters.emit(params);
  }



}

import { Component, OnInit, Inject, PLATFORM_ID, Input } from '@angular/core';
import { SearchModel } from '../../shared/shared.models';
import { isPlatformBrowser } from '@angular/common';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-compare-panel',
  templateUrl: './compare-panel.component.html',
  styleUrls: ['./compare-panel.component.scss']
})
export class ComparePanelComponent implements OnInit {
  @Input() mainProductID: number;
  keyword: string = '';
  noResultsFound: boolean;
  products: any[] = [];

  compareListSpinner: boolean;

  constructor(private catService: CategoryService, @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    this.getProductsToCompare()
  }

  getProductsToCompare = () => {
    this.compareListSpinner = true;
    let params: SearchModel = new SearchModel();
    this.noResultsFound = false;
    params.Keyword = this.keyword;
    if (isPlatformBrowser(this.platformId)) {
      params.CountryID = localStorage.getItem('countryID')
    }

    this.catService.searchProducts(params).subscribe((results) => {
      this.compareListSpinner = false;
      this.products = results['Data'];
      if (results['Data'].length == 0) {
        this.noResultsFound = true;
      }
    });
  };

  debounceTimeOut;
  debounceTime(fn, time) {
    clearTimeout(this.debounceTimeOut);
    this.debounceTimeOut = setTimeout(fn, time);
  };
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductPageComponent } from './product-page/product-page.component';
import { ProductsPageComponent } from './products-page/products-page.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ComparePageComponent } from './compare-page/compare-page.component';

const routes: Routes = [
  { path: 'product', component: ProductPageComponent },
  { path: 'product-list', component: ProductListComponent },
  { path: 'products-search', component: ProductsPageComponent },
  { path: 'compare', component: ComparePageComponent },
  { path: '', redirectTo: 'products-search', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }

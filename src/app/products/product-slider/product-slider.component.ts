import { Component, OnInit, Input } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['./product-slider.component.scss']
})
export class ProductSliderComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  
  @Input() galleryImages: NgxGalleryImage[];
  constructor() { }

  ngOnInit(): void {
    this.galleryOptions = [
      {
        width: '100%',
        height: '350px',
        thumbnailsColumns: 4,
        "previewZoom": true,
        "previewRotate": true,
        "previewCloseOnClick": true,
        "previewCloseOnEsc": true,
        "previewKeyboardNavigation": true,
        imageInfinityMove:true,
        imagePercent: 100,
        thumbnailsPercent: 30,
        arrowPrevIcon:"fa fa-angle-left",
        arrowNextIcon:"fa fa-angle-right",
        imageSwipe:true,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 500
      {
        breakpoint: 500,
        thumbnailsPercent: 50,
        height: '200px',
        preview: true
      },

    ];

  }
}

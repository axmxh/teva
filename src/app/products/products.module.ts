import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxGalleryModule } from 'ngx-gallery';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductPageComponent } from './product-page/product-page.component';
import { SharedModule } from '../shared/shared.module';
import { ProductSliderComponent } from './product-slider/product-slider.component';
import { ProductTabsComponent } from './product-tabs/product-tabs.component';
import { ProductsPageComponent } from './products-page/products-page.component';
import { ProductsFiltersComponent } from './products-filters/products-filters.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { ProductListComponent } from './product-list/product-list.component';
import { FormsModule } from '@angular/forms';
import { ComparePageComponent } from './compare-page/compare-page.component';
import { ComparePanelComponent } from './compare-panel/compare-panel.component';

@NgModule({
  declarations: [ProductPageComponent, ProductSliderComponent, ProductTabsComponent, ProductsPageComponent, ProductsFiltersComponent, ProductItemComponent, ProductListComponent, ComparePageComponent, ComparePanelComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    SharedModule,
    NgxGalleryModule,
    FormsModule
  ]
})
export class ProductsModule { }

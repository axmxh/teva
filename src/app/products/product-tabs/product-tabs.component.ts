import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-tabs',
  templateUrl: './product-tabs.component.html',
  styleUrls: ['./product-tabs.component.scss']
})
export class ProductTabsComponent implements OnInit {
  productId: number;
  style2 = { align: 'center', theme: 'white' }
  support = {
    title: "Support",
    description: "Access and download more product information and find quick and easy troubleshooting advice.",
    url: "/support",
    urlTxt: "Get Support",
    image: "assets/images/support-image.jpg"
  }

  widgets = [];
  logos = [];
  // logos = [
  //   "assets/images/additional-features/alexa.png",
  //   "assets/images/additional-features/dolby-audio.png",
  //   "assets/images/additional-features/netflix.png",
  //   "assets/images/additional-features/onkyo.png",
  //   "assets/images/additional-features/secure.png",
  //   "assets/images/additional-features/usb-cloning.png",
  //   "assets/images/additional-features/screen-share.png",
  //   "assets/images/additional-features/social-feed.png",
  //   "assets/images/additional-features/eco-friendly.png"
  // ]

  specifications: any[] = [];
  loading: boolean;
  downloads: any[] = [];
  constructor(private catService: CategoryService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.productId = params['ProductID'];
      this.getProductFeatures();
    });
  }


  getProductDownloads() {
    if (this.downloads.length == 0) {
      this.loading = true;
      let params = { ProductID: this.productId };
      this.catService.getProductDownloads(params).subscribe((data) => {
        this.loading = false;
        this.downloads = data['Data'];
      })
    }
  }

  getProductSpecs() {
    if (this.specifications.length == 0) {
      this.loading = true;
      let params = { ProductID: this.productId };
      this.catService.getProductSpecs(params).subscribe((data) => {
        this.loading = false;
        this.specifications = data['Data'];
      })
    }
  }
  getProductFeatures() {
    if (this.widgets.length == 0) {
      this.logos = [];
      this.loading = true;
      let params = { ProductID: this.productId };
      this.catService.getProductFeatures(params).subscribe((data) => {
        this.loading = false;
        data['Data'].map((widget) => {
          let item = {
            title: widget.Title,
            logo: "''",
            description: widget.Description,
            image: widget.featureImage,
            layoutId: widget.LayoutID,
            ShowInAdditional: widget.ShowInAdditional
          };
          if (!item.ShowInAdditional) this.widgets.push(item); else {this.logos.push(item)}
        });
      })
    }
  }


}

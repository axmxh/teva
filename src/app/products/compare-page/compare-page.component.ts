import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CategoryService } from '../../services/category.service';
import { ActivatedRoute } from '@angular/router';
import { SearchModel } from '../../shared/shared.models';

@Component({
  selector: 'app-compare-page',
  templateUrl: './compare-page.component.html',
  styleUrls: ['./compare-page.component.scss']
})
export class ComparePageComponent implements OnInit {
  loading: boolean;
  mainProduct: number;
  compareWith: number;
  mainProductData: any;
  compareWithData: any;
  products: any[] = [];
  selectedProduct: number;
  selectedProductWith: number;
  loadingMainProduct: boolean;
  loadingSecondaryProduct: boolean;
  constructor(private catService: CategoryService, private route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    this.getProductsToCompare();
    this.route.queryParams.subscribe((params) => {
      this.getProductDetails(params.mainProduct, 'main');
      this.getProductDetails(params.compareWith, 'secondary');
      this.selectedProduct = params.mainProduct;
      this.selectedProductWith = params.compareWith;
    });
  }

  getProductsToCompare() {
    let params: SearchModel = new SearchModel();
    params.Keyword = '';
    if (isPlatformBrowser(this.platformId)) {
      params.CountryID = localStorage.getItem('countryID')
    }

    this.catService.searchProducts(params).subscribe((results) => {
      this.products = results['Data'];
    });
  }

  getProductDetails(productId, type) {

    if (type === 'main') this.loadingMainProduct = true;
    if (type === 'secondary') this.loadingSecondaryProduct = true;
    this.loading = true;
    let params = {
      'ProductID': productId
    };
    this.catService.getProductToCompare(params).subscribe((data) => {
      this.loading = false;
      this.loadingMainProduct = false;
      this.loadingSecondaryProduct = false;
      if (type === 'main') this.mainProductData = data;
      if (type === 'secondary') this.compareWithData = data;
    });
  };



}

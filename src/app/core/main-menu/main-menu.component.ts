import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../services/header.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
  categories: any[] = [];
  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.headerService.getCategories().subscribe((categories: any) => {
      this.categories = categories.Data;
    })
  }

}

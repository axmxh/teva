import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RegionSelectComponent } from './region-select/region-select.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { TopSearchComponent } from './top-search/top-search.component';
import { FooterComponent } from './footer/footer.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
import { HeaderService } from '../services/header.service';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { FooterContentComponent } from './footer-content/footer-content.component';
import { CoreRoutingModule } from './core-routing.module';
@NgModule({
  declarations: [
    HeaderComponent,
    RegionSelectComponent,
    MainMenuComponent,
    TopSearchComponent,
    FooterComponent,
    PageNotFoundComponent,
    FooterContentComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    FormsModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    HeaderComponent,
    RegionSelectComponent,
    MainMenuComponent,
    TopSearchComponent,
    FooterComponent,
    PageNotFoundComponent
  ],
  providers: [HeaderService]
})
export class CoreModule { }

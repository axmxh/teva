import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { HeaderService } from '../../services/header.service';

@Component({
  selector: 'app-region-select',
  templateUrl: './region-select.component.html',
  styleUrls: ['./region-select.component.scss']
})
export class RegionSelectComponent implements OnInit {
  countries: any[] = [];
  country: string = '1';
  loading: boolean;
  constructor(private headerService: HeaderService, @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    this.getCountries();
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('countryID', '1');
    }
  }

  getCountries() {
    this.headerService.getCountries().subscribe((countries: any) => {
      this.countries = countries.Data;
    })
  };

  setCoutry() {
    this.loading = true;
    localStorage.setItem('countryID', this.country);
    setTimeout(() => {
      this.loading = false;
    }, 500);
  }

}

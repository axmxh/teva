import { Component, OnInit } from '@angular/core';
import { SearchModel } from '../../shared/shared.models';
import { Router } from '@angular/router';
@Component({
  selector: 'app-top-search',
  templateUrl: './top-search.component.html',
  styleUrls: ['./top-search.component.scss']
})
export class TopSearchComponent implements OnInit {
  keyword: string = '';

  constructor(private router: Router) { }

  ngOnInit() { }

  search() {

    if (this.keyword.trim().length > 0) {
      let search: SearchModel = new SearchModel();
      search.Keyword = this.keyword;

      this.router.navigate(['/products'], {
        queryParams: search
      });
      this.keyword = '';
    } else {
      this.keyword = '';
    }

  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FooterContentComponent } from './footer-content/footer-content.component';

const routes: Routes = [
    { path: 'footer-content', component: FooterContentComponent },
    { path: '', redirectTo: 'footer-content', pathMatch: 'full' }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoreRoutingModule { }

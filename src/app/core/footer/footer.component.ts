import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../services/header.service';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footerLinks: any = [];
  socialLinks: any = [];
  copyRights = new Date();
  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.getFooterLinks();
    this.getSocialLinks()
  }

  getFooterLinks() {
    this.headerService.getFooterLinks().subscribe(links => this.footerLinks = links)
  }

  getSocialLinks() {
    this.headerService.getSocialLinks().subscribe((links:any) => this.socialLinks = links.Data)
  }


}

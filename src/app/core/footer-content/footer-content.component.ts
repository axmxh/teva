import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../services/header.service';
import { ActivatedRoute } from '@angular/router';
import { ModalData } from '../../shared/shared.models';

@Component({
  selector: 'app-footer-content',
  templateUrl: './footer-content.component.html',
  styleUrls: ['./footer-content.component.scss']
})
export class FooterContentComponent implements OnInit {
  content: ModalData = new ModalData();
  contentId: number;

  constructor(private headerService: HeaderService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.contentId = params.id;
      this.getContent();
    })
  }

  getContent() {
    this.content = new ModalData();
    this.headerService.getStaticLinksData(this.contentId).subscribe((data) => {
      this.content = data['Data'].Content;
    })
  }
}

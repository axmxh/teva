export default {
    "latestNews": [
        {
            Title: '[Invitation] A Galaxy Event 2019',
            Date: '11 Feb 2019',
            Source: 'adigitalboom',
            ArticleImage: ''
        }, {
            Title: 'Toshiba’s QLED TVs and Its New “Magic Screen” Feature Transform Living Rooms into a Gallery Space',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: ''
        }, {
            Title: 'What’s Inside the World’s Largest Technology Experience Centre ',
            Date: '22 Aug 2019',
            Source: 'adigitalboom',
            ArticleImage: ''
        }, {
            Title: 'Toshiba to focus on traditional products for reboot',
            Date: '14 Dec 2019',
            Source: 'adigitalboom',
            ArticleImage: ''
        }, {
            Title: '[Invitation] A Galaxy Event 2019',
            Date: '11 Feb 2019',
            Source: 'adigitalboom',
            ArticleImage: ''
        }],
    "centerTop": [
        {
            Title: 'Toshiba Memory IPO likely pushed back two months to November',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news01.png'
        }, {
            Title: 'Toshiba’s QLED TVs and Its New “Magic Screen” Feature Transform Living Rooms …',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news03.png'
        }, {
            Title: '[Invitation] A Galaxy Event 2019',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news04.png'
        }],
    "rightColNews": [
        {
            Title: 'What’s Inside the World’s Largest Technology Experience Centre ',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news02.png'
        }, {
            Title: 'Toshiba Retail Challenge 2019',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news05.png'
        }, {
            Title: 'What’s Inside the World’s Largest Technology Experience Centre ',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news08.png'
        }, {
            Title: 'How internal power struggles put Toshiba on a dangerous path',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news11.png'
        }],
    "middleSection": [
        {
            Title: 'Toshiba to focus on traditional products for reboot',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news06.png'
        }, {
            Title: 'Toshiba said it now expects a net profit of 870 billion yen ($7.86 billion), down from a November …',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news07.png'
        }],
    "mostRecent": [
        {
            Title: 'Toshiba Memory IPO likely pushed back two months to November',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news09.png'
        }, {
            Title: 'Toshiba Memory IPO likely pushed back two months to November',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news10.png'
        }],
    "mostPopular": [
        {
            Title: 'Toshiba Memory IPO likely now expects a net profit of 870 billion yen ($7.86 billion), down from a November',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news07.png'
        }, {
            Title: 'Toshiba to focus on traditional products for reboot',
            Date: '02 Oct 2019',
            Source: 'adigitalboom',
            ArticleImage: 'assets/images/news/news11.png'
        }]
}

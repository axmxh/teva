import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from '../../services/news.service';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.scss']
})
export class NewsDetailsComponent implements OnInit {
  news: any = {};
  loading: boolean;
  constructor(private route: ActivatedRoute, private newsService: NewsService) { }

  ngOnInit() {
    this.loading = true;
    this.route.queryParams.subscribe(params => {
      this.newsService.getNewsDetails({ ID: params['id'] }).subscribe((news) => {
        this.loading = false;
        this.news = news['Data'];
      })
    });
  }

}

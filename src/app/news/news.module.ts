import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { VerticalWidgetNewsComponent } from './vertical-widget-news/vertical-widget-news.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { NewsService } from '../services/news.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [VerticalWidgetNewsComponent, NewsPageComponent, NewsDetailsComponent],
  imports: [
    CommonModule,
    NewsRoutingModule,
    SharedModule
  ],
  providers: [NewsService]
})
export class NewsModule { }

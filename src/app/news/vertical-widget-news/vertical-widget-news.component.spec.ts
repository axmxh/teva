import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalWidgetNewsComponent } from './vertical-widget-news.component';

describe('VerticalWidgetNewsComponent', () => {
  let component: VerticalWidgetNewsComponent;
  let fixture: ComponentFixture<VerticalWidgetNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerticalWidgetNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalWidgetNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

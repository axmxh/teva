import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vertical-widget-news',
  templateUrl: './vertical-widget-news.component.html',
  styleUrls: ['./vertical-widget-news.component.scss']
})
export class VerticalWidgetNewsComponent implements OnInit {
  @Input() news: Object;
  @Input() theme: string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onClickNews(newsId) {
    this.router.navigate(['/news/news-details'], { queryParams: { id: newsId } })
  }

}

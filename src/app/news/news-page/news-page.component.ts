import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss']
})
export class NewsPageComponent implements OnInit {
  latestNews: any[] = [];
  centerColNews: any[] = [];
  rightColNews: any[] = [];
  middleSection: any[] = [];
  onMostRecent: boolean = true;
  loading: boolean;
  countryID:number;
  constructor(private newsService: NewsService,
    @Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    this.getLatestNews();
    this.getNews();
    if (isPlatformBrowser(this.platformId)) {
        this.countryID = +localStorage.getItem('countryID')
    }
  }

  getLatestNews() {
    let params = { CountryID: this.countryID };
    this.newsService.getLatestNews(params).subscribe((data) => {
      this.latestNews = data['Data'];
    });
  };
  getNews() {
    this.loading = true;
    let params = { CountryID: this.countryID };
    this.newsService.getNews(params).subscribe((data) => {
      this.loading = false;
      this.centerColNews = data['Data'].filter(news => news.BlockTypeID == 1);
      this.middleSection = data['Data'].filter(news => news.BlockTypeID == 2);
      this.rightColNews = data['Data'].filter(news => news.BlockTypeID == 3);
    });
  };

}

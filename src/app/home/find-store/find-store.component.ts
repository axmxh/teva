import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../services/header.service';
import { MapsAPILoader } from '@agm/core';
import { HttpClient } from '@angular/common/http';
declare var $: any;
declare const google: any;

@Component({
  selector: 'app-find-store',
  templateUrl: './find-store.component.html',
  styleUrls: ['./find-store.component.scss']
})
export class FindStoreComponent implements OnInit {
  stores: any[] = [];
  loading: boolean;
  storeName: string = '';
  inputLoader: boolean;
  onChanges: boolean;
  onErr: boolean;
  countryCode: string;
  ip: any;
  constructor(private headerService: HeaderService, private mapsAPILoader: MapsAPILoader, private http: HttpClient) { }

  ngOnInit() {
    // $.get("http://ip-api.com/json", (response) => {
    //   this.countryCode = response.countryCode;
    //   this.getStores();
    // }, "jsonp");

    // this.getCurrentCountry();

    this.http.get<{ ip: string }>('https://jsonip.com').subscribe(data => {
      this.ip = data.ip;
      this.getStores();
    });
  }

  getStores = () => {
    this.stores = [];
    this.loading = true;
    this.onErr = false;
    this.headerService.getStores(this.ip, this.storeName).subscribe((stores: any) => {
      this.stores = stores.Data;
      this.onErr = false;
      this.loading = false;
    }, (err) => {
      this.loading = false;
      this.onErr = true;
    })
  }

  debounceTimeOut;
  debounceTime(fn, time) {
    this.onChanges = true;
    if (this.storeName.trim().length >= 1) {
      clearTimeout(this.debounceTimeOut);
      this.debounceTimeOut = setTimeout(fn, time);
    }
  }


  async getCurrentCountry() {
    return await new Promise((resolve, reject) => {
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition(async (position) => {
          // console.log(position);
          this.mapsAPILoader.load().then(() => {
            const geocoder = new google.maps.Geocoder();
            const latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            const request = { latLng: latlng };

            geocoder.geocode(request, (results, status) => {
              if (status === google.maps.GeocoderStatus.OK) {
                // console.log(results);
                let address_components = results[0].address_components;
                let address = address_components.filter(r => {
                  if (r.types[0] == 'country') {
                    return r;
                  }
                }).map(r => {
                  return r.short_name;
                })
                console.log(address[0]);
                resolve(address[0]);
              }
            });
          });

        });
      } else {
        /* default return */
        return 'SA';
      }
    })
  }

}

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HeaderService } from '../../services/header.service';
import { HomeService } from '../../services/home.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-distributor',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.scss']
})
export class DistributorComponent implements OnInit {
  @ViewChild('myInput') myInputVariable: ElementRef;
  companyName: string;
  countryId: string;
  companyNumber: string;
  companyEmail: string;
  contactPerson: string;
  contactNumber: string;
  countries: any[] = [];
  loading: boolean;
  onSuccess: boolean;
  successMsg: string = '';
  onErr: boolean;
  errors: any[] = [];
  fileToUpload: File = null;
  uploadedFile: FormData;
  constructor(private headerService: HeaderService, private homeService: HomeService) { }

  ngOnInit() {
    this.headerService.getCountries().subscribe((countries) => {
      this.countries = countries['Data']
    })
  }


  saveDistributor(disForm: NgForm) {
    this.onSuccess = false;
    this.onErr = false;
    this.loading = true;

    let body = {
      "CompanyName": this.companyName,
      "CompanyNumber": this.companyNumber ? this.companyNumber.toString() : '',
      "CompanyEmail": this.companyEmail,
      "ContactPersonName": this.contactPerson,
      "ContactNumber": this.contactNumber,
      "CountryID": this.countryId
    }
    this.homeService.SaveDistributor(JSON.stringify(body), this.uploadedFile).subscribe((data) => {
      this.loading = false;
      if (data['Status'] == true) {
        this.onSuccess = true;
        this.successMsg = 'Email Sent Successfully!';
        this.fileToUpload = null;
        this.myInputVariable.nativeElement.value = "";
        disForm.reset();
      } else {
        // disForm.reset();
        this.onErr = true;
        this.errors = data['Exceptions'];
        this.fileToUpload = null;
        this.myInputVariable.nativeElement.value = "";
      }
    }, (err) => {
      this.fileToUpload = null;
    })
  };

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload)
    this.uploadedFile = new FormData();
    this.uploadedFile.append('file', this.fileToUpload, this.fileToUpload.name);
    // console.log(this.uploadedFile)
  }
}



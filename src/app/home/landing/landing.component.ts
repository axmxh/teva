import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home.service';
import { Router } from '@angular/router';
import { SearchModel } from '../../shared/shared.models';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  loading: boolean;
  support = {
    title: "Support",
    description: "Access and download more product information and find quick and easy troubleshooting advice.",
    url: "/support",
    urlTxt: "Get Support",
    image: "assets/images/support-image.jpg"
  }
  widgets = [];
  sliderImgs: any[] = [];
  single: any[] = [];
  grid: any[] = [];
  keyword: string = '';
  constructor(private homeService: HomeService, private router: Router) { }

  ngOnInit() {
    this.getHomeBlocks();
  }

  getHomeBlocks() {
    this.loading = true;
    this.homeService.getHomeBlocks().subscribe((blocks: any) => {
      this.loading = false;
      this.sliderImgs = blocks.Data.filter((block) => block.HomeBlockTypeID == 1);
      blocks.Data.map((block) => {
        let widget = {
          id: block.ID,
          title: block.Title,
          subTitle: block.SubTitle,
          url: block.URL,
          image: block.Image,
          description: block.Description,
          urlTxt: 'See Details',
          urlType: 'external'
        }
        if (block.HomeBlockTypeID == 2) {
          this.single.push(widget);
        } else if (block.HomeBlockTypeID == 3) {
          this.grid.push(widget)
        }
      });

    })
  }

  search() {
    let search: SearchModel = new SearchModel();
    search.Keyword = this.keyword;

    this.router.navigate(['/products'], {
      queryParams: search
    })

  }
}

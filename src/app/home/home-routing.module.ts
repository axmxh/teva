import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { FindStoreComponent } from './find-store/find-store.component';
import { DistributorComponent } from './distributor/distributor.component';

const routes: Routes = [
  { path: 'home', component: LandingComponent },
  { path: 'find-store', component: FindStoreComponent },
  { path: 'become-distributor', component: DistributorComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { LandingComponent } from './landing/landing.component';
import { SharedModule } from '../shared/shared.module';
import { FindStoreComponent } from './find-store/find-store.component';
import { FormsModule } from '@angular/forms';
import { HomeService } from '../services/home.service';
import { DistributorComponent } from './distributor/distributor.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [LandingComponent, FindStoreComponent, DistributorComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74',
      language: 'en'
    })
  ],
  providers: [HomeService]
})
export class HomeModule { }
// AIzaSyB2tKpENK9xz4pEGYN4yGyeGS3NgAiCRIY

/// AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74
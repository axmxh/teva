(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/distributor/distributor.component.html":
/*!*************************************************************!*\
  !*** ./src/app/home/distributor/distributor.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"py-5\">\r\n  <div class=\"container text-center\">\r\n    <h2 class=\"mt-5\">Are you a distributor?</h2>\r\n    <h5>Send your request and join our family</h5>\r\n  </div>\r\n</header>\r\n<div class=\"container my-5 bg-white py-3 shadow\">\r\n  <h2 class=\"bg-secondary text-white mb-4 p-2\"><small>Distributor info</small></h2>\r\n  <div class=\"alert alert-danger\" *ngIf=\"onErr\">\r\n    <p *ngFor=\"let err of errors\">{{err}}</p>\r\n  </div>\r\n  <div class=\"alert alert-success\" *ngIf=\"onSuccess\">{{successMsg}}</div>\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n  <form autocomplete=\"off\" (ngSubmit)=\"saveDistributor(disForm)\" #disForm=\"ngForm\">\r\n    <div class=\"row\">\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Company Name <span class=\"text-danger\">*</span></label>\r\n\r\n        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"companyName\" name=\"companyName\" placeholder=\"Company Name\"\r\n          required>\r\n        <div class=\"has-error\">This field is required</div>\r\n\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Country Name <span class=\"text-danger\">*</span></label>\r\n\r\n        <select class=\"form-control\" [(ngModel)]=\"countryId\" name=\"countryId\" required>\r\n          <option value=\"null\" selected hidden>Select Country</option>\r\n          <option *ngFor=\"let country of countries\" [value]=\"country.ID\">{{country.Name}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Company Number <span class=\"text-danger\">*</span></label>\r\n\r\n        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"companyNumber\" name=\"companyNumber\"\r\n          placeholder=\"Company Number\" required>\r\n        <div class=\"has-error\">This field is required</div>\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Email Address <span class=\"text-danger\">*</span></label>\r\n\r\n        <input type=\"email\" class=\"form-control\" required [(ngModel)]=\"companyEmail\" name=\"companyEmail\"\r\n          pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\" required placeholder=\"Company Email\">\r\n        <div class=\"has-error\">Invalid email address</div>\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Contact Person</label>\r\n\r\n        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"contactPerson\" name=\"contactPerson\"\r\n          placeholder=\"Contact Person (name)\">\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Contact Number <span class=\"text-danger\">*</span></label>\r\n\r\n        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"contactNumber\" name=\"contactNumber\"\r\n          placeholder=\"Contact Number\" required>\r\n          <div class=\"has-error\">This field is required</div>\r\n      </div>\r\n      <div class=\"form-group col-12\">\r\n        <label>Attachment</label>\r\n        <div class=\"input-group mb-3\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\" id=\"inputGroupFileAddon01\">\r\n              <i class=\"fa fa-paperclip\" aria-hidden=\"true\"></i></span>\r\n          </div>\r\n          <div class=\"custom-file\">\r\n            <input type=\"file\" class=\"custom-file-input\" id=\"file\" #myInput (change)=\"handleFileInput($event.target.files)\"\r\n              aria-describedby=\"inputGroupFileAddon01\">\r\n            <label class=\"custom-file-label\" for=\"inputGroupFile01\">{{fileToUpload?.name || 'Choose file'}}</label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group col-12 text-center mt-3\">\r\n        <button type=\"submit\" class=\"primary-btn w-25 py-2\">Send Request</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/home/distributor/distributor.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/home/distributor/distributor.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvZGlzdHJpYnV0b3IvZGlzdHJpYnV0b3IuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/home/distributor/distributor.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/home/distributor/distributor.component.ts ***!
  \***********************************************************/
/*! exports provided: DistributorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistributorComponent", function() { return DistributorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/header.service */ "./src/app/services/header.service.ts");
/* harmony import */ var _services_home_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/home.service */ "./src/app/services/home.service.ts");




var DistributorComponent = /** @class */ (function () {
    function DistributorComponent(headerService, homeService) {
        this.headerService = headerService;
        this.homeService = homeService;
        this.countries = [];
        this.successMsg = '';
        this.errors = [];
        this.fileToUpload = null;
    }
    DistributorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.headerService.getCountries().subscribe(function (countries) {
            _this.countries = countries['Data'];
        });
    };
    DistributorComponent.prototype.saveDistributor = function (disForm) {
        var _this = this;
        this.onSuccess = false;
        this.onErr = false;
        this.loading = true;
        var body = {
            "CompanyName": this.companyName,
            "CompanyNumber": this.companyNumber ? this.companyNumber.toString() : '',
            "CompanyEmail": this.companyEmail,
            "ContactPersonName": this.contactPerson,
            "ContactNumber": this.contactNumber,
            "CountryID": this.countryId
        };
        this.homeService.SaveDistributor(JSON.stringify(body), this.uploadedFile).subscribe(function (data) {
            _this.loading = false;
            if (data['Status'] == true) {
                _this.onSuccess = true;
                _this.successMsg = 'Email Sent Successfully!';
                _this.fileToUpload = null;
                _this.myInputVariable.nativeElement.value = "";
                disForm.reset();
            }
            else {
                // disForm.reset();
                _this.onErr = true;
                _this.errors = data['Exceptions'];
                _this.fileToUpload = null;
                _this.myInputVariable.nativeElement.value = "";
            }
        }, function (err) {
            _this.fileToUpload = null;
        });
    };
    ;
    DistributorComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
        console.log(this.fileToUpload);
        this.uploadedFile = new FormData();
        this.uploadedFile.append('file', this.fileToUpload, this.fileToUpload.name);
        // console.log(this.uploadedFile)
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myInput'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], DistributorComponent.prototype, "myInputVariable", void 0);
    DistributorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-distributor',
            template: __webpack_require__(/*! ./distributor.component.html */ "./src/app/home/distributor/distributor.component.html"),
            styles: [__webpack_require__(/*! ./distributor.component.scss */ "./src/app/home/distributor/distributor.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"], _services_home_service__WEBPACK_IMPORTED_MODULE_3__["HomeService"]])
    ], DistributorComponent);
    return DistributorComponent;
}());



/***/ }),

/***/ "./src/app/home/find-store/find-store.component.html":
/*!***********************************************************!*\
  !*** ./src/app/home/find-store/find-store.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"py-5\">\r\n  <div class=\"container text-center\">\r\n    <h2 class=\"mt-5\">Find a store</h2>\r\n    <div class=\"search-input\">\r\n      <i class=\"fa fa-search\"></i>\r\n      <input (keyup)=\"debounceTime(getStores, 800)\" [(ngModel)]=\"storeName\" name=\"storeName\" type=\"text\"\r\n        class=\"form-control\" placeholder=\"Enter store name or Keyword…\">\r\n    </div>\r\n    <button *ngIf=\"onChanges\" (click)=\"storeName = ''; getStores();onChanges = false\"\r\n      class=\"plain-btn btn-sm text-white\">Show All</button>\r\n\r\n  </div>\r\n</header>\r\n<div class=\"container my-5\">\r\n  <div class=\"spinner-container my-5\" *ngIf=\"loading\">\r\n    <div class=\"spinner-grow text-danger\" role=\"status\"></div>\r\n  </div>\r\n  <div class=\"alert alert-danger\" *ngIf=\"onErr\">Something went wrong! please try again!</div>\r\n  <ul class=\"stores-list\">\r\n    <li *ngFor=\"let store of stores\">\r\n      <h3 class=\"mb-4\"><img class=\"mr-3\" width=\"38\" [src]=\"store.CountryFlag\">{{store.CountryName}} Stores</h3>\r\n      <ul>\r\n        <li *ngFor=\"let branch of store.Stores\">\r\n          <h4>{{branch.StoreName}}</h4>\r\n          <h5 class=\"text-muted\">{{branch.Address}}</h5>\r\n          <h5 class=\"text-muted\">{{branch.ContactPerson}}</h5>\r\n          <h5 class=\"text-muted\">{{branch.Mobile}}</h5>\r\n          <h5 class=\"text-muted\">{{branch.PhoneNumber}}</h5>\r\n          <h5 class=\"text-muted\">{{branch.Email}}</h5>\r\n        </li>\r\n      </ul>\r\n    </li>\r\n  </ul>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/home/find-store/find-store.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/home/find-store/find-store.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background-image: url('find-a-store-header.jpg');\n  background-repeat: no-repeat;\n  background-size: cover; }\n  header h2 {\n    font-family: sfpro-bold;\n    text-shadow: 0 0 12px rgba(0, 0, 0, 0.63);\n    font-size: 49px;\n    letter-spacing: -3px;\n    color: #FFF; }\n  header .search-input {\n    display: flex;\n    width: 40%;\n    align-items: center;\n    background: #FFF;\n    border: 1px solid #cbd0d4;\n    padding: 0 10px;\n    margin: auto auto 30px auto; }\n  header .search-input input {\n      border: 0; }\n  header .search-input i {\n      color: #ed1c24; }\n  .stores-list ul {\n  display: flex;\n  flex-wrap: wrap;\n  padding: 0 20px;\n  margin-bottom: 40px; }\n  .stores-list ul li {\n    flex: 33%;\n    margin-bottom: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9maW5kLXN0b3JlL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcaG9tZVxcZmluZC1zdG9yZVxcZmluZC1zdG9yZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvaG9tZS9maW5kLXN0b3JlL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFzc2V0c1xcc2Nzc1xcdmFyaWFibGVzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxnREFBcUU7RUFDckUsNEJBQTRCO0VBQzVCLHNCQUFzQixFQUFBO0VBSHhCO0lBTUksdUJBQXVCO0lBQ3ZCLHlDQUF5QztJQUN6QyxlQUFlO0lBQ2Ysb0JBQW9CO0lBQ3BCLFdBQ0YsRUFBQTtFQVhGO0lBY0ksYUFBYTtJQUNiLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2YsMkJBQTJCLEVBQUE7RUFwQi9CO01BdUJNLFNBQVMsRUFBQTtFQXZCZjtNQTJCTSxjQzVCcUIsRUFBQTtFRGdDM0I7RUFDRSxhQUFhO0VBQ2IsZUFBZTtFQUNmLGVBQWU7RUFDZixtQkFBbUIsRUFBQTtFQUpyQjtJQU1JLFNBQVM7SUFDVCxtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2hvbWUvZmluZC1zdG9yZS9maW5kLXN0b3JlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xyXG5cclxuaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9maW5kLWEtc3RvcmUtaGVhZGVyLmpwZyk7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG5cclxuICBoMiB7XHJcbiAgICBmb250LWZhbWlseTogc2Zwcm8tYm9sZDtcclxuICAgIHRleHQtc2hhZG93OiAwIDAgMTJweCByZ2JhKDAsIDAsIDAsIDAuNjMpO1xyXG4gICAgZm9udC1zaXplOiA0OXB4O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IC0zcHg7XHJcbiAgICBjb2xvcjogI0ZGRlxyXG4gIH1cclxuXHJcbiAgLnNlYXJjaC1pbnB1dCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NiZDBkNDtcclxuICAgIHBhZGRpbmc6IDAgMTBweDtcclxuICAgIG1hcmdpbjogYXV0byBhdXRvIDMwcHggYXV0bztcclxuXHJcbiAgICBpbnB1dCB7XHJcbiAgICAgIGJvcmRlcjogMDtcclxuICAgIH1cclxuXHJcbiAgICBpIHtcclxuICAgICAgY29sb3I6ICRwcmltYXJ5LWJ0bi1jb2xvclxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uc3RvcmVzLWxpc3QgdWx7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgcGFkZGluZzogMCAyMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XHJcbiAgbGkge1xyXG4gICAgZmxleDogMzMlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICB9XHJcbn0iLCIkcHJpbWFyeS1iZy1jb2xvcjogIzQ0NDQ0NDtcclxuJHByaW1hcnktYnRuLWNvbG9yOiAjZWQxYzI0OyJdfQ== */"

/***/ }),

/***/ "./src/app/home/find-store/find-store.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/home/find-store/find-store.component.ts ***!
  \*********************************************************/
/*! exports provided: FindStoreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FindStoreComponent", function() { return FindStoreComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/header.service */ "./src/app/services/header.service.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm5/agm-core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");





var FindStoreComponent = /** @class */ (function () {
    function FindStoreComponent(headerService, mapsAPILoader, http) {
        var _this = this;
        this.headerService = headerService;
        this.mapsAPILoader = mapsAPILoader;
        this.http = http;
        this.stores = [];
        this.storeName = '';
        this.getStores = function () {
            _this.stores = [];
            _this.loading = true;
            _this.onErr = false;
            _this.headerService.getStores(_this.ip, _this.storeName).subscribe(function (stores) {
                _this.stores = stores.Data;
                _this.onErr = false;
                _this.loading = false;
            }, function (err) {
                _this.loading = false;
                _this.onErr = true;
            });
        };
    }
    FindStoreComponent.prototype.ngOnInit = function () {
        // $.get("http://ip-api.com/json", (response) => {
        //   this.countryCode = response.countryCode;
        //   this.getStores();
        // }, "jsonp");
        var _this = this;
        // this.getCurrentCountry();
        this.http.get('https://jsonip.com').subscribe(function (data) {
            _this.ip = data.ip;
            _this.getStores();
        });
    };
    FindStoreComponent.prototype.debounceTime = function (fn, time) {
        this.onChanges = true;
        if (this.storeName.trim().length >= 1) {
            clearTimeout(this.debounceTimeOut);
            this.debounceTimeOut = setTimeout(fn, time);
        }
    };
    FindStoreComponent.prototype.getCurrentCountry = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve, reject) {
                            if ('geolocation' in navigator) {
                                navigator.geolocation.getCurrentPosition(function (position) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                        // console.log(position);
                                        this.mapsAPILoader.load().then(function () {
                                            var geocoder = new google.maps.Geocoder();
                                            var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                            var request = { latLng: latlng };
                                            geocoder.geocode(request, function (results, status) {
                                                if (status === google.maps.GeocoderStatus.OK) {
                                                    // console.log(results);
                                                    var address_components = results[0].address_components;
                                                    var address = address_components.filter(function (r) {
                                                        if (r.types[0] == 'country') {
                                                            return r;
                                                        }
                                                    }).map(function (r) {
                                                        return r.short_name;
                                                    });
                                                    console.log(address[0]);
                                                    resolve(address[0]);
                                                }
                                            });
                                        });
                                        return [2 /*return*/];
                                    });
                                }); });
                            }
                            else {
                                /* default return */
                                return 'SA';
                            }
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    FindStoreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-find-store',
            template: __webpack_require__(/*! ./find-store.component.html */ "./src/app/home/find-store/find-store.component.html"),
            styles: [__webpack_require__(/*! ./find-store.component.scss */ "./src/app/home/find-store/find-store.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"], _agm_core__WEBPACK_IMPORTED_MODULE_3__["MapsAPILoader"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
    ], FindStoreComponent);
    return FindStoreComponent;
}());



/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _landing_landing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./landing/landing.component */ "./src/app/home/landing/landing.component.ts");
/* harmony import */ var _find_store_find_store_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./find-store/find-store.component */ "./src/app/home/find-store/find-store.component.ts");
/* harmony import */ var _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./distributor/distributor.component */ "./src/app/home/distributor/distributor.component.ts");






var routes = [
    { path: 'home', component: _landing_landing_component__WEBPACK_IMPORTED_MODULE_3__["LandingComponent"] },
    { path: 'find-store', component: _find_store_find_store_component__WEBPACK_IMPORTED_MODULE_4__["FindStoreComponent"] },
    { path: 'become-distributor', component: _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_5__["DistributorComponent"] },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
];
var HomeRoutingModule = /** @class */ (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _landing_landing_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./landing/landing.component */ "./src/app/home/landing/landing.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _find_store_find_store_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./find-store/find-store.component */ "./src/app/home/find-store/find-store.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_home_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/home.service */ "./src/app/services/home.service.ts");
/* harmony import */ var _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./distributor/distributor.component */ "./src/app/home/distributor/distributor.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm5/agm-core.js");











var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_landing_landing_component__WEBPACK_IMPORTED_MODULE_4__["LandingComponent"], _find_store_find_store_component__WEBPACK_IMPORTED_MODULE_6__["FindStoreComponent"], _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_9__["DistributorComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_3__["HomeRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_10__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74',
                    language: 'en'
                })
            ],
            providers: [_services_home_service__WEBPACK_IMPORTED_MODULE_8__["HomeService"]]
        })
    ], HomeModule);
    return HomeModule;
}());

// AIzaSyB2tKpENK9xz4pEGYN4yGyeGS3NgAiCRIY
/// AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74


/***/ }),

/***/ "./src/app/home/landing/landing.component.html":
/*!*****************************************************!*\
  !*** ./src/app/home/landing/landing.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"border-bottom\">\r\n  <div class=\"container\">\r\n    <div class=\"px-3 d-flex search-product py-1\">\r\n      <img width=\"26\" src=\"assets/images/search-anticon.svg\">\r\n      <input type=\"text\" class=\"form-control border-0\" placeholder=\"Search for product\" [(ngModel)]=\"keyword\" name=\"keyword\" (keyup.enter)=\"search()\">\r\n    </div>\r\n  </div>\r\n</div> -->\r\n\r\n<app-spinner *ngIf=\"loading\"></app-spinner>\r\n<app-carousel [slides]=\"sliderImgs\"></app-carousel>\r\n<div class=\"container-fluid p-0\">\r\n  <div class=\"row m-0\">\r\n    <div class=\"col-12 p-0\">\r\n      <app-widget-article [data]=\"single[0]\" [style]=\"'styleXfalse'\" [themeColor]=\"'black'\"></app-widget-article>\r\n    </div>\r\n    <div class=\"col-md-6 col-12 p-0\">\r\n      <app-widget-article [style]=\"'styleVertical'\" [data]=\"grid[0]\"></app-widget-article>\r\n    </div>\r\n    <div class=\"col-md-6 col-12 p-0\">\r\n      <app-widget-article [style]=\"'styleVertical'\" [data]=\"grid[1]\"></app-widget-article>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <app-widget-article [style]=\"'styleHorizontal'\" [data]=\"support\"></app-widget-article>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/home/landing/landing.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/home/landing/landing.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".search-product input {\n  font-size: 25px;\n  color: #444444;\n  background-color: #fafafa; }\n  .search-product input::-webkit-input-placeholder {\n    font-size: 25px;\n    color: #c8c8c8; }\n  .search-product input::-moz-placeholder {\n    font-size: 25px;\n    color: #c8c8c8; }\n  .search-product input::-ms-input-placeholder {\n    font-size: 25px;\n    color: #c8c8c8; }\n  .search-product input::placeholder {\n    font-size: 25px;\n    color: #c8c8c8; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9sYW5kaW5nL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcaG9tZVxcbGFuZGluZ1xcbGFuZGluZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGVBQWU7RUFDZixjQUFjO0VBQ2QseUJBQXlCLEVBQUE7RUFKN0I7SUFNUSxlQUFlO0lBQ2YsY0FBYyxFQUFBO0VBUHRCO0lBTVEsZUFBZTtJQUNmLGNBQWMsRUFBQTtFQVB0QjtJQU1RLGVBQWU7SUFDZixjQUFjLEVBQUE7RUFQdEI7SUFNUSxlQUFlO0lBQ2YsY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9sYW5kaW5nL2xhbmRpbmcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2VhcmNoLXByb2R1Y3Qge1xyXG4gIGlucHV0IHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIGNvbG9yOiAjNDQ0NDQ0O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmYTtcclxuICAgICY6OnBsYWNlaG9sZGVyIHtcclxuICAgICAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICAgICAgY29sb3I6ICNjOGM4Yzg7XHJcbiAgICAgIH1cclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/home/landing/landing.component.ts":
/*!***************************************************!*\
  !*** ./src/app/home/landing/landing.component.ts ***!
  \***************************************************/
/*! exports provided: LandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingComponent", function() { return LandingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_home_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/home.service */ "./src/app/services/home.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");





var LandingComponent = /** @class */ (function () {
    function LandingComponent(homeService, router) {
        this.homeService = homeService;
        this.router = router;
        this.support = {
            title: "Support",
            description: "Access and download more product information and find quick and easy troubleshooting advice.",
            url: "/support",
            urlTxt: "Get Support",
            image: "assets/images/support-image.jpg"
        };
        this.widgets = [];
        this.sliderImgs = [];
        this.single = [];
        this.grid = [];
        this.keyword = '';
    }
    LandingComponent.prototype.ngOnInit = function () {
        this.getHomeBlocks();
    };
    LandingComponent.prototype.getHomeBlocks = function () {
        var _this = this;
        this.loading = true;
        this.homeService.getHomeBlocks().subscribe(function (blocks) {
            _this.loading = false;
            _this.sliderImgs = blocks.Data.filter(function (block) { return block.HomeBlockTypeID == 1; });
            blocks.Data.map(function (block) {
                var widget = {
                    id: block.ID,
                    title: block.Title,
                    subTitle: block.SubTitle,
                    url: block.URL,
                    image: block.Image,
                    description: block.Description,
                    urlTxt: 'See Details',
                    urlType: 'external'
                };
                if (block.HomeBlockTypeID == 2) {
                    _this.single.push(widget);
                }
                else if (block.HomeBlockTypeID == 3) {
                    _this.grid.push(widget);
                }
            });
        });
    };
    LandingComponent.prototype.search = function () {
        var search = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_4__["SearchModel"]();
        search.Keyword = this.keyword;
        this.router.navigate(['/products'], {
            queryParams: search
        });
    };
    LandingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landing',
            template: __webpack_require__(/*! ./landing.component.html */ "./src/app/home/landing/landing.component.html"),
            styles: [__webpack_require__(/*! ./landing.component.scss */ "./src/app/home/landing/landing.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_home_service__WEBPACK_IMPORTED_MODULE_2__["HomeService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LandingComponent);
    return LandingComponent;
}());



/***/ }),

/***/ "./src/app/services/home.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/home.service.ts ***!
  \******************************************/
/*! exports provided: HomeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeService", function() { return HomeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api */ "./src/app/api.ts");



// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

var HomeService = /** @class */ (function () {
    function HomeService(http, proxy) {
        this.http = http;
        this.proxy = proxy;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
            'Access-Control-Allow-Headers': "*",
            'Access-Control-Allow-Credentials': 'true',
        });
        this.headersToUpload = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'multipart/form-data;boundary=' + Math.random()
        });
    }
    // get home blocks
    HomeService.prototype.getHomeBlocks = function () {
        return this.http.get(this.proxy.api + 'Home/GetHomeBlocks', { headers: this.headers });
    };
    // become a distributor
    HomeService.prototype.SaveDistributor = function (request, files) {
        return this.http.post(this.proxy.api + 'Distributor/SaveDistributor', { request: request, files: files }, { headers: this.headers });
    };
    HomeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _api__WEBPACK_IMPORTED_MODULE_3__["ProxyUrl"]])
    ], HomeService);
    return HomeService;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map
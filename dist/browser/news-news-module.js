(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["news-news-module"],{

/***/ "./src/app/news/news-details/news-details.component.html":
/*!***************************************************************!*\
  !*** ./src/app/news/news-details/news-details.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container my-4\">\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n  <h1>{{news.Title}}</h1>\r\n  <p class=\"mb-3\"><small class=\"text-muted\">{{news.Date}}</small></p>\r\n  <div *ngIf=\"news.ArticleImage\" class=\"news-img\">\r\n    <img [src]=\"news.ArticleImage\" alt=\"\" class=\"img-fluid\">\r\n  </div>\r\n  <div *ngIf=\"news.YoutubeLink\" class=\"news-iframe\">\r\n    <iframe [src]=\"news.YoutubeLink | safe:'resourceUrl'\" frameborder=\"0\" allow=\"fullscreen\" allowfullscreen></iframe>\r\n  </div>\r\n  <p class=\"mb-3\"><small class=\"text-muted\">Source: {{news.Source}}</small></p>\r\n\r\n  <div class=\"news-details\">\r\n    <!-- <ul class=\"share-btns d-flex flex-column float-left mr-5\">\r\n      <li><a class=\"text-dark\" href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n      <li><a class=\"text-dark\" href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n      <li><a class=\"text-dark\" href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n    </ul> -->\r\n    <p class=\"px-3\">{{news.FullDescription}}</p>\r\n  </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/news/news-details/news-details.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/news/news-details/news-details.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".news-iframe {\n  width: 100%;\n  text-align: center; }\n  .news-iframe iframe {\n    width: 100%;\n    height: 22rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy9uZXdzLWRldGFpbHMvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxuZXdzXFxuZXdzLWRldGFpbHNcXG5ld3MtZGV0YWlscy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQVc7RUFDWCxrQkFBa0IsRUFBQTtFQUZ0QjtJQUlRLFdBQVc7SUFDWCxhQUFhLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9uZXdzL25ld3MtZGV0YWlscy9uZXdzLWRldGFpbHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmV3cy1pZnJhbWV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGlmcmFtZSB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAyMnJlbTtcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/news/news-details/news-details.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/news/news-details/news-details.component.ts ***!
  \*************************************************************/
/*! exports provided: NewsDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsDetailsComponent", function() { return NewsDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_news_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/news.service */ "./src/app/services/news.service.ts");




var NewsDetailsComponent = /** @class */ (function () {
    function NewsDetailsComponent(route, newsService) {
        this.route = route;
        this.newsService = newsService;
        this.news = {};
    }
    NewsDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loading = true;
        this.route.queryParams.subscribe(function (params) {
            _this.newsService.getNewsDetails({ ID: params['id'] }).subscribe(function (news) {
                _this.loading = false;
                _this.news = news['Data'];
            });
        });
    };
    NewsDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-news-details',
            template: __webpack_require__(/*! ./news-details.component.html */ "./src/app/news/news-details/news-details.component.html"),
            styles: [__webpack_require__(/*! ./news-details.component.scss */ "./src/app/news/news-details/news-details.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_news_service__WEBPACK_IMPORTED_MODULE_3__["NewsService"]])
    ], NewsDetailsComponent);
    return NewsDetailsComponent;
}());



/***/ }),

/***/ "./src/app/news/news-page/news-page.component.html":
/*!*********************************************************!*\
  !*** ./src/app/news/news-page/news-page.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\r\n  <div class=\"row\">\r\n    <app-spinner *ngIf=\"loading\" [fixedSpinner]=\"true\"></app-spinner>\r\n\r\n    <div class=\"col-md-9 col-12 bg-white\">\r\n      <h3 class=\"my-3\">Latest News</h3>\r\n      <div class=\"row\">\r\n        <!-- news left -->\r\n        <ul class=\"col-md-4 col-12\">\r\n          <li *ngFor=\"let item of latestNews\">\r\n            <app-vertical-widget-news [news]=\"item\" [theme]=\"'default'\"></app-vertical-widget-news>\r\n          </li>\r\n        </ul>\r\n\r\n        <!-- news middle -->\r\n        <ul class=\"middle-col-news col-md-8 col-12 p-0\">\r\n          <li *ngFor=\"let item of centerColNews; let i = index\">\r\n            <app-vertical-widget-news [news]=\"item\" [theme]=\"i == 1 ? 'default' : 'cover'\"></app-vertical-widget-news>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n\r\n      <ul class=\"middle-section-news\">\r\n        <li *ngFor=\"let item of middleSection; let i = index\">\r\n          <app-vertical-widget-news [news]=\"item\" [theme]=\"'cover'\"></app-vertical-widget-news>\r\n        </li>\r\n      </ul>\r\n\r\n      <!-- tabs -->\r\n      <!-- <div class=\"row\">\r\n        <div class=\"tabs\">\r\n          <ul class=\"tabs-header\">\r\n            <li><a (click)=\"onMostRecent = true\" [ngClass]=\"{'active' : onMostRecent}\">Most Recent</a></li>\r\n            <li><a (click)=\"onMostRecent = false\" [ngClass]=\"{'active' : !onMostRecent}\">Most Popular</a></li>\r\n          </ul>\r\n          <ul [ngClass]=\"{'d-none': !onMostRecent}\" class=\"tabs-content\">\r\n            <li *ngFor=\"let item of mostRecent\" class=\"mb-2\">\r\n              <app-vertical-widget-news [news]=\"item\" [theme]=\"'grid'\"></app-vertical-widget-news>\r\n            </li>\r\n            <li class=\"text-center mt-2\"><a class=\"text-danger font-weight-bold\" href=\"#\">See more</a></li>\r\n          </ul>\r\n          <ul [ngClass]=\"{'d-none': onMostRecent}\" class=\"tabs-content\">\r\n            <li *ngFor=\"let item of mostPopular\" class=\"mb-2\">\r\n              <app-vertical-widget-news [news]=\"item\" [theme]=\"'grid'\"></app-vertical-widget-news>\r\n            </li>\r\n            <li class=\"text-center mt-2\"><a class=\"text-danger font-weight-bold\" href=\"#\">See more</a></li>\r\n          </ul>\r\n        </div>\r\n      </div> -->\r\n\r\n    </div>\r\n    <!-- news right -->\r\n    <div class=\"col-md-3 col-12\">\r\n      <ul class=\"right-col-news\">\r\n        <li class=\"mb-3\" *ngFor=\"let item of rightColNews; let i = index\">\r\n          <app-vertical-widget-news [news]=\"item\" [theme]=\"i == 0 ? 'default' : 'cover'\"></app-vertical-widget-news>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/news/news-page/news-page.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/news/news-page/news-page.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".middle-col-news {\n  display: flex;\n  flex-wrap: wrap; }\n  .middle-col-news li {\n    flex-basis: calc(50% - 10px);\n    height: 185px;\n    margin-bottom: 10px;\n    margin-right: 5px;\n    margin-left: 5px; }\n  .middle-col-news li:first-child {\n      flex-basis: 100%;\n      min-height: 230px; }\n  .right-col-news /deep/ .widget.cover {\n  min-height: 13rem; }\n  .middle-section-news {\n  display: flex;\n  margin-right: -15px;\n  margin-left: -15px; }\n  .middle-section-news li {\n    flex-basis: 30%;\n    min-height: 13rem; }\n  .middle-section-news li:first-child {\n      flex-basis: 70%;\n      margin-right: 5px; }\n  .tabs .tabs-header {\n  display: flex; }\n  .tabs .tabs-header li {\n    margin: 0 10px; }\n  .tabs .tabs-header li a {\n      color: #000;\n      font-size: 18px;\n      font-weight: bold;\n      cursor: pointer; }\n  .tabs .tabs-header li a.active {\n        color: red; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy9uZXdzLXBhZ2UvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxuZXdzXFxuZXdzLXBhZ2VcXG5ld3MtcGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixlQUFlLEVBQUE7RUFGakI7SUFLSSw0QkFBNEI7SUFDNUIsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsZ0JBQWdCLEVBQUE7RUFUcEI7TUFZTSxnQkFBZ0I7TUFDaEIsaUJBQWlCLEVBQUE7RUFNdkI7RUFDRSxpQkFBaUIsRUFBQTtFQUduQjtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7RUFIcEI7SUFNSSxlQUFlO0lBQ2YsaUJBQWlCLEVBQUE7RUFQckI7TUFVTSxlQUFlO01BQ2YsaUJBQWlCLEVBQUE7RUFNdkI7RUFFSSxhQUFhLEVBQUE7RUFGakI7SUFLTSxjQUFjLEVBQUE7RUFMcEI7TUFRUSxXQUFXO01BQ1gsZUFBZTtNQUNmLGlCQUFpQjtNQUNqQixlQUFlLEVBQUE7RUFYdkI7UUFjVSxVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9uZXdzL25ld3MtcGFnZS9uZXdzLXBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWlkZGxlLWNvbC1uZXdzIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxuXHJcbiAgbGkge1xyXG4gICAgZmxleC1iYXNpczogY2FsYyg1MCUgLSAxMHB4KTtcclxuICAgIGhlaWdodDogMTg1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG5cclxuICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICBmbGV4LWJhc2lzOiAxMDAlO1xyXG4gICAgICBtaW4taGVpZ2h0OiAyMzBweDtcclxuXHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4ucmlnaHQtY29sLW5ld3MgL2RlZXAvIC53aWRnZXQuY292ZXIge1xyXG4gIG1pbi1oZWlnaHQ6IDEzcmVtO1xyXG59XHJcblxyXG4ubWlkZGxlLXNlY3Rpb24tbmV3cyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBtYXJnaW4tcmlnaHQ6IC0xNXB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTVweDtcclxuXHJcbiAgbGkge1xyXG4gICAgZmxleC1iYXNpczogMzAlO1xyXG4gICAgbWluLWhlaWdodDogMTNyZW07XHJcblxyXG4gICAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgIGZsZXgtYmFzaXM6IDcwJTtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcblxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnRhYnMge1xyXG4gIC50YWJzLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgIGxpIHtcclxuICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcblxyXG4gICAgICBhIHtcclxuICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG5cclxuICAgICAgICAmLmFjdGl2ZSB7XHJcbiAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/news/news-page/news-page.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/news/news-page/news-page.component.ts ***!
  \*******************************************************/
/*! exports provided: NewsPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsPageComponent", function() { return NewsPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_news_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/news.service */ "./src/app/services/news.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




var NewsPageComponent = /** @class */ (function () {
    function NewsPageComponent(newsService, platformId) {
        this.newsService = newsService;
        this.platformId = platformId;
        this.latestNews = [];
        this.centerColNews = [];
        this.rightColNews = [];
        this.middleSection = [];
        this.onMostRecent = true;
    }
    NewsPageComponent.prototype.ngOnInit = function () {
        this.getLatestNews();
        this.getNews();
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_3__["isPlatformBrowser"])(this.platformId)) {
            this.countryID = +localStorage.getItem('countryID');
        }
    };
    NewsPageComponent.prototype.getLatestNews = function () {
        var _this = this;
        var params = { CountryID: this.countryID };
        this.newsService.getLatestNews(params).subscribe(function (data) {
            _this.latestNews = data['Data'];
        });
    };
    ;
    NewsPageComponent.prototype.getNews = function () {
        var _this = this;
        this.loading = true;
        var params = { CountryID: this.countryID };
        this.newsService.getNews(params).subscribe(function (data) {
            _this.loading = false;
            _this.centerColNews = data['Data'].filter(function (news) { return news.BlockTypeID == 1; });
            _this.middleSection = data['Data'].filter(function (news) { return news.BlockTypeID == 2; });
            _this.rightColNews = data['Data'].filter(function (news) { return news.BlockTypeID == 3; });
        });
    };
    ;
    NewsPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-news-page',
            template: __webpack_require__(/*! ./news-page.component.html */ "./src/app/news/news-page/news-page.component.html"),
            styles: [__webpack_require__(/*! ./news-page.component.scss */ "./src/app/news/news-page/news-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_news_service__WEBPACK_IMPORTED_MODULE_2__["NewsService"],
            Object])
    ], NewsPageComponent);
    return NewsPageComponent;
}());



/***/ }),

/***/ "./src/app/news/news-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/news/news-routing.module.ts ***!
  \*********************************************/
/*! exports provided: NewsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsRoutingModule", function() { return NewsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _news_page_news_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./news-page/news-page.component */ "./src/app/news/news-page/news-page.component.ts");
/* harmony import */ var _news_details_news_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./news-details/news-details.component */ "./src/app/news/news-details/news-details.component.ts");





var routes = [
    { path: '', component: _news_page_news_page_component__WEBPACK_IMPORTED_MODULE_3__["NewsPageComponent"] },
    { path: 'news-details', component: _news_details_news_details_component__WEBPACK_IMPORTED_MODULE_4__["NewsDetailsComponent"] }
];
var NewsRoutingModule = /** @class */ (function () {
    function NewsRoutingModule() {
    }
    NewsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], NewsRoutingModule);
    return NewsRoutingModule;
}());



/***/ }),

/***/ "./src/app/news/news.module.ts":
/*!*************************************!*\
  !*** ./src/app/news/news.module.ts ***!
  \*************************************/
/*! exports provided: NewsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsModule", function() { return NewsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _news_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./news-routing.module */ "./src/app/news/news-routing.module.ts");
/* harmony import */ var _vertical_widget_news_vertical_widget_news_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./vertical-widget-news/vertical-widget-news.component */ "./src/app/news/vertical-widget-news/vertical-widget-news.component.ts");
/* harmony import */ var _news_page_news_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./news-page/news-page.component */ "./src/app/news/news-page/news-page.component.ts");
/* harmony import */ var _news_details_news_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./news-details/news-details.component */ "./src/app/news/news-details/news-details.component.ts");
/* harmony import */ var _services_news_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/news.service */ "./src/app/services/news.service.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");









var NewsModule = /** @class */ (function () {
    function NewsModule() {
    }
    NewsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_vertical_widget_news_vertical_widget_news_component__WEBPACK_IMPORTED_MODULE_4__["VerticalWidgetNewsComponent"], _news_page_news_page_component__WEBPACK_IMPORTED_MODULE_5__["NewsPageComponent"], _news_details_news_details_component__WEBPACK_IMPORTED_MODULE_6__["NewsDetailsComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _news_routing_module__WEBPACK_IMPORTED_MODULE_3__["NewsRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"]
            ],
            providers: [_services_news_service__WEBPACK_IMPORTED_MODULE_7__["NewsService"]]
        })
    ], NewsModule);
    return NewsModule;
}());



/***/ }),

/***/ "./src/app/news/vertical-widget-news/vertical-widget-news.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/news/vertical-widget-news/vertical-widget-news.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngSwitch]=\"theme\" style=\"height:100%;\">\r\n  <div *ngSwitchCase=\"'default'\" (click)=\"onClickNews(news.ID)\" class=\"widget border-bottom\">\r\n    <img *ngIf=\"news.ArticleImage\" [src]=\"news.ArticleImage\">\r\n    <iframe *ngIf=\"news.YoutubeLink\" [src]=\"news.YoutubeLink | safe:'resourceUrl'\" frameborder=\"0\" allow=\"fullscreen\"\r\n          allowfullscreen></iframe>\r\n    <h4 class=\"pt-2\"><a [routerLink]=\"['news-details']\" [queryParams]=\"{id:news.ID}\"\r\n        class=\"text-dark\">{{news.Title}}</a></h4>\r\n    <button *ngIf=\"news.ArticleImage\" class=\"plain-btn share-btn\"><img width=\"40\" src=\"assets/images/share-icon.svg\"></button>\r\n    <div class=\"widget-footer d-flex justify-content-between pb-2\">\r\n      <small class=\"text-muted\">{{news.Date}}</small>\r\n      <small class=\"text-muted\">Source: {{news.Source}}</small>\r\n    </div>\r\n  </div>\r\n\r\n  <div *ngSwitchCase=\"'cover'\" (click)=\"onClickNews(news.ID)\" class=\"widget pb-1 cover\" [ngStyle]=\"{'background-image': 'url(' + news.ArticleImage + ')'}\">\r\n      <iframe *ngIf=\"news.YoutubeLink\" [src]=\"news.YoutubeLink | safe:'resourceUrl'\" frameborder=\"0\" allow=\"fullscreen\"\r\n      allowfullscreen></iframe>\r\n    <h4><a [routerLink]=\"['news-details']\" [queryParams]=\"{id:news.ID}\" class=\"text-white\">{{news.Title}}</a></h4>\r\n    <button class=\"plain-btn share-btn\"><img width=\"40\" src=\"assets/images/share-icon.svg\"></button>\r\n    <div class=\"widget-footer d-flex justify-content-between border-top py-1\">\r\n      <small class=\"text-light\">{{news.Date}}</small>\r\n      <small class=\"text-light\">Source: {{news.Source}}</small>\r\n    </div>\r\n  </div>\r\n\r\n  <div *ngSwitchCase=\"'grid'\" class=\"widget border-bottom grid\">\r\n    <img *ngIf=\"news.ArticleImage\" [src]=\"news.ArticleImage\">\r\n    <iframe *ngIf=\"news.YoutubeLink\" [src]=\"news.YoutubeLink | safe:'resourceUrl'\" frameborder=\"0\" allow=\"fullscreen\"\r\n          allowfullscreen></iframe>\r\n    <div class=\"right-section\">\r\n      <h4 class=\"pt-2\"><a [routerLink]=\"['news-details']\" [queryParams]=\"{id:news.ID}\">{{news.Title}}</a></h4>\r\n      <button *ngIf=\"news.ArticleImage\" class=\"plain-btn share-btn\"><img width=\"40\" src=\"assets/images/share-icon.svg\"></button>\r\n      <div class=\"widget-footer d-flex justify-content-between pb-2\">\r\n        <small class=\"text-muted\">{{news.Date}}</small>\r\n        <small class=\"text-muted\">Source: {{news.source}}</small>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/news/vertical-widget-news/vertical-widget-news.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/news/vertical-widget-news/vertical-widget-news.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".widget {\n  position: relative;\n  border-radius: 5px;\n  background: #FFF;\n  height: 100%;\n  cursor: pointer; }\n  .widget h4 > a {\n    font-weight: bold; }\n  .widget > img, .widget > iframe {\n    width: 100%;\n    height: 60%; }\n  .share-btn {\n  position: absolute;\n  top: 10%;\n  right: 5%; }\n  .widget.cover {\n  background-repeat: no-repeat;\n  background-size: cover;\n  min-height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-end;\n  padding: 0 15px; }\n  .widget.cover small,\n  .widget.cover h4 {\n    z-index: 9; }\n  .widget.cover::before {\n    content: \"\";\n    width: 100%;\n    display: inline;\n    height: 100%;\n    position: absolute;\n    top: 0;\n    left: 0;\n    background: linear-gradient(to bottom, rgba(255, 255, 255, 0), rgba(0, 0, 0, 0.7)); }\n  .widget.grid {\n  display: flex; }\n  .widget.grid h4 > a {\n    font-weight: bold;\n    color: #000; }\n  .widget.grid > * {\n    margin: 0 10px;\n    flex-basis: 70%; }\n  .widget.grid > *:first-child {\n      flex-basis: 30%; }\n  .widget.grid .right-section {\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between; }\n  .widget.grid .share-btn {\n    position: absolute;\n    top: 10%;\n    left: 5%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy92ZXJ0aWNhbC13aWRnZXQtbmV3cy9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXG5ld3NcXHZlcnRpY2FsLXdpZGdldC1uZXdzXFx2ZXJ0aWNhbC13aWRnZXQtbmV3cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixlQUFlLEVBQUE7RUFMakI7SUFPSSxpQkFBaUIsRUFBQTtFQVByQjtJQVdJLFdBQVc7SUFDWCxXQUFXLEVBQUE7RUFJZjtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FDRixFQUFBO0VBRUE7RUFDRSw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHlCQUF5QjtFQUN6QixlQUFlLEVBQUE7RUFQakI7O0lBV0ksVUFBVSxFQUFBO0VBWGQ7SUFlSSxXQUFXO0lBQ1gsV0FBVztJQUNYLGVBQWU7SUFDZixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixPQUFPO0lBQ1Asa0ZBQWlGLEVBQUE7RUFJckY7RUFDRSxhQUFhLEVBQUE7RUFEZjtJQUlJLGlCQUFpQjtJQUNqQixXQUFXLEVBQUE7RUFMZjtJQWFJLGNBQWM7SUFDZCxlQUFlLEVBQUE7RUFkbkI7TUFVTSxlQUFlLEVBQUE7RUFWckI7SUFrQkksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEIsRUFBQTtFQXBCbEM7SUF3Qkksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixRQUFRLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9uZXdzL3ZlcnRpY2FsLXdpZGdldC1uZXdzL3ZlcnRpY2FsLXdpZGdldC1uZXdzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndpZGdldCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBiYWNrZ3JvdW5kOiAjRkZGO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgaDQ+YSB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcblxyXG4gID5pbWcsID5pZnJhbWUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDYwJTtcclxuICB9XHJcbn1cclxuXHJcbi5zaGFyZS1idG4ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDEwJTtcclxuICByaWdodDogNSVcclxufVxyXG5cclxuLndpZGdldC5jb3ZlciB7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIG1pbi1oZWlnaHQ6IDEwMCU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgcGFkZGluZzogMCAxNXB4O1xyXG5cclxuICBzbWFsbCxcclxuICBoNCB7XHJcbiAgICB6LWluZGV4OiA5O1xyXG4gIH1cclxuXHJcbiAgJjo6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCByZ2JhKDI1NSwgMjU1LCAyNTUsIDApLCByZ2JhKDAsIDAsIDAsIC43KSk7XHJcbiAgfVxyXG59XHJcblxyXG4ud2lkZ2V0LmdyaWQge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcblxyXG4gIGg0PmEge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICB9XHJcblxyXG4gID4qIHtcclxuICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICBmbGV4LWJhc2lzOiAzMCU7XHJcbiAgICB9XHJcblxyXG4gICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICBmbGV4LWJhc2lzOiA3MCU7XHJcbiAgfVxyXG5cclxuICAucmlnaHQtc2VjdGlvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB9XHJcblxyXG4gIC5zaGFyZS1idG4ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxMCU7XHJcbiAgICBsZWZ0OiA1JTtcclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/news/vertical-widget-news/vertical-widget-news.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/news/vertical-widget-news/vertical-widget-news.component.ts ***!
  \*****************************************************************************/
/*! exports provided: VerticalWidgetNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerticalWidgetNewsComponent", function() { return VerticalWidgetNewsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var VerticalWidgetNewsComponent = /** @class */ (function () {
    function VerticalWidgetNewsComponent(router) {
        this.router = router;
    }
    VerticalWidgetNewsComponent.prototype.ngOnInit = function () {
    };
    VerticalWidgetNewsComponent.prototype.onClickNews = function (newsId) {
        this.router.navigate(['/news/news-details'], { queryParams: { id: newsId } });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], VerticalWidgetNewsComponent.prototype, "news", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], VerticalWidgetNewsComponent.prototype, "theme", void 0);
    VerticalWidgetNewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-vertical-widget-news',
            template: __webpack_require__(/*! ./vertical-widget-news.component.html */ "./src/app/news/vertical-widget-news/vertical-widget-news.component.html"),
            styles: [__webpack_require__(/*! ./vertical-widget-news.component.scss */ "./src/app/news/vertical-widget-news/vertical-widget-news.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], VerticalWidgetNewsComponent);
    return VerticalWidgetNewsComponent;
}());



/***/ }),

/***/ "./src/app/services/news.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/news.service.ts ***!
  \******************************************/
/*! exports provided: NewsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsService", function() { return NewsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api */ "./src/app/api.ts");



// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

var NewsService = /** @class */ (function () {
    function NewsService(http, proxy) {
        this.http = http;
        this.proxy = proxy;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
            'Access-Control-Allow-Headers': "*",
            'Access-Control-Allow-Credentials': 'true',
        });
    }
    // get news
    NewsService.prototype.getNews = function (params) {
        return this.http.get(this.proxy.api + 'News/GetNews', { headers: this.headers, params: params });
    };
    // get latest news
    NewsService.prototype.getLatestNews = function (params) {
        return this.http.get(this.proxy.api + 'News/GetLatestNews', { headers: this.headers, params: params });
    };
    // get news details
    NewsService.prototype.getNewsDetails = function (params) {
        return this.http.get(this.proxy.api + 'News/GetNewsDetails', { headers: this.headers, params: params });
    };
    NewsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _api__WEBPACK_IMPORTED_MODULE_3__["ProxyUrl"]])
    ], NewsService);
    return NewsService;
}());



/***/ })

}]);
//# sourceMappingURL=news-news-module.js.map
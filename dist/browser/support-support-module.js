(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["support-support-module"],{

/***/ "./src/app/services/support.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/support.service.ts ***!
  \*********************************************/
/*! exports provided: SupportService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportService", function() { return SupportService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api */ "./src/app/api.ts");



// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

var SupportService = /** @class */ (function () {
    function SupportService(http, proxy) {
        this.http = http;
        this.proxy = proxy;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
            'Access-Control-Allow-Headers': "*",
            'Access-Control-Allow-Credentials': 'true',
        });
        this.headersToUpload = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'multipart/form-data',
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
            'Access-Control-Allow-Headers': "*",
            'Access-Control-Allow-Credentials': 'true',
        });
    }
    // get downloads
    SupportService.prototype.getDownloads = function (keyword) {
        if (keyword === void 0) { keyword = ''; }
        return this.http.get(this.proxy.api + 'Support/GetDownloads?Keyword=' + keyword, { headers: this.headers });
    };
    // get most popular faqs
    SupportService.prototype.getMostPopularFAQs = function () {
        return this.http.get(this.proxy.api + 'Support/GetMostPopularFAQs', { headers: this.headers });
    };
    // get faqs
    SupportService.prototype.getFaqs = function (keyword) {
        if (keyword === void 0) { keyword = ''; }
        return this.http.get(this.proxy.api + 'Support/GetFAQs?Keyword=' + keyword, { headers: this.headers });
    };
    // get contact info
    SupportService.prototype.getContactInfo = function () {
        return this.http.get(this.proxy.api + 'Support/GetContactInfo', { headers: this.headers });
    };
    // get videos
    SupportService.prototype.getVideos = function (keyword) {
        if (keyword === void 0) { keyword = ''; }
        return this.http.get(this.proxy.api + 'Support/GetVideos?Keyword=' + keyword, { headers: this.headers });
    };
    // get most popular videos
    SupportService.prototype.getMostPopularVideos = function () {
        return this.http.get(this.proxy.api + 'Support/GetMostPopularVideos', { headers: this.headers });
    };
    // send email
    SupportService.prototype.sendEmail = function (request, files) {
        return this.http.post(this.proxy.api + 'Emailing/SaveEmail', { request: request, files: files }, { headers: this.headers });
    };
    // get product id
    SupportService.prototype.getProductId = function (keyword) {
        if (keyword === void 0) { keyword = ''; }
        return this.http.get(this.proxy.api + 'Emailing/GetProducts?Keyword=' + keyword, { headers: this.headers });
    };
    SupportService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _api__WEBPACK_IMPORTED_MODULE_3__["ProxyUrl"]])
    ], SupportService);
    return SupportService;
}());



/***/ }),

/***/ "./src/app/support/downloads/downloads.component.html":
/*!************************************************************!*\
  !*** ./src/app/support/downloads/downloads.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n  <h1>Downloads\r\n    <div class=\"float-right mx-3 search\">\r\n      <i class=\"fa fa-search\"></i>\r\n      <input type=\"text\" placeholder=\"Search in Downloads …\" (keyup)=\"debounceTime(getDownloads, 800)\"\r\n        [(ngModel)]=\"downloadName\" name=\"downloadName\" class=\"form-control\">\r\n    </div>\r\n    <!-- <div class=\"float-right\">\r\n      <select class=\"form-control\">\r\n        <option>Sort by</option>\r\n      </select>\r\n    </div> -->\r\n    <div class=\"clearfix\"></div>\r\n  </h1>\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n  <div class=\"alert alert-danger mx-3\" *ngIf=\"onErr\">Something went wrong! please try again!</div>\r\n  <div class=\"alert alert-danger mx-3\" *ngIf=\"noResult\">Sorry! we can't find what you are looking for!</div>\r\n\r\n  <ul class=\"px-3\">\r\n    <li class=\"d-flex my-3 download-list-item\" *ngFor=\"let download of downloads\">\r\n      <div class=\"thumbnail\">\r\n        <img [src]=\"download.ProductImage\" />\r\n      </div>\r\n      <div class=\"item-details mx-3 py-3\">\r\n        <h4>{{download.Name}}</h4>\r\n        <h5>{{download.Code}}</h5>\r\n      </div>\r\n      <div class=\"downloads\">\r\n        <ul>\r\n          <li *ngFor=\"let file of download.Downloads\" class=\"mb-3\">\r\n            <a [href]=\"file.PDF\" target=\"_blank\">\r\n              <img width=\"40\" src=\"assets/images/pdf-ico.svg\">\r\n              <div class=\"mx-2\">\r\n                <h4 class=\"m-0\">{{file.FileName}}</h4>\r\n                <h5 class=\"text-muted m-0\">{{file.description}}</h5>\r\n                <h5 class=\"text-muted\">{{file.type}}</h5>\r\n              </div>\r\n            </a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/support/downloads/downloads.component.scss":
/*!************************************************************!*\
  !*** ./src/app/support/downloads/downloads.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "section > h1 {\n  background: #bcc0c7;\n  color: #FFF;\n  font-size: 20px;\n  padding: 5px 15px;\n  line-height: 38px; }\n  section > h1 .search {\n    display: flex;\n    align-items: center;\n    background: #FFF;\n    color: #ccc;\n    padding: 0 10px;\n    border-radius: 3px; }\n  section > h1 .search input {\n      border: 0; }\n  section .downloads {\n  margin-left: auto;\n  width: 44%; }\n  section .downloads ul li a {\n    display: flex;\n    align-items: center; }\n  .download-list-item .thumbnail img {\n  max-width: 200px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VwcG9ydC9kb3dubG9hZHMvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxzdXBwb3J0XFxkb3dubG9hZHNcXGRvd25sb2Fkcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTtFQU5yQjtJQVNNLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysa0JBQWtCLEVBQUE7RUFkeEI7TUFpQlEsU0FBUyxFQUFBO0VBakJqQjtFQXVCSSxpQkFBaUI7RUFDakIsVUFBVSxFQUFBO0VBeEJkO0lBNEJVLGFBQWE7SUFDYixtQkFDRixFQUFBO0VBTVI7RUFHTSxnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3N1cHBvcnQvZG93bmxvYWRzL2Rvd25sb2Fkcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNlY3Rpb24ge1xyXG4gID5oMSB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYmNjMGM3O1xyXG4gICAgY29sb3I6ICNGRkY7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzOHB4O1xyXG5cclxuICAgIC5zZWFyY2gge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjRkZGO1xyXG4gICAgICBjb2xvcjogI2NjYztcclxuICAgICAgcGFkZGluZzogMCAxMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcblxyXG4gICAgICBpbnB1dCB7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuZG93bmxvYWRzIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgd2lkdGg6IDQ0JTtcclxuICAgIHVsIHtcclxuICAgICAgbGkge1xyXG4gICAgICAgIGEge1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5kb3dubG9hZC1saXN0LWl0ZW0ge1xyXG4gIC50aHVtYm5haWwge1xyXG4gICAgaW1nIHtcclxuICAgICAgbWF4LXdpZHRoOiAyMDBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/support/downloads/downloads.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/support/downloads/downloads.component.ts ***!
  \**********************************************************/
/*! exports provided: DownloadsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadsComponent", function() { return DownloadsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_support_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/support.service */ "./src/app/services/support.service.ts");



var DownloadsComponent = /** @class */ (function () {
    function DownloadsComponent(supportService) {
        var _this = this;
        this.supportService = supportService;
        this.downloadName = '';
        this.downloads = [];
        this.getDownloads = function () {
            _this.downloads = [];
            _this.onErr = false;
            _this.loading = true;
            _this.noResult = false;
            _this.supportService.getDownloads(_this.downloadName).subscribe(function (downloads) {
                _this.loading = false;
                _this.downloads = downloads.Data;
                if (downloads.Data.length == 0)
                    _this.noResult = true;
            }, function (err) {
                _this.loading = false;
                _this.onErr = true;
            });
        };
    }
    DownloadsComponent.prototype.ngOnInit = function () {
        this.getDownloads();
    };
    DownloadsComponent.prototype.debounceTime = function (fn, time) {
        clearTimeout(this.debounceTimeOut);
        this.debounceTimeOut = setTimeout(fn, time);
    };
    DownloadsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-downloads',
            template: __webpack_require__(/*! ./downloads.component.html */ "./src/app/support/downloads/downloads.component.html"),
            styles: [__webpack_require__(/*! ./downloads.component.scss */ "./src/app/support/downloads/downloads.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_support_service__WEBPACK_IMPORTED_MODULE_2__["SupportService"]])
    ], DownloadsComponent);
    return DownloadsComponent;
}());



/***/ }),

/***/ "./src/app/support/email/email.component.html":
/*!****************************************************!*\
  !*** ./src/app/support/email/email.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n  <h1>Email</h1>\r\n\r\n  <div class=\"alert alert-danger\" *ngIf=\"onErr\">\r\n    <p *ngFor=\"let err of errors\">{{err}}</p>\r\n  </div>\r\n  <div class=\"alert alert-success\" *ngIf=\"onSuccess\">{{successMsg}}</div>\r\n  <form autocomplete=\"off\" #emailForm=\"ngForm\" (ngSubmit)=\"sendEmail(emailForm)\" class=\"my-3 px-4\">\r\n    <p class=\"my-4 font-weight-bold\">It is empty here right now, please try again later!</p>\r\n    <div class=\"row\">\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Full Name <span class=\"text-danger\">*</span></label>\r\n        <input type=\"text\" placeholder=\"Full name\" [(ngModel)]=\"emailFormModel.FullName\" name=\"FullName\"\r\n          class=\"form-control\">\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Phone Number <span class=\"text-danger\">*</span></label>\r\n        <input type=\"number\" placeholder=\"Phone Number\" [(ngModel)]=\"emailFormModel.PhoneNumber\" name=\"PhoneNumber\"\r\n          class=\"form-control\">\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Subject <span class=\"text-danger\">*</span></label>\r\n        <input type=\"text\" placeholder=\"Subject\" class=\"form-control\" [(ngModel)]=\"emailFormModel.Subject\"\r\n          name=\"Subject\">\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Email Address <span class=\"text-danger\">*</span></label>\r\n        <input type=\"text\" placeholder=\"Email Address\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\" required\r\n          class=\"form-control\" [(ngModel)]=\"emailFormModel.EmailAddress\" name=\"EmailAddress\">\r\n        <div class=\"has-error\">Invalid email address</div>\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Model Number</label>\r\n        <input type=\"text\" placeholder=\"Model Number\" class=\"form-control\" (blur)=\"resetAutoComplete('blur')\" (focus)=\"resetAutoComplete()\"\r\n          (keyup)=\"debounceTime(onModelEnter, 500)\" [(ngModel)]=\"emailFormModel.ProductName\" name=\"ProductName\">\r\n        <div class=\"autocomplete-list\" *ngIf=\"onCompleteList\">\r\n          <ul>\r\n            <li *ngFor=\"let product of modelsList\"><button type=\"button\" class=\"btn btn-link\"\r\n                (click)=\"onSelect(product)\">{{product.Name}}</button></li>\r\n            <li *ngIf=\"noResult\">No Result Found</li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group col-md-6 col-12\">\r\n        <label>Serial Number</label>\r\n        <input type=\"text\" placeholder=\"Serial Number\" class=\"form-control\" [(ngModel)]=\"emailFormModel.SerialNumber\"\r\n          name=\"SerialNumber\">\r\n      </div>\r\n      <div class=\"form-group col-12\">\r\n        <label>Message <span class=\"text-danger\">*</span></label>\r\n        <textarea placeholder=\"Message\" class=\"form-control\" [(ngModel)]=\"emailFormModel.Message\"\r\n          name=\"Message\"></textarea>\r\n      </div>\r\n\r\n      <div class=\"form-group col-12\">\r\n        <label>Attachment</label>\r\n        <div class=\"input-group mb-3\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\" id=\"inputGroupFileAddon01\">\r\n              <i class=\"fa fa-paperclip\" aria-hidden=\"true\"></i></span>\r\n          </div>\r\n          <div class=\"custom-file\">\r\n            <input type=\"file\" class=\"custom-file-input\" #myInput id=\"file\" (change)=\"handleFileInput($event)\"\r\n              aria-describedby=\"inputGroupFileAddon01\">\r\n            <label class=\"custom-file-label\" for=\"inputGroupFile01\">{{fileToUpload?.name || 'Choose file'}}</label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group col-12 text-center\">\r\n        <button [disabled]=\"loading\" type=\"submit\" class=\"w-25 py-2 primary-btn font-weight-bold\">\r\n          <app-spinner [theme]=\"'text-light'\" [type]=\"'inline'\" *ngIf=\"loading\"></app-spinner> Submit\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/support/email/email.component.scss":
/*!****************************************************!*\
  !*** ./src/app/support/email/email.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "section > h1 {\n  background: #bcc0c7;\n  color: #FFF;\n  font-size: 20px;\n  padding: 5px 15px;\n  line-height: 38px; }\n\n.autocomplete-list {\n  position: relative;\n  background: #FFF;\n  padding: 10px;\n  z-index: 99;\n  bottom: -4px;\n  box-shadow: 0 0 1px #444;\n  border-radius: 2px;\n  max-height: 200px;\n  overflow-y: scroll; }\n\n.autocomplete-list ul li a {\n    display: block;\n    padding: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VwcG9ydC9lbWFpbC9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXHN1cHBvcnRcXGVtYWlsXFxlbWFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFJckI7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixXQUFXO0VBQ1gsWUFBWTtFQUNaLHdCQUF3QjtFQUN4QixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBOztBQVRwQjtJQWFRLGNBQWM7SUFDZCxZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zdXBwb3J0L2VtYWlsL2VtYWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic2VjdGlvbiB7XHJcbiAgPmgxIHtcclxuICAgIGJhY2tncm91bmQ6ICNiY2MwYzc7XHJcbiAgICBjb2xvcjogI0ZGRjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDM4cHg7XHJcbiAgfVxyXG59XHJcblxyXG4uYXV0b2NvbXBsZXRlLWxpc3Qge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBiYWNrZ3JvdW5kOiAjRkZGO1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgei1pbmRleDogOTk7XHJcbiAgYm90dG9tOiAtNHB4O1xyXG4gIGJveC1zaGFkb3c6IDAgMCAxcHggIzQ0NDtcclxuICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gIHVsIHtcclxuICAgIGxpIHtcclxuICAgICAgYSB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/support/email/email.component.ts":
/*!**************************************************!*\
  !*** ./src/app/support/email/email.component.ts ***!
  \**************************************************/
/*! exports provided: EmailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailComponent", function() { return EmailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");
/* harmony import */ var _services_support_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/support.service */ "./src/app/services/support.service.ts");




var EmailComponent = /** @class */ (function () {
    function EmailComponent(supportService) {
        var _this = this;
        this.supportService = supportService;
        this.emailFormModel = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_2__["EmailModel"]();
        this.errors = [];
        this.fileToUpload = null;
        this.modelsList = [];
        this.onModelEnter = function () {
            _this.onCompleteList = true;
            _this.noResult = false;
            _this.supportService.getProductId(_this.emailFormModel.ProductName).subscribe(function (products) {
                _this.modelsList = products.Data;
                if (products.Data.length == 0) {
                    _this.noResult = true;
                }
            });
        };
    }
    EmailComponent.prototype.ngOnInit = function () {
    };
    EmailComponent.prototype.sendEmail = function (emailForm) {
        var _this = this;
        this.onErr = false;
        this.onSuccess = false;
        this.loading = true;
        var obj = {
            "FullName": this.emailFormModel.FullName,
            "Subject": this.emailFormModel.Subject,
            "ProductID": this.emailFormModel.ProductID,
            "Message": this.emailFormModel.Message,
            "PhoneNumber": this.emailFormModel.PhoneNumber,
            "EmailAddress": this.emailFormModel.EmailAddress,
            "SerialNumber": this.emailFormModel.SerialNumber
        };
        this.supportService.sendEmail(JSON.stringify(obj), this.fileToUpload).subscribe(function (data) {
            if (data.Status) {
                _this.onSuccess = true;
                _this.successMsg = "Email has been sent successfully!";
                _this.loading = false;
                emailForm.reset();
                _this.fileToUpload = null;
                _this.myInputVariable.nativeElement.value = "";
            }
            else {
                _this.onErr = true;
                _this.loading = false;
                _this.errors = data['Exceptions'];
                _this.fileToUpload = null;
                _this.myInputVariable.nativeElement.value = "";
            }
        }, function (err) {
            _this.loading = false;
            _this.onErr = true;
            _this.errors = err.statusText;
        });
    };
    EmailComponent.prototype.handleFileInput = function (e) {
        this.fileToUpload = e.target.files.item(0);
        var elem = e.target || e.srcElement;
        this.uploadedFile = new FormData();
        this.uploadedFile.append('file', elem.files[0]);
    };
    EmailComponent.prototype.debounceTime = function (fn, time) {
        clearTimeout(this.debounceTimeOut);
        this.debounceTimeOut = setTimeout(fn, time);
    };
    EmailComponent.prototype.onSelect = function (product) {
        var _this = this;
        this.emailFormModel.ProductID = product.ID;
        this.emailFormModel.ProductName = product.Name;
        setTimeout(function () {
            _this.onCompleteList = false;
        }, 100);
    };
    EmailComponent.prototype.resetAutoComplete = function (type) {
        var _this = this;
        if (type === 'blur') {
            setTimeout(function () {
                _this.onCompleteList = false;
            }, 200);
        }
        else {
            this.emailFormModel.ProductID = '';
            this.emailFormModel.ProductName = '';
            this.onCompleteList = false;
            this.noResult = false;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myInput'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], EmailComponent.prototype, "myInputVariable", void 0);
    EmailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-email',
            template: __webpack_require__(/*! ./email.component.html */ "./src/app/support/email/email.component.html"),
            styles: [__webpack_require__(/*! ./email.component.scss */ "./src/app/support/email/email.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_support_service__WEBPACK_IMPORTED_MODULE_3__["SupportService"]])
    ], EmailComponent);
    return EmailComponent;
}());



/***/ }),

/***/ "./src/app/support/faqs/faqs.component.html":
/*!**************************************************!*\
  !*** ./src/app/support/faqs/faqs.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n  <h1>Most Popular Questions</h1>\r\n  <app-spinner *ngIf=\"loading1\"></app-spinner>\r\n  <div class=\"panel-group mb-5 px-3\" id=\"mostPopular\" role=\"tablist\" aria-multiselectable=\"true\">\r\n    <div *ngFor=\"let panel of mostPopular;let i = index\" class=\"panel panel-default\">\r\n      <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">\r\n        <h4 class=\"panel-title\">\r\n          <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#mostPopular\" [href]=\"'#collapse' + i\"\r\n            [attr.aria-expanded]=\"true ? i == 0 : false\" [attr.aria-controls]=\"'collapse'+i\">\r\n            {{panel.Question}}\r\n          </a>\r\n        </h4>\r\n      </div>\r\n      <div [id]=\"'collapse' + i\" class=\"panel-collapse collapse\" [ngClass]=\"{'in show': i == 0}\" role=\"tabpanel\"\r\n        [attr.aria-labelledby]=\"'heading'+i\">\r\n        <div class=\"panel-body\">\r\n          {{panel.Answer}}\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</section>\r\n<section>\r\n  <h1>FAQS\r\n    <div class=\"float-right mx-3 search\">\r\n      <i class=\"fa fa-search\"></i>\r\n      <input type=\"text\" placeholder=\"Search in FAQs …\" class=\"form-control\" [(ngModel)]=\"faqName\" name=\"faqName\" (keyup)=\"debounceTime(getFaqs, 800)\">\r\n    </div>\r\n    <!-- <div class=\"float-right\">\r\n      <select class=\"form-control\">\r\n        <option>Sort by</option>\r\n      </select>\r\n    </div> -->\r\n    <div class=\"clearfix\"></div>\r\n  </h1>\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n  <div class=\"alert alert-info shadow border\" *ngIf=\"noResult\">Sorry! we cannot find what you are looking for...</div>\r\n  <div class=\"panel-group mb-5 px-3\" id=\"faqs\" role=\"tablist\" aria-multiselectable=\"true\">\r\n    <div *ngFor=\"let panel of faqs;let i = index\" class=\"panel panel-default\">\r\n      <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">\r\n        <h4 class=\"panel-title\">\r\n          <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#faqs\" [href]=\"'#collapseFaqs' + i\" aria-expanded=\"false\"\r\n            [attr.aria-controls]=\"'collapse'+i\">\r\n            {{panel.Question}}\r\n          </a>\r\n        </h4>\r\n      </div>\r\n      <div [id]=\"'collapseFaqs' + i\" class=\"panel-collapse collapse\" role=\"tabpanel\"\r\n        [attr.aria-labelledby]=\"'heading'+i\">\r\n        <div class=\"panel-body\">\r\n          {{panel.Answer}}\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/support/faqs/faqs.component.scss":
/*!**************************************************!*\
  !*** ./src/app/support/faqs/faqs.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-default > .panel-heading {\n  color: #333;\n  background-color: #fff;\n  border-color: #e4e5e7;\n  padding: 0;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  min-height: 40px; }\n\n.panel-default > .panel-heading a {\n  display: block;\n  padding: 0;\n  font-size: 16px;\n  font-weight: 600;\n  color: #000000;\n  position: relative; }\n\n.panel-default > .panel-heading a:before {\n  content: \"\";\n  position: absolute;\n  top: 1px;\n  display: inline-block;\n  font: normal normal normal 14px/1 FontAwesome;\n  font-style: normal;\n  font-weight: bold;\n  color: red;\n  margin-right: 10px;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  transition: -webkit-transform .25s linear;\n  transition: transform .25s linear;\n  transition: transform .25s linear, -webkit-transform .25s linear;\n  -webkit-transition: -webkit-transform .25s linear;\n  line-height: 26px;\n  font-size: 15px;\n  border: 1px solid;\n  right: 10px;\n  width: 30px;\n  height: 30px;\n  border-radius: 100%;\n  text-align: center; }\n\n.panel-default > .panel-heading a[aria-expanded=\"true\"] {\n  background-color: #fff; }\n\n.panel-default > .panel-heading a[aria-expanded=\"true\"]:before {\n  content: \"\\f107\";\n  -webkit-transform: rotate(180deg);\n  transform: rotate(180deg); }\n\n.panel-default > .panel-heading a[aria-expanded=\"false\"]:before {\n  content: \"\\f106\";\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg); }\n\n.accordion-option {\n  width: 100%;\n  float: left;\n  clear: both;\n  margin: 15px 0; }\n\n.accordion-option .title {\n  font-size: 20px;\n  font-weight: bold;\n  float: left;\n  padding: 0;\n  margin: 0; }\n\n.accordion-option .toggle-accordion {\n  float: right;\n  font-size: 16px;\n  color: #6a6c6f; }\n\n.panel-title {\n  margin: 0; }\n\n.panel-body {\n  padding: 0px 10px 10px; }\n\n.panel-body ul li {\n    font-size: 14px;\n    font-weight: 500;\n    color: #838a93; }\n\n.panel-body ul li label {\n      width: 40%;\n      font-weight: bold; }\n\n@media (max-width: 500px) {\n      .panel-body ul li label {\n        width: 50%; } }\n\nsection > h1 {\n  background: #bcc0c7;\n  color: #FFF;\n  font-size: 20px;\n  padding: 5px 15px;\n  line-height: 38px; }\n\nsection > h1 .search {\n    display: flex;\n    align-items: center;\n    background: #FFF;\n    color: #ccc;\n    padding: 0 10px;\n    border-radius: 3px; }\n\nsection > h1 .search input {\n      border: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VwcG9ydC9mYXFzL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcc3VwcG9ydFxcZmFxc1xcZmFxcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVc7RUFDWCxzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLFVBQVU7RUFDVix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsY0FBYztFQUNkLFVBQVU7RUFDVixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixxQkFBcUI7RUFDckIsNkNBQTZDO0VBQzdDLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixtQ0FBbUM7RUFDbkMsa0NBQWtDO0VBQ2xDLHlDQUF5QztFQUN6QyxpQ0FBaUM7RUFDakMsZ0VBQWdFO0VBQ2hFLGlEQUFpRDtFQUNqRCxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsV0FBVztFQUNYLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0Usc0JBQXNCLEVBQUE7O0FBR3hCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGlDQUFpQztFQUNqQyx5QkFBeUIsRUFBQTs7QUFHM0I7RUFDRSxnQkFBZ0I7RUFDaEIsZ0NBQWdDO0VBQ2hDLHdCQUF3QixFQUFBOztBQUcxQjtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsV0FBVztFQUNYLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxVQUFVO0VBQ1YsU0FBUyxFQUFBOztBQUdYO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsU0FBUyxFQUFBOztBQUdYO0VBQ0Usc0JBQXNCLEVBQUE7O0FBRHhCO0lBS00sZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixjQUFjLEVBQUE7O0FBUHBCO01BVVEsVUFBVTtNQUNWLGlCQUFpQixFQUFBOztBQUduQjtNQWROO1FBZ0JVLFVBQVUsRUFBQSxFQUNYOztBQU1UO0VBRUksbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQU5yQjtJQVNNLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysa0JBQWtCLEVBQUE7O0FBZHhCO01BaUJRLFNBQVMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3N1cHBvcnQvZmFxcy9mYXFzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBhbmVsLWRlZmF1bHQ+LnBhbmVsLWhlYWRpbmcge1xyXG4gIGNvbG9yOiAjMzMzO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgYm9yZGVyLWNvbG9yOiAjZTRlNWU3O1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxuICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICBtaW4taGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG4ucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwYWRkaW5nOiAwO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLnBhbmVsLWRlZmF1bHQ+LnBhbmVsLWhlYWRpbmcgYTpiZWZvcmUge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMXB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBmb250OiBub3JtYWwgbm9ybWFsIG5vcm1hbCAxNHB4LzEgRm9udEF3ZXNvbWU7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGNvbG9yOiByZWQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xyXG4gIC1tb3otb3N4LWZvbnQtc21vb3RoaW5nOiBncmF5c2NhbGU7XHJcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjI1cyBsaW5lYXI7XHJcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC4yNXMgbGluZWFyO1xyXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuMjVzIGxpbmVhciwgLXdlYmtpdC10cmFuc2Zvcm0gLjI1cyBsaW5lYXI7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAuMjVzIGxpbmVhcjtcclxuICBsaW5lLWhlaWdodDogMjZweDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgcmlnaHQ6IDEwcHg7XHJcbiAgd2lkdGg6IDMwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhW2FyaWEtZXhwYW5kZWQ9XCJ0cnVlXCJdIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4ucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhW2FyaWEtZXhwYW5kZWQ9XCJ0cnVlXCJdOmJlZm9yZSB7XHJcbiAgY29udGVudDogXCJcXGYxMDdcIjtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcclxufVxyXG5cclxuLnBhbmVsLWRlZmF1bHQ+LnBhbmVsLWhlYWRpbmcgYVthcmlhLWV4cGFuZGVkPVwiZmFsc2VcIl06YmVmb3JlIHtcclxuICBjb250ZW50OiBcIlxcZjEwNlwiO1xyXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xyXG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcclxufVxyXG5cclxuLmFjY29yZGlvbi1vcHRpb24ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIGNsZWFyOiBib3RoO1xyXG4gIG1hcmdpbjogMTVweCAwO1xyXG59XHJcblxyXG4uYWNjb3JkaW9uLW9wdGlvbiAudGl0bGUge1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmbG9hdDogbGVmdDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG5cclxuLmFjY29yZGlvbi1vcHRpb24gLnRvZ2dsZS1hY2NvcmRpb24ge1xyXG4gIGZsb2F0OiByaWdodDtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgY29sb3I6ICM2YTZjNmY7XHJcbn1cclxuXHJcbi5wYW5lbC10aXRsZSB7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4ucGFuZWwtYm9keSB7XHJcbiAgcGFkZGluZzogMHB4IDEwcHggMTBweDtcclxuXHJcbiAgdWwge1xyXG4gICAgbGkge1xyXG4gICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIGNvbG9yOiAjODM4YTkzO1xyXG5cclxuICAgICAgbGFiZWwge1xyXG4gICAgICAgIHdpZHRoOiA0MCU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIEBtZWRpYShtYXgtd2lkdGg6NTAwcHgpIHtcclxuICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuc2VjdGlvbiB7XHJcbiAgPmgxIHtcclxuICAgIGJhY2tncm91bmQ6ICNiY2MwYzc7XHJcbiAgICBjb2xvcjogI0ZGRjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDM4cHg7XHJcblxyXG4gICAgLnNlYXJjaCB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNGRkY7XHJcbiAgICAgIGNvbG9yOiAjY2NjO1xyXG4gICAgICBwYWRkaW5nOiAwIDEwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuXHJcbiAgICAgIGlucHV0IHtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/support/faqs/faqs.component.ts":
/*!************************************************!*\
  !*** ./src/app/support/faqs/faqs.component.ts ***!
  \************************************************/
/*! exports provided: FaqsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqsComponent", function() { return FaqsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_support_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/support.service */ "./src/app/services/support.service.ts");



var FaqsComponent = /** @class */ (function () {
    function FaqsComponent(supportService) {
        var _this = this;
        this.supportService = supportService;
        this.mostPopular = [];
        this.faqs = [];
        this.faqName = '';
        this.getFaqs = function () {
            _this.faqs = [];
            _this.onErr = false;
            _this.loading = true;
            _this.noResult = false;
            _this.supportService.getFaqs(_this.faqName).subscribe(function (faqs) {
                _this.loading = false;
                _this.faqs = faqs.Data;
                if (faqs.Data.length == 0)
                    _this.noResult = true;
            }, function (err) {
                _this.loading = false;
                _this.onErr = true;
            });
        };
    }
    FaqsComponent.prototype.ngOnInit = function () {
        this.getFaqs();
        this.getMostPopularFaqs();
    };
    FaqsComponent.prototype.getMostPopularFaqs = function () {
        var _this = this;
        this.loading1 = true;
        this.supportService.getMostPopularFAQs().subscribe(function (data) {
            _this.mostPopular = data.Data;
            _this.loading1 = false;
        });
    };
    FaqsComponent.prototype.debounceTime = function (fn, time) {
        clearTimeout(this.debounceTimeOut);
        this.debounceTimeOut = setTimeout(fn, time);
    };
    FaqsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-faqs',
            template: __webpack_require__(/*! ./faqs.component.html */ "./src/app/support/faqs/faqs.component.html"),
            styles: [__webpack_require__(/*! ./faqs.component.scss */ "./src/app/support/faqs/faqs.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_support_service__WEBPACK_IMPORTED_MODULE_2__["SupportService"]])
    ], FaqsComponent);
    return FaqsComponent;
}());



/***/ }),

/***/ "./src/app/support/phone/phone.component.html":
/*!****************************************************!*\
  !*** ./src/app/support/phone/phone.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n  <h1>Call Us</h1>\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n\r\n  <div class=\"info px-3\">\r\n    <h4 class=\"my-3\" [innerHtml]=\"contactInfo[0]?.Value | safe:'html'\"></h4>\r\n    <p class=\"text-muted\">\r\n      {{contactInfo[1]?.Key}} : <span\r\n        [innerHtml]=\"contactInfo[1]?.Value | safe:'html'\">{{contactInfo[1]?.Value}}</span><br>\r\n      {{contactInfo[2]?.Key}} : <span [innerHtml]=\"contactInfo[2]?.Value | safe:'html'\">{{contactInfo[1]?.Value}}</span>\r\n      <!-- {{contactInfo[3]?.Key}} : <span [innerHtml]=\"contactInfo[3]?.Value | safe:'html'\">{{contactInfo[1]?.Value}}</span> -->\r\n    </p>\r\n  </div>\r\n  <div class=\"my-3\">\r\n    <agm-map [latitude]=\"lat\" [longitude]=\"lng\" [zoom]=\"15\">\r\n      <agm-marker [latitude]=\"lat\" [longitude]=\"lng\"></agm-marker>\r\n    </agm-map>\r\n  </div>\r\n</section>"

/***/ }),

/***/ "./src/app/support/phone/phone.component.scss":
/*!****************************************************!*\
  !*** ./src/app/support/phone/phone.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "section > h1 {\n  background: #bcc0c7;\n  color: #FFF;\n  font-size: 20px;\n  padding: 5px 15px;\n  line-height: 38px; }\n\nagm-map {\n  height: 300px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VwcG9ydC9waG9uZS9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXHN1cHBvcnRcXHBob25lXFxwaG9uZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVNLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFJckI7RUFDRSxhQUFhLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zdXBwb3J0L3Bob25lL3Bob25lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic2VjdGlvbiB7XHJcbiAgICA+aDEge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjYmNjMGM3O1xyXG4gICAgICBjb2xvcjogI0ZGRjtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgICAgbGluZS1oZWlnaHQ6IDM4cHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIGFnbS1tYXAge1xyXG4gICAgaGVpZ2h0OiAzMDBweDtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/support/phone/phone.component.ts":
/*!**************************************************!*\
  !*** ./src/app/support/phone/phone.component.ts ***!
  \**************************************************/
/*! exports provided: PhoneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneComponent", function() { return PhoneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_support_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/support.service */ "./src/app/services/support.service.ts");



var PhoneComponent = /** @class */ (function () {
    function PhoneComponent(supportService) {
        this.supportService = supportService;
        this.contactInfo = [];
    }
    PhoneComponent.prototype.ngOnInit = function () {
        this.getInfo();
    };
    PhoneComponent.prototype.getInfo = function () {
        var _this = this;
        this.loading = true;
        this.supportService.getContactInfo().subscribe(function (info) {
            _this.loading = false;
            _this.contactInfo = info.Data;
            _this.location = info.Data[3].Value.split(",");
            _this.lat = +_this.location[0];
            _this.lng = +_this.location[1];
        });
    };
    PhoneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-phone',
            template: __webpack_require__(/*! ./phone.component.html */ "./src/app/support/phone/phone.component.html"),
            styles: [__webpack_require__(/*! ./phone.component.scss */ "./src/app/support/phone/phone.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_support_service__WEBPACK_IMPORTED_MODULE_2__["SupportService"]])
    ], PhoneComponent);
    return PhoneComponent;
}());



/***/ }),

/***/ "./src/app/support/support-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/support/support-routing.module.ts ***!
  \***************************************************/
/*! exports provided: SupportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportRoutingModule", function() { return SupportRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _faqs_faqs_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./faqs/faqs.component */ "./src/app/support/faqs/faqs.component.ts");
/* harmony import */ var _support_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./support.component */ "./src/app/support/support.component.ts");
/* harmony import */ var _phone_phone_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./phone/phone.component */ "./src/app/support/phone/phone.component.ts");
/* harmony import */ var _email_email_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./email/email.component */ "./src/app/support/email/email.component.ts");
/* harmony import */ var _warrenty_warrenty_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./warrenty/warrenty.component */ "./src/app/support/warrenty/warrenty.component.ts");
/* harmony import */ var _downloads_downloads_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./downloads/downloads.component */ "./src/app/support/downloads/downloads.component.ts");
/* harmony import */ var _videos_videos_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./videos/videos.component */ "./src/app/support/videos/videos.component.ts");










var routes = [
    {
        path: '', component: _support_component__WEBPACK_IMPORTED_MODULE_4__["SupportComponent"],
        children: [
            { path: 'faqs', component: _faqs_faqs_component__WEBPACK_IMPORTED_MODULE_3__["FaqsComponent"] },
            { path: 'videos', component: _videos_videos_component__WEBPACK_IMPORTED_MODULE_9__["VideosComponent"] },
            { path: 'downloads', component: _downloads_downloads_component__WEBPACK_IMPORTED_MODULE_8__["DownloadsComponent"] },
            { path: 'warrenty', component: _warrenty_warrenty_component__WEBPACK_IMPORTED_MODULE_7__["WarrentyComponent"] },
            { path: 'email', component: _email_email_component__WEBPACK_IMPORTED_MODULE_6__["EmailComponent"] },
            { path: 'phone', component: _phone_phone_component__WEBPACK_IMPORTED_MODULE_5__["PhoneComponent"] },
            { path: '', redirectTo: 'faqs', pathMatch: 'full' },
        ]
    }
];
var SupportRoutingModule = /** @class */ (function () {
    function SupportRoutingModule() {
    }
    SupportRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], SupportRoutingModule);
    return SupportRoutingModule;
}());



/***/ }),

/***/ "./src/app/support/support.component.html":
/*!************************************************!*\
  !*** ./src/app/support/support.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"py-5\">\r\n  <div class=\"container\">\r\n    <h1>Need Support?</h1>\r\n    <h2>We are here to help</h2>\r\n    <!-- <div class=\"search-input\">\r\n      <i class=\"fa fa-search\"></i>\r\n      <input type=\"text\" class=\"form-control\" placeholder=\"Enter your model number or Keyword…\">\r\n    </div> -->\r\n  </div>\r\n</header>\r\n<nav class=\"container py-4\">\r\n  <h2 class=\"text-center\">What help do you need?</h2>\r\n  <ul class=\"py-3\">\r\n    <li><a routerLinkActive routerLinkActive=\"active-link\" #fags=\"routerLinkActive\" routerLink=\"/support/faqs\"><img\r\n          width=\"104\" height=\"104\"\r\n          [src]=\"fags.isActive ? 'assets/images/support/faq-active-icon.svg' : 'assets/images/support/faq-deactive-icon.svg'\">\r\n        <div>FAQ</div>\r\n      </a></li>\r\n    <li><a routerLinkActive routerLinkActive=\"active-link\" #videos=\"routerLinkActive\" routerLink=\"/support/videos\"><img\r\n          width=\"104\" height=\"104\"\r\n          [src]=\"videos.isActive ? 'assets/images/support/videos-active-icon.svg' : 'assets/images/support/videos-deactive-icon.svg'\">\r\n        <div>Videos</div>\r\n      </a></li>\r\n    <li><a routerLinkActive routerLinkActive=\"active-link\" #downloads=\"routerLinkActive\"\r\n        routerLink=\"/support/downloads\"><img width=\"104\" height=\"104\"\r\n          [src]=\"downloads.isActive ? 'assets/images/support/downloads-active-icon.svg' : 'assets/images/support/downloads-deactive-icon.svg'\">\r\n        <div>Downloads</div>\r\n      </a></li>\r\n    <!-- <li><a routerLinkActive routerLinkActive=\"active-link\" #warrenty=\"routerLinkActive\"\r\n        routerLink=\"/support/warrenty\"><img width=\"104\" height=\"104\"\r\n          [src]=\"warrenty.isActive ? 'assets/images/support/warrenty-active-icon.png':'assets/images/support/warrenty-deactive-icon.svg'\">\r\n        <div>Warrenty</div>\r\n      </a></li> -->\r\n    <li><a routerLinkActive routerLinkActive=\"active-link\" #email=\"routerLinkActive\" routerLink=\"/support/email\"><img\r\n          width=\"104\" height=\"104\"\r\n          [src]=\"email.isActive ? 'assets/images/support/email-active-icon.svg':'assets/images/support/email-deactive-icon.svg'\">\r\n        <div>Contact Us</div>\r\n      </a></li>\r\n    <li><a routerLinkActive routerLinkActive=\"active-link\" #phone=\"routerLinkActive\" routerLink=\"/support/phone\"><img\r\n          width=\"104\" height=\"104\"\r\n          [src]=\"phone.isActive ? 'assets/images/support/phone-active-icon.svg':'assets/images/support/phone-deactive-icon.svg'\">\r\n        <div>Phone</div>\r\n      </a></li>\r\n  </ul>\r\n</nav>\r\n<div class=\"container\">\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/support/support.component.scss":
/*!************************************************!*\
  !*** ./src/app/support/support.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background-image: url('support-header.jpg');\n  background-repeat: no-repeat;\n  background-size: cover; }\n  header h1 {\n    font-weight: bold;\n    font-size: 39px;\n    padding: 0;\n    margin: 0; }\n  header h2 {\n    font-family: sfpro-bold;\n    font-size: 53px;\n    letter-spacing: -3px; }\n  header .search-input {\n    display: flex;\n    width: 30%;\n    align-items: center;\n    background: #FFF;\n    border: 1px solid #cbd0d4;\n    padding: 0 10px; }\n  header .search-input input {\n      border: 0; }\n  header .search-input i {\n      color: #ed1c24; }\n  nav h2 {\n  letter-spacing: -3px;\n  font-family: sfpro-bold;\n  font-size: 45px;\n  color: #000000; }\n  nav ul {\n  display: flex;\n  justify-content: space-around;\n  flex-wrap: wrap;\n  text-align: center; }\n  nav ul li {\n    margin-bottom: 30px; }\n  nav ul li a.active-link div {\n      color: #ed1c24; }\n  nav ul li a div {\n      font-family: sfpro-bold;\n      font-size: 22px;\n      color: #000000; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VwcG9ydC9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXHN1cHBvcnRcXHN1cHBvcnQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3N1cHBvcnQvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXNzZXRzXFxzY3NzXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLDJDQUFxRTtFQUNyRSw0QkFBNEI7RUFDNUIsc0JBQXNCLEVBQUE7RUFIeEI7SUFLSSxpQkFBaUI7SUFDakIsZUFBZTtJQUNmLFVBQVU7SUFDVixTQUFTLEVBQUE7RUFSYjtJQVlJLHVCQUF1QjtJQUN2QixlQUFlO0lBQ2Ysb0JBQW9CLEVBQUE7RUFkeEI7SUFrQkksYUFBYTtJQUNiLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLHlCQUF5QjtJQUN6QixlQUFlLEVBQUE7RUF2Qm5CO01BMEJNLFNBQVMsRUFBQTtFQTFCZjtNQThCTSxjQy9CcUIsRUFBQTtFRG9DM0I7RUFFSSxvQkFBb0I7RUFDcEIsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZixjQUFjLEVBQUE7RUFMbEI7RUFTSSxhQUFhO0VBQ2IsNkJBQTZCO0VBQzdCLGVBQWU7RUFDZixrQkFBa0IsRUFBQTtFQVp0QjtJQWVNLG1CQUFtQixFQUFBO0VBZnpCO01BbUJVLGNDdkRpQixFQUFBO0VEb0MzQjtNQXlCVSx1QkFBdUI7TUFDdkIsZUFBZTtNQUNmLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3N1cHBvcnQvc3VwcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJy4uLy4uL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzJztcclxuXHJcbmhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvc3VwcG9ydC9zdXBwb3J0LWhlYWRlci5qcGcpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBoMSB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMzlweDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG5cclxuICBoMiB7XHJcbiAgICBmb250LWZhbWlseTogc2Zwcm8tYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogNTNweDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAtM3B4O1xyXG4gIH1cclxuXHJcbiAgLnNlYXJjaC1pbnB1dCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NiZDBkNDtcclxuICAgIHBhZGRpbmc6IDAgMTBweDtcclxuXHJcbiAgICBpbnB1dCB7XHJcbiAgICAgIGJvcmRlcjogMDtcclxuICAgIH1cclxuXHJcbiAgICBpIHtcclxuICAgICAgY29sb3I6ICRwcmltYXJ5LWJ0bi1jb2xvclxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxubmF2IHtcclxuICBoMiB7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogLTNweDtcclxuICAgIGZvbnQtZmFtaWx5OiBzZnByby1ib2xkO1xyXG4gICAgZm9udC1zaXplOiA0NXB4O1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgfVxyXG5cclxuICB1bCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgbGkge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG5cclxuICAgICAgYSB7XHJcbiAgICAgICAgJi5hY3RpdmUtbGluayBkaXYge1xyXG4gICAgICAgICAgY29sb3I6ICRwcmltYXJ5LWJ0bi1jb2xvclxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW1nIHt9XHJcblxyXG4gICAgICAgIGRpdiB7XHJcbiAgICAgICAgICBmb250LWZhbWlseTogc2Zwcm8tYm9sZDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCIkcHJpbWFyeS1iZy1jb2xvcjogIzQ0NDQ0NDtcclxuJHByaW1hcnktYnRuLWNvbG9yOiAjZWQxYzI0OyJdfQ== */"

/***/ }),

/***/ "./src/app/support/support.component.ts":
/*!**********************************************!*\
  !*** ./src/app/support/support.component.ts ***!
  \**********************************************/
/*! exports provided: SupportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportComponent", function() { return SupportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SupportComponent = /** @class */ (function () {
    function SupportComponent() {
    }
    SupportComponent.prototype.ngOnInit = function () {
    };
    SupportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-support',
            template: __webpack_require__(/*! ./support.component.html */ "./src/app/support/support.component.html"),
            styles: [__webpack_require__(/*! ./support.component.scss */ "./src/app/support/support.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SupportComponent);
    return SupportComponent;
}());



/***/ }),

/***/ "./src/app/support/support.module.ts":
/*!*******************************************!*\
  !*** ./src/app/support/support.module.ts ***!
  \*******************************************/
/*! exports provided: SupportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportModule", function() { return SupportModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _support_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./support-routing.module */ "./src/app/support/support-routing.module.ts");
/* harmony import */ var _faqs_faqs_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./faqs/faqs.component */ "./src/app/support/faqs/faqs.component.ts");
/* harmony import */ var _support_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./support.component */ "./src/app/support/support.component.ts");
/* harmony import */ var _videos_videos_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./videos/videos.component */ "./src/app/support/videos/videos.component.ts");
/* harmony import */ var _downloads_downloads_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./downloads/downloads.component */ "./src/app/support/downloads/downloads.component.ts");
/* harmony import */ var _warrenty_warrenty_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./warrenty/warrenty.component */ "./src/app/support/warrenty/warrenty.component.ts");
/* harmony import */ var _email_email_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./email/email.component */ "./src/app/support/email/email.component.ts");
/* harmony import */ var _phone_phone_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./phone/phone.component */ "./src/app/support/phone/phone.component.ts");
/* harmony import */ var _services_support_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/support.service */ "./src/app/services/support.service.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm5/agm-core.js");















var SupportModule = /** @class */ (function () {
    function SupportModule() {
    }
    SupportModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_faqs_faqs_component__WEBPACK_IMPORTED_MODULE_5__["FaqsComponent"], _support_component__WEBPACK_IMPORTED_MODULE_6__["SupportComponent"], _videos_videos_component__WEBPACK_IMPORTED_MODULE_7__["VideosComponent"], _downloads_downloads_component__WEBPACK_IMPORTED_MODULE_8__["DownloadsComponent"], _warrenty_warrenty_component__WEBPACK_IMPORTED_MODULE_9__["WarrentyComponent"], _email_email_component__WEBPACK_IMPORTED_MODULE_10__["EmailComponent"], _phone_phone_component__WEBPACK_IMPORTED_MODULE_11__["PhoneComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _support_routing_module__WEBPACK_IMPORTED_MODULE_4__["SupportRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_13__["SharedModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_14__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74',
                    language: 'en'
                })
            ],
            providers: [_services_support_service__WEBPACK_IMPORTED_MODULE_12__["SupportService"]]
        })
    ], SupportModule);
    return SupportModule;
}());

// AIzaSyB2tKpENK9xz4pEGYN4yGyeGS3NgAiCRIY
/// AIzaSyAi9hmqX3O_sn2lncv5LdqP1soXxJYyd74


/***/ }),

/***/ "./src/app/support/videos/videos.component.html":
/*!******************************************************!*\
  !*** ./src/app/support/videos/videos.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n  <h1>Most Popular Videos</h1>\r\n  <app-spinner *ngIf=\"loading1\"></app-spinner>\r\n\r\n  <ul>\r\n    <li class=\"d-flex mb-3\" *ngFor=\"let video of mostPopular\">\r\n      <div class=\"video\">\r\n        <iframe [src]=\"video.Link | safe:'resourceUrl'\" width=\"200\" height=\"100\" frameborder=\"0\" allow=\"fullscreen\"\r\n          allowfullscreen></iframe>\r\n      </div>\r\n      <div class=\"video-details mx-3 py-3\">\r\n        <h4>{{video.Title}}</h4>\r\n        <h5>{{video.SupportGroupName}}</h5>\r\n        <h6>{{video.ViewsCount}}</h6>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</section>\r\n<section>\r\n  <h1>Videos\r\n    <div class=\"float-right mx-3 search\">\r\n      <i class=\"fa fa-search\"></i>\r\n      <input type=\"text\" placeholder=\"Search in Videos …\" class=\"form-control\" [(ngModel)]=\"videoName\" name=\"videoName\"\r\n        (keyup)=\"debounceTime(getVideos, 800)\">\r\n    </div>\r\n    <!-- <div class=\"float-right\">\r\n      <select class=\"form-control\">\r\n        <option>Sort by</option>\r\n      </select>\r\n    </div> -->\r\n    <div class=\"clearfix\"></div>\r\n  </h1>\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n  <div class=\"alert alert-info shadow border\" *ngIf=\"noResult\">Sorry! we cannot find what you are looking for...</div>\r\n  <ul>\r\n    <li class=\"d-flex mb-3\" *ngFor=\"let video of videos\">\r\n      <div class=\"video\">\r\n        <iframe [src]=\"video.Link | safe:'resourceUrl'\" width=\"200\" height=\"100\" frameborder=\"0\" allow=\"fullscreen\"\r\n          allowfullscreen></iframe>\r\n      </div>\r\n      <div class=\"video-details mx-3 py-3\">\r\n        <h4>{{video.Title}}</h4>\r\n        <h5>{{video.SupportGroupName}}</h5>\r\n        <h6>{{video.ViewsCount}}</h6>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/support/videos/videos.component.scss":
/*!******************************************************!*\
  !*** ./src/app/support/videos/videos.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "section > h1 {\n  background: #bcc0c7;\n  color: #FFF;\n  font-size: 20px;\n  padding: 5px 15px;\n  line-height: 38px; }\n  section > h1 .search {\n    display: flex;\n    align-items: center;\n    background: #FFF;\n    color: #ccc;\n    padding: 0 10px;\n    border-radius: 3px; }\n  section > h1 .search input {\n      border: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VwcG9ydC92aWRlb3MvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxzdXBwb3J0XFx2aWRlb3NcXHZpZGVvcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVNLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTtFQU52QjtJQVNRLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysa0JBQWtCLEVBQUE7RUFkMUI7TUFpQlUsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc3VwcG9ydC92aWRlb3MvdmlkZW9zLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic2VjdGlvbiB7XHJcbiAgICA+aDEge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjYmNjMGM3O1xyXG4gICAgICBjb2xvcjogI0ZGRjtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgICAgbGluZS1oZWlnaHQ6IDM4cHg7XHJcbiAgXHJcbiAgICAgIC5zZWFyY2gge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRkZGO1xyXG4gICAgICAgIGNvbG9yOiAjY2NjO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgXHJcbiAgICAgICAgaW5wdXQge1xyXG4gICAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/support/videos/videos.component.ts":
/*!****************************************************!*\
  !*** ./src/app/support/videos/videos.component.ts ***!
  \****************************************************/
/*! exports provided: VideosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideosComponent", function() { return VideosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_support_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/support.service */ "./src/app/services/support.service.ts");



var VideosComponent = /** @class */ (function () {
    function VideosComponent(supportService) {
        var _this = this;
        this.supportService = supportService;
        this.videoName = '';
        this.mostPopular = [
            {
                title: 'Setting up your smart TV’s Internet connection.',
                subTitle: 'Network connectivity',
                views: '205k',
                url: 'https://player.vimeo.com/video/121653965'
            },
            {
                title: 'Setting up your smart TV’s wireless display connection.',
                subTitle: 'Operations',
                views: '229k',
                url: 'https://player.vimeo.com/video/2259254'
            },
            {
                title: 'Setting up and using Freeview Play catch-up services.',
                subTitle: 'Operations',
                views: '205k',
                url: 'https://player.vimeo.com/video/24905664'
            }
        ];
        this.videos = [
            {
                title: 'Setting up your smart TV’s Internet connection.',
                subTitle: 'Network connectivity',
                views: '205k',
                url: 'https://player.vimeo.com/video/121653965'
            },
            {
                title: 'Setting up your smart TV’s wireless display connection.',
                subTitle: 'Operations',
                views: '229k',
                url: 'https://player.vimeo.com/video/2259254'
            },
            {
                title: 'Setting up and using Freeview Play catch-up services.',
                subTitle: 'Operations',
                views: '205k',
                url: 'https://player.vimeo.com/video/24905664'
            },
            {
                title: 'Setting up your TV to enhance the sound performance.',
                subTitle: 'TV performance enhancement',
                views: '205k',
                url: 'https://player.vimeo.com/video/81097964'
            },
        ];
        this.getVideos = function () {
            _this.videos = [];
            _this.onErr = false;
            _this.loading = true;
            _this.noResult = false;
            _this.supportService.getVideos(_this.videoName).subscribe(function (videos) {
                _this.loading = false;
                _this.videos = videos.Data;
                if (videos.Data.length == 0)
                    _this.noResult = true;
            }, function (err) {
                _this.loading = false;
                _this.onErr = true;
            });
        };
    }
    VideosComponent.prototype.ngOnInit = function () {
        this.getMostPopularVideos();
        this.getVideos();
    };
    VideosComponent.prototype.getMostPopularVideos = function () {
        var _this = this;
        this.loading1 = true;
        this.supportService.getMostPopularVideos().subscribe(function (data) {
            _this.mostPopular = data.Data;
            _this.loading1 = false;
        });
    };
    VideosComponent.prototype.debounceTime = function (fn, time) {
        clearTimeout(this.debounceTimeOut);
        this.debounceTimeOut = setTimeout(fn, time);
    };
    VideosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-videos',
            template: __webpack_require__(/*! ./videos.component.html */ "./src/app/support/videos/videos.component.html"),
            styles: [__webpack_require__(/*! ./videos.component.scss */ "./src/app/support/videos/videos.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_support_service__WEBPACK_IMPORTED_MODULE_2__["SupportService"]])
    ], VideosComponent);
    return VideosComponent;
}());



/***/ }),

/***/ "./src/app/support/warrenty/warrenty.component.html":
/*!**********************************************************!*\
  !*** ./src/app/support/warrenty/warrenty.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n  <h1>Warrenty</h1>\r\n  <p class=\"text-center my-4\">It is empty here right now, please try again later!</p>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/support/warrenty/warrenty.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/support/warrenty/warrenty.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "section > h1 {\n  background: #bcc0c7;\n  color: #FFF;\n  font-size: 20px;\n  padding: 5px 15px;\n  line-height: 38px; }\n  section > h1 .search {\n    display: flex;\n    align-items: center;\n    background: #FFF;\n    color: #ccc;\n    padding: 0 10px;\n    border-radius: 3px; }\n  section > h1 .search input {\n      border: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VwcG9ydC93YXJyZW50eS9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXHN1cHBvcnRcXHdhcnJlbnR5XFx3YXJyZW50eS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTtFQU5yQjtJQVNNLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysa0JBQWtCLEVBQUE7RUFkeEI7TUFpQlEsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc3VwcG9ydC93YXJyZW50eS93YXJyZW50eS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNlY3Rpb24ge1xyXG4gID5oMSB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYmNjMGM3O1xyXG4gICAgY29sb3I6ICNGRkY7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzOHB4O1xyXG5cclxuICAgIC5zZWFyY2gge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjRkZGO1xyXG4gICAgICBjb2xvcjogI2NjYztcclxuICAgICAgcGFkZGluZzogMCAxMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcblxyXG4gICAgICBpbnB1dCB7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/support/warrenty/warrenty.component.ts":
/*!********************************************************!*\
  !*** ./src/app/support/warrenty/warrenty.component.ts ***!
  \********************************************************/
/*! exports provided: WarrentyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarrentyComponent", function() { return WarrentyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WarrentyComponent = /** @class */ (function () {
    function WarrentyComponent() {
    }
    WarrentyComponent.prototype.ngOnInit = function () {
    };
    WarrentyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-warrenty',
            template: __webpack_require__(/*! ./warrenty.component.html */ "./src/app/support/warrenty/warrenty.component.html"),
            styles: [__webpack_require__(/*! ./warrenty.component.scss */ "./src/app/support/warrenty/warrenty.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WarrentyComponent);
    return WarrentyComponent;
}());



/***/ })

}]);
//# sourceMappingURL=support-support-module.js.map
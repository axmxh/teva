(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["products-products-module"],{

/***/ "./node_modules/ngx-gallery/bundles/ngx-gallery.umd.js":
/*!*************************************************************!*\
  !*** ./node_modules/ngx-gallery/bundles/ngx-gallery.umd.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports, __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js"), __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js"), __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js")) :
	undefined;
}(this, (function (exports,core,common,platformBrowser) { 'use strict';

var NgxGalleryActionComponent = /** @class */ (function () {
    function NgxGalleryActionComponent() {
        this.disabled = false;
        this.titleText = '';
        this.onClick = new core.EventEmitter();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    NgxGalleryActionComponent.prototype.handleClick = function (event) {
        if (!this.disabled) {
            this.onClick.emit(event);
        }
        event.stopPropagation();
        event.preventDefault();
    };
    NgxGalleryActionComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'ngx-gallery-action',
                    template: "\n        <div class=\"ngx-gallery-icon\" [class.ngx-gallery-icon-disabled]=\"disabled\"\n            aria-hidden=\"true\"\n            title=\"{{ titleText }}\"\n            (click)=\"handleClick($event)\">\n                <i class=\"ngx-gallery-icon-content {{ icon }}\"></i>\n        </div>",
                    changeDetection: core.ChangeDetectionStrategy.OnPush
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryActionComponent.ctorParameters = function () { return []; };
    NgxGalleryActionComponent.propDecorators = {
        'icon': [{ type: core.Input },],
        'disabled': [{ type: core.Input },],
        'titleText': [{ type: core.Input },],
        'onClick': [{ type: core.Output },],
    };
    return NgxGalleryActionComponent;
}());

var NgxGalleryArrowsComponent = /** @class */ (function () {
    function NgxGalleryArrowsComponent() {
        this.onPrevClick = new core.EventEmitter();
        this.onNextClick = new core.EventEmitter();
    }
    /**
     * @return {?}
     */
    NgxGalleryArrowsComponent.prototype.handlePrevClick = function () {
        this.onPrevClick.emit();
    };
    /**
     * @return {?}
     */
    NgxGalleryArrowsComponent.prototype.handleNextClick = function () {
        this.onNextClick.emit();
    };
    NgxGalleryArrowsComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'ngx-gallery-arrows',
                    template: "\n        <div class=\"ngx-gallery-arrow-wrapper ngx-gallery-arrow-left\">\n            <div class=\"ngx-gallery-icon ngx-gallery-arrow\" aria-hidden=\"true\" (click)=\"handlePrevClick()\" [class.ngx-gallery-disabled]=\"prevDisabled\">\n                <i class=\"ngx-gallery-icon-content {{arrowPrevIcon}}\"></i>\n            </div>\n        </div>\n        <div class=\"ngx-gallery-arrow-wrapper ngx-gallery-arrow-right\">\n            <div class=\"ngx-gallery-icon ngx-gallery-arrow\" aria-hidden=\"true\" (click)=\"handleNextClick()\" [class.ngx-gallery-disabled]=\"nextDisabled\">\n                <i class=\"ngx-gallery-icon-content {{arrowNextIcon}}\"></i>\n            </div>\n        </div>\n    ",
                    styles: [".ngx-gallery-arrow-wrapper { position: absolute; height: 100%; width: 1px; display: table; z-index: 2000; table-layout: fixed; } .ngx-gallery-arrow-left { left: 0px; } .ngx-gallery-arrow-right { right: 0px; } .ngx-gallery-arrow { top: 50%; transform: translateY(-50%); cursor: pointer; } .ngx-gallery-arrow.ngx-gallery-disabled { opacity: 0.6; cursor: default; } .ngx-gallery-arrow-left .ngx-gallery-arrow { left: 10px; } .ngx-gallery-arrow-right .ngx-gallery-arrow { right: 10px; } "]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryArrowsComponent.ctorParameters = function () { return []; };
    NgxGalleryArrowsComponent.propDecorators = {
        'prevDisabled': [{ type: core.Input },],
        'nextDisabled': [{ type: core.Input },],
        'arrowPrevIcon': [{ type: core.Input },],
        'arrowNextIcon': [{ type: core.Input },],
        'onPrevClick': [{ type: core.Output },],
        'onNextClick': [{ type: core.Output },],
    };
    return NgxGalleryArrowsComponent;
}());

var NgxGalleryBulletsComponent = /** @class */ (function () {
    function NgxGalleryBulletsComponent() {
        this.active = 0;
        this.onChange = new core.EventEmitter();
    }
    /**
     * @return {?}
     */
    NgxGalleryBulletsComponent.prototype.getBullets = function () {
        return Array(this.count);
    };
    /**
     * @param {?} event
     * @param {?} index
     * @return {?}
     */
    NgxGalleryBulletsComponent.prototype.handleChange = function (event, index) {
        this.onChange.emit(index);
    };
    NgxGalleryBulletsComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'ngx-gallery-bullets',
                    template: "\n        <div class=\"ngx-gallery-bullet\" *ngFor=\"let bullet of getBullets(); let i = index;\" (click)=\"handleChange($event, i)\" [ngClass]=\"{ 'ngx-gallery-active': i == active }\"></div>\n    ",
                    styles: [":host { position: absolute; z-index: 2000; display: inline-flex; left: 50%; transform: translateX(-50%); bottom: 0px; padding: 10px; } .ngx-gallery-bullet { width: 10px; height: 10px; border-radius: 50%; cursor: pointer; background: white; } .ngx-gallery-bullet:not(:first-child) { margin-left: 5px; } .ngx-gallery-bullet:hover, .ngx-gallery-bullet.ngx-gallery-active { background: black; } "]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryBulletsComponent.ctorParameters = function () { return []; };
    NgxGalleryBulletsComponent.propDecorators = {
        'count': [{ type: core.Input },],
        'active': [{ type: core.Input },],
        'onChange': [{ type: core.Output },],
    };
    return NgxGalleryBulletsComponent;
}());

var NgxGalleryHelperService = /** @class */ (function () {
    /**
     * @param {?} renderer
     */
    function NgxGalleryHelperService(renderer) {
        this.renderer = renderer;
        this.swipeHandlers = new Map();
    }
    /**
     * @param {?} status
     * @param {?} element
     * @param {?} id
     * @param {?} nextHandler
     * @param {?} prevHandler
     * @return {?}
     */
    NgxGalleryHelperService.prototype.manageSwipe = function (status, element, id, nextHandler, prevHandler) {
        var /** @type {?} */ handlers = this.getSwipeHandlers(id);
        // swipeleft and swiperight are available only if hammerjs is included
        try {
            if (status && !handlers) {
                this.swipeHandlers.set(id, [
                    this.renderer.listen(element.nativeElement, 'swipeleft', function () { return nextHandler(); }),
                    this.renderer.listen(element.nativeElement, 'swiperight', function () { return prevHandler(); })
                ]);
            }
            else if (!status && handlers) {
                handlers.map(function (handler) { return handler(); });
                this.removeSwipeHandlers(id);
            }
        }
        catch (e) { }
    };
    /**
     * @param {?} url
     * @return {?}
     */
    NgxGalleryHelperService.prototype.validateUrl = function (url) {
        if (url.replace) {
            return url.replace(new RegExp(' ', 'g'), '%20')
                .replace(new RegExp('\'', 'g'), '%27');
        }
        else {
            return url;
        }
    };
    /**
     * @param {?} image
     * @return {?}
     */
    NgxGalleryHelperService.prototype.getBackgroundUrl = function (image) {
        return 'url(\'' + this.validateUrl(image) + '\')';
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NgxGalleryHelperService.prototype.getSwipeHandlers = function (id) {
        return this.swipeHandlers.get(id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NgxGalleryHelperService.prototype.removeSwipeHandlers = function (id) {
        this.swipeHandlers.delete(id);
    };
    NgxGalleryHelperService.decorators = [
        { type: core.Injectable },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryHelperService.ctorParameters = function () { return [
        { type: core.Renderer, },
    ]; };
    return NgxGalleryHelperService;
}());

var NgxGalleryAnimation = /** @class */ (function () {
    function NgxGalleryAnimation() {
    }
    NgxGalleryAnimation.Fade = 'fade';
    NgxGalleryAnimation.Slide = 'slide';
    NgxGalleryAnimation.Rotate = 'rotate';
    NgxGalleryAnimation.Zoom = 'zoom';
    return NgxGalleryAnimation;
}());

var NgxGalleryImageComponent = /** @class */ (function () {
    /**
     * @param {?} sanitization
     * @param {?} elementRef
     * @param {?} helperService
     */
    function NgxGalleryImageComponent(sanitization, elementRef, helperService) {
        this.sanitization = sanitization;
        this.elementRef = elementRef;
        this.helperService = helperService;
        this.onClick = new core.EventEmitter();
        this.onActiveChange = new core.EventEmitter();
        this.canChangeImage = true;
    }
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.ngOnInit = function () {
        if (this.arrows && this.arrowsAutoHide) {
            this.arrows = false;
        }
        if (this.autoPlay) {
            this.startAutoPlay();
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes['swipe']) {
            this.helperService.manageSwipe(this.swipe, this.elementRef, 'image', function () { return _this.showNext(); }, function () { return _this.showPrev(); });
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.onMouseEnter = function () {
        if (this.arrowsAutoHide && !this.arrows) {
            this.arrows = true;
        }
        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.stopAutoPlay();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.onMouseLeave = function () {
        if (this.arrowsAutoHide && this.arrows) {
            this.arrows = false;
        }
        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.startAutoPlay();
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.reset = function (index) {
        this.selectedIndex = index;
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.getImages = function () {
        if (!this.images) {
            return [];
        }
        if (this.lazyLoading) {
            var /** @type {?} */ indexes_1 = [this.selectedIndex];
            var /** @type {?} */ prevIndex = this.selectedIndex - 1;
            if (prevIndex === -1 && this.infinityMove) {
                indexes_1.push(this.images.length - 1);
            }
            else if (prevIndex >= 0) {
                indexes_1.push(prevIndex);
            }
            var /** @type {?} */ nextIndex = this.selectedIndex + 1;
            if (nextIndex == this.images.length && this.infinityMove) {
                indexes_1.push(0);
            }
            else if (nextIndex < this.images.length) {
                indexes_1.push(nextIndex);
            }
            return this.images.filter(function (img, i) { return indexes_1.indexOf(i) != -1; });
        }
        else {
            return this.images;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.startAutoPlay = function () {
        var _this = this;
        this.stopAutoPlay();
        this.timer = setInterval(function () {
            if (!_this.showNext()) {
                _this.selectedIndex = -1;
                _this.showNext();
            }
        }, this.autoPlayInterval);
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.stopAutoPlay = function () {
        if (this.timer) {
            clearInterval(this.timer);
        }
    };
    /**
     * @param {?} event
     * @param {?} index
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.handleClick = function (event, index) {
        if (this.clickable) {
            this.onClick.emit(index);
            event.stopPropagation();
            event.preventDefault();
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.show = function (index) {
        this.selectedIndex = index;
        this.onActiveChange.emit(this.selectedIndex);
        this.setChangeTimeout();
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.showNext = function () {
        if (this.canShowNext() && this.canChangeImage) {
            this.selectedIndex++;
            if (this.selectedIndex === this.images.length) {
                this.selectedIndex = 0;
            }
            this.onActiveChange.emit(this.selectedIndex);
            this.setChangeTimeout();
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.showPrev = function () {
        if (this.canShowPrev() && this.canChangeImage) {
            this.selectedIndex--;
            if (this.selectedIndex < 0) {
                this.selectedIndex = this.images.length - 1;
            }
            this.onActiveChange.emit(this.selectedIndex);
            this.setChangeTimeout();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.setChangeTimeout = function () {
        var _this = this;
        this.canChangeImage = false;
        var /** @type {?} */ timeout = 1000;
        if (this.animation === NgxGalleryAnimation.Slide
            || this.animation === NgxGalleryAnimation.Fade) {
            timeout = 500;
        }
        setTimeout(function () {
            _this.canChangeImage = true;
        }, timeout);
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.canShowNext = function () {
        if (this.images) {
            return this.infinityMove || this.selectedIndex < this.images.length - 1
                ? true : false;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.canShowPrev = function () {
        if (this.images) {
            return this.infinityMove || this.selectedIndex > 0 ? true : false;
        }
        else {
            return false;
        }
    };
    /**
     * @param {?} image
     * @return {?}
     */
    NgxGalleryImageComponent.prototype.getSafeUrl = function (image) {
        return this.sanitization.bypassSecurityTrustStyle(this.helperService.getBackgroundUrl(image));
    };
    NgxGalleryImageComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'ngx-gallery-image',
                    template: "\n        <div class=\"ngx-gallery-image-wrapper ngx-gallery-animation-{{animation}} ngx-gallery-image-size-{{size}}\">\n            <div class=\"ngx-gallery-image\" *ngFor=\"let image of getImages(); let i = index;\" [ngClass]=\"{ 'ngx-gallery-active': selectedIndex == image.index, 'ngx-gallery-inactive-left': selectedIndex > image.index, 'ngx-gallery-inactive-right': selectedIndex < image.index, 'ngx-gallery-clickable': clickable }\" [style.background-image]=\"getSafeUrl(image.src)\" (click)=\"handleClick($event, image.index)\">\n                <div class=\"ngx-gallery-icons-wrapper\">\n                    <ngx-gallery-action *ngFor=\"let action of actions\" [icon]=\"action.icon\" [disabled]=\"action.disabled\" [titleText]=\"action.titleText\" (onClick)=\"action.onClick($event, image.index)\"></ngx-gallery-action>\n                </div>\n                <div class=\"ngx-gallery-image-text\" *ngIf=\"showDescription && descriptions[image.index]\" [innerHTML]=\"descriptions[image.index]\" (click)=\"$event.stopPropagation()\"></div>\n            </div>\n        </div>\n        <ngx-gallery-bullets *ngIf=\"bullets\" [count]=\"images.length\" [active]=\"selectedIndex\" (onChange)=\"show($event)\"></ngx-gallery-bullets>\n        <ngx-gallery-arrows class=\"ngx-gallery-image-size-{{size}}\" *ngIf=\"arrows\" (onPrevClick)=\"showPrev()\" (onNextClick)=\"showNext()\" [prevDisabled]=\"!canShowPrev()\" [nextDisabled]=\"!canShowNext()\" [arrowPrevIcon]=\"arrowPrevIcon\" [arrowNextIcon]=\"arrowNextIcon\"></ngx-gallery-arrows>\n    ",
                    styles: [":host { width: 100%; display: inline-block; position: relative; } .ngx-gallery-image-wrapper { width: 100%; height: 100%; position: absolute; left: 0px; top: 0px; overflow: hidden; } .ngx-gallery-image { background-position: center; background-repeat: no-repeat; height: 100%; width: 100%; position: absolute; top: 0px; } .ngx-gallery-image.ngx-gallery-active { z-index: 1000; } .ngx-gallery-image-size-cover .ngx-gallery-image { background-size: cover; } .ngx-gallery-image-size-contain .ngx-gallery-image { background-size: contain; } .ngx-gallery-animation-fade .ngx-gallery-image { left: 0px; opacity: 0; transition: 0.5s ease-in-out; } .ngx-gallery-animation-fade .ngx-gallery-image.ngx-gallery-active { opacity: 1; } .ngx-gallery-animation-slide .ngx-gallery-image { transition: 0.5s ease-in-out; } .ngx-gallery-animation-slide .ngx-gallery-image.ngx-gallery-active { left: 0px; } .ngx-gallery-animation-slide .ngx-gallery-image.ngx-gallery-inactive-left { left: -100%; } .ngx-gallery-animation-slide .ngx-gallery-image.ngx-gallery-inactive-right { left: 100%; } .ngx-gallery-animation-rotate .ngx-gallery-image { transition: 1s ease; transform: scale(3.5, 3.5) rotate(90deg); left: 0px; opacity: 0; } .ngx-gallery-animation-rotate .ngx-gallery-image.ngx-gallery-active { transform: scale(1, 1) rotate(0deg); opacity: 1; } .ngx-gallery-animation-zoom .ngx-gallery-image { transition: 1s ease; transform: scale(2.5, 2.5); left: 0px; opacity: 0; } .ngx-gallery-animation-zoom .ngx-gallery-image.ngx-gallery-active { transform: scale(1, 1); opacity: 1; } .ngx-gallery-image-text { width: 100%; background: rgba(0, 0, 0, 0.7); padding: 10px; text-align: center; color: white; font-size: 16px; position: absolute; bottom: 0px; z-index: 10; } "]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryImageComponent.ctorParameters = function () { return [
        { type: platformBrowser.DomSanitizer, },
        { type: core.ElementRef, },
        { type: NgxGalleryHelperService, },
    ]; };
    NgxGalleryImageComponent.propDecorators = {
        'images': [{ type: core.Input },],
        'clickable': [{ type: core.Input },],
        'selectedIndex': [{ type: core.Input },],
        'arrows': [{ type: core.Input },],
        'arrowsAutoHide': [{ type: core.Input },],
        'swipe': [{ type: core.Input },],
        'animation': [{ type: core.Input },],
        'size': [{ type: core.Input },],
        'arrowPrevIcon': [{ type: core.Input },],
        'arrowNextIcon': [{ type: core.Input },],
        'autoPlay': [{ type: core.Input },],
        'autoPlayInterval': [{ type: core.Input },],
        'autoPlayPauseOnHover': [{ type: core.Input },],
        'infinityMove': [{ type: core.Input },],
        'lazyLoading': [{ type: core.Input },],
        'actions': [{ type: core.Input },],
        'descriptions': [{ type: core.Input },],
        'showDescription': [{ type: core.Input },],
        'bullets': [{ type: core.Input },],
        'onClick': [{ type: core.Output },],
        'onActiveChange': [{ type: core.Output },],
        'onMouseEnter': [{ type: core.HostListener, args: ['mouseenter',] },],
        'onMouseLeave': [{ type: core.HostListener, args: ['mouseleave',] },],
    };
    return NgxGalleryImageComponent;
}());

var NgxGalleryOrder = /** @class */ (function () {
    function NgxGalleryOrder() {
    }
    NgxGalleryOrder.Column = 1;
    NgxGalleryOrder.Row = 2;
    NgxGalleryOrder.Page = 3;
    return NgxGalleryOrder;
}());

var NgxGalleryThumbnailsComponent = /** @class */ (function () {
    /**
     * @param {?} sanitization
     * @param {?} elementRef
     * @param {?} helperService
     */
    function NgxGalleryThumbnailsComponent(sanitization, elementRef, helperService) {
        this.sanitization = sanitization;
        this.elementRef = elementRef;
        this.helperService = helperService;
        this.minStopIndex = 0;
        this.onActiveChange = new core.EventEmitter();
        this.index = 0;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes['selectedIndex']) {
            this.validateIndex();
        }
        if (changes['swipe']) {
            this.helperService.manageSwipe(this.swipe, this.elementRef, 'thumbnails', function () { return _this.moveRight(); }, function () { return _this.moveLeft(); });
        }
        if (this.images) {
            this.remainingCountValue = this.images.length - (this.rows * this.columns);
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.onMouseEnter = function () {
        this.mouseenter = true;
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.onMouseLeave = function () {
        this.mouseenter = false;
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.reset = function (index) {
        this.selectedIndex = index;
        this.setDefaultPosition();
        this.index = 0;
        this.validateIndex();
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getImages = function () {
        if (!this.images) {
            return [];
        }
        if (this.remainingCount) {
            return this.images.slice(0, this.rows * this.columns);
        }
        else if (this.lazyLoading && this.order != NgxGalleryOrder.Row) {
            var /** @type {?} */ stopIndex = 0;
            if (this.order === NgxGalleryOrder.Column) {
                stopIndex = (this.index + this.columns + this.moveSize) * this.rows;
            }
            else if (this.order === NgxGalleryOrder.Page) {
                stopIndex = this.index + ((this.columns * this.rows) * 2);
            }
            if (stopIndex <= this.minStopIndex) {
                stopIndex = this.minStopIndex;
            }
            else {
                this.minStopIndex = stopIndex;
            }
            return this.images.slice(0, stopIndex);
        }
        else {
            return this.images;
        }
    };
    /**
     * @param {?} event
     * @param {?} index
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.handleClick = function (event, index) {
        if (!this.hasLink(index)) {
            this.selectedIndex = index;
            this.onActiveChange.emit(index);
            event.stopPropagation();
            event.preventDefault();
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.hasLink = function (index) {
        if (this.links && this.links.length && this.links[index])
            return true;
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.moveRight = function () {
        if (this.canMoveRight()) {
            this.index += this.moveSize;
            var /** @type {?} */ maxIndex = this.getMaxIndex() - this.columns;
            if (this.index > maxIndex) {
                this.index = maxIndex;
            }
            this.setThumbnailsPosition();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.moveLeft = function () {
        if (this.canMoveLeft()) {
            this.index -= this.moveSize;
            if (this.index < 0) {
                this.index = 0;
            }
            this.setThumbnailsPosition();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.canMoveRight = function () {
        return this.index + this.columns < this.getMaxIndex() ? true : false;
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.canMoveLeft = function () {
        return this.index !== 0 ? true : false;
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getThumbnailLeft = function (index) {
        var /** @type {?} */ calculatedIndex;
        if (this.order === NgxGalleryOrder.Column) {
            calculatedIndex = Math.floor(index / this.rows);
        }
        else if (this.order === NgxGalleryOrder.Page) {
            calculatedIndex = (index % this.columns) + (Math.floor(index / (this.rows * this.columns)) * this.columns);
        }
        else if (this.order == NgxGalleryOrder.Row && this.remainingCount) {
            calculatedIndex = index % this.columns;
        }
        else {
            calculatedIndex = index % Math.ceil(this.images.length / this.rows);
        }
        return this.getThumbnailPosition(calculatedIndex, this.columns);
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getThumbnailTop = function (index) {
        var /** @type {?} */ calculatedIndex;
        if (this.order === NgxGalleryOrder.Column) {
            calculatedIndex = index % this.rows;
        }
        else if (this.order === NgxGalleryOrder.Page) {
            calculatedIndex = Math.floor(index / this.columns) - (Math.floor(index / (this.rows * this.columns)) * this.rows);
        }
        else if (this.order == NgxGalleryOrder.Row && this.remainingCount) {
            calculatedIndex = Math.floor(index / this.columns);
        }
        else {
            calculatedIndex = Math.floor(index / Math.ceil(this.images.length / this.rows));
        }
        return this.getThumbnailPosition(calculatedIndex, this.rows);
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getThumbnailWidth = function () {
        return this.getThumbnailDimension(this.columns);
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getThumbnailHeight = function () {
        return this.getThumbnailDimension(this.rows);
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.setThumbnailsPosition = function () {
        this.thumbnailsLeft = -((100 / this.columns) * this.index) + '%';
        this.thumbnailsMarginLeft = -((this.margin - (((this.columns - 1)
            * this.margin) / this.columns)) * this.index) + 'px';
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.setDefaultPosition = function () {
        this.thumbnailsLeft = '0px';
        this.thumbnailsMarginLeft = '0px';
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.canShowArrows = function () {
        if (this.remainingCount) {
            return false;
        }
        else if (this.arrows && this.images && this.images.length > this.getVisibleCount()
            && (!this.arrowsAutoHide || this.mouseenter)) {
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.validateIndex = function () {
        if (this.images) {
            var /** @type {?} */ newIndex = void 0;
            if (this.order === NgxGalleryOrder.Column) {
                newIndex = Math.floor(this.selectedIndex / this.rows);
            }
            else {
                newIndex = this.selectedIndex % Math.ceil(this.images.length / this.rows);
            }
            if (this.remainingCount) {
                newIndex = 0;
            }
            if (newIndex < this.index || newIndex >= this.index + this.columns) {
                var /** @type {?} */ maxIndex = this.getMaxIndex() - this.columns;
                this.index = newIndex > maxIndex ? maxIndex : newIndex;
                this.setThumbnailsPosition();
            }
        }
    };
    /**
     * @param {?} image
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getSafeUrl = function (image) {
        return this.sanitization.bypassSecurityTrustStyle(this.helperService.getBackgroundUrl(image));
    };
    /**
     * @param {?} index
     * @param {?} count
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getThumbnailPosition = function (index, count) {
        return this.getSafeStyle('calc(' + ((100 / count) * index) + '% + '
            + ((this.margin - (((count - 1) * this.margin) / count)) * index) + 'px)');
    };
    /**
     * @param {?} count
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getThumbnailDimension = function (count) {
        if (this.margin !== 0) {
            return this.getSafeStyle('calc(' + (100 / count) + '% - '
                + (((count - 1) * this.margin) / count) + 'px)');
        }
        else {
            return this.getSafeStyle('calc(' + (100 / count) + '% + 1px)');
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getMaxIndex = function () {
        if (this.order == NgxGalleryOrder.Page) {
            var /** @type {?} */ maxIndex = (Math.floor(this.images.length / this.getVisibleCount()) * this.columns);
            if (this.images.length % this.getVisibleCount() > this.columns) {
                maxIndex += this.columns;
            }
            else {
                maxIndex += this.images.length % this.getVisibleCount();
            }
            return maxIndex;
        }
        else {
            return Math.ceil(this.images.length / this.rows);
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getVisibleCount = function () {
        return this.columns * this.rows;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    NgxGalleryThumbnailsComponent.prototype.getSafeStyle = function (value) {
        return this.sanitization.bypassSecurityTrustStyle(value);
    };
    NgxGalleryThumbnailsComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'ngx-gallery-thumbnails',
                    template: "\n    <div class=\"ngx-gallery-thumbnails-wrapper ngx-gallery-thumbnail-size-{{size}}\">\n        <div class=\"ngx-gallery-thumbnails\" [style.transform]=\"'translateX(' + thumbnailsLeft + ')'\" [style.marginLeft]=\"thumbnailsMarginLeft\">\n            <a [href]=\"hasLink(i) ? links[i] : '#'\" [target]=\"linkTarget\" class=\"ngx-gallery-thumbnail\" *ngFor=\"let image of getImages(); let i = index;\" [style.background-image]=\"getSafeUrl(image)\" (click)=\"handleClick($event, i)\" [style.width]=\"getThumbnailWidth()\" [style.height]=\"getThumbnailHeight()\" [style.left]=\"getThumbnailLeft(i)\" [style.top]=\"getThumbnailTop(i)\" [ngClass]=\"{ 'ngx-gallery-active': i == selectedIndex, 'ngx-gallery-clickable': clickable }\" [attr.aria-label]=\"labels[i]\">\n                <div class=\"ngx-gallery-icons-wrapper\">\n                    <ngx-gallery-action *ngFor=\"let action of actions\" [icon]=\"action.icon\" [disabled]=\"action.disabled\" [titleText]=\"action.titleText\" (onClick)=\"action.onClick($event, i)\"></ngx-gallery-action>\n                </div>\n                <div class=\"ngx-gallery-remaining-count-overlay\" *ngIf=\"remainingCount && remainingCountValue && (i == (rows * columns) - 1)\">\n                    <span class=\"ngx-gallery-remaining-count\">+{{remainingCountValue}}</span>\n                </div>\n            </a>\n        </div>\n    </div>\n    <ngx-gallery-arrows *ngIf=\"canShowArrows()\" (onPrevClick)=\"moveLeft()\" (onNextClick)=\"moveRight()\" [prevDisabled]=\"!canMoveLeft()\" [nextDisabled]=\"!canMoveRight()\" [arrowPrevIcon]=\"arrowPrevIcon\" [arrowNextIcon]=\"arrowNextIcon\"></ngx-gallery-arrows>\n    ",
                    styles: [":host { width: 100%; display: inline-block; position: relative; } .ngx-gallery-thumbnails-wrapper { width: 100%; height: 100%; position: absolute; overflow: hidden; } .ngx-gallery-thumbnails { height: 100%; width: 100%; position: absolute; left: 0px; transform: translateX(0); transition: transform 0.5s ease-in-out; will-change: transform; } .ngx-gallery-thumbnails .ngx-gallery-thumbnail { position: absolute; height: 100%; background-position: center; background-repeat: no-repeat; text-decoration: none; } .ngx-gallery-thumbnail-size-cover .ngx-gallery-thumbnails .ngx-gallery-thumbnail { background-size: cover; } .ngx-gallery-thumbnail-size-contain .ngx-gallery-thumbnails .ngx-gallery-thumbnail { background-size: contain; } .ngx-gallery-remaining-count-overlay { width: 100%; height: 100%; position: absolute; left: 0px; top: 0px; background-color: rgba(0, 0, 0, 0.4); } .ngx-gallery-remaining-count { position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 30px; } "]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryThumbnailsComponent.ctorParameters = function () { return [
        { type: platformBrowser.DomSanitizer, },
        { type: core.ElementRef, },
        { type: NgxGalleryHelperService, },
    ]; };
    NgxGalleryThumbnailsComponent.propDecorators = {
        'images': [{ type: core.Input },],
        'links': [{ type: core.Input },],
        'labels': [{ type: core.Input },],
        'linkTarget': [{ type: core.Input },],
        'columns': [{ type: core.Input },],
        'rows': [{ type: core.Input },],
        'arrows': [{ type: core.Input },],
        'arrowsAutoHide': [{ type: core.Input },],
        'margin': [{ type: core.Input },],
        'selectedIndex': [{ type: core.Input },],
        'clickable': [{ type: core.Input },],
        'swipe': [{ type: core.Input },],
        'size': [{ type: core.Input },],
        'arrowPrevIcon': [{ type: core.Input },],
        'arrowNextIcon': [{ type: core.Input },],
        'moveSize': [{ type: core.Input },],
        'order': [{ type: core.Input },],
        'remainingCount': [{ type: core.Input },],
        'lazyLoading': [{ type: core.Input },],
        'actions': [{ type: core.Input },],
        'onActiveChange': [{ type: core.Output },],
        'onMouseEnter': [{ type: core.HostListener, args: ['mouseenter',] },],
        'onMouseLeave': [{ type: core.HostListener, args: ['mouseleave',] },],
    };
    return NgxGalleryThumbnailsComponent;
}());

var NgxGalleryPreviewComponent = /** @class */ (function () {
    /**
     * @param {?} sanitization
     * @param {?} elementRef
     * @param {?} helperService
     * @param {?} renderer
     * @param {?} changeDetectorRef
     */
    function NgxGalleryPreviewComponent(sanitization, elementRef, helperService, renderer, changeDetectorRef) {
        this.sanitization = sanitization;
        this.elementRef = elementRef;
        this.helperService = helperService;
        this.renderer = renderer;
        this.changeDetectorRef = changeDetectorRef;
        this.showSpinner = false;
        this.positionLeft = 0;
        this.positionTop = 0;
        this.zoomValue = 1;
        this.loading = false;
        this.rotateValue = 0;
        this.index = 0;
        this.onOpen = new core.EventEmitter();
        this.onClose = new core.EventEmitter();
        this.onActiveChange = new core.EventEmitter();
        this.isOpen = false;
        this.initialX = 0;
        this.initialY = 0;
        this.initialLeft = 0;
        this.initialTop = 0;
        this.isMove = false;
    }
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.ngOnInit = function () {
        if (this.arrows && this.arrowsAutoHide) {
            this.arrows = false;
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes['swipe']) {
            this.helperService.manageSwipe(this.swipe, this.elementRef, 'preview', function () { return _this.showNext(); }, function () { return _this.showPrev(); });
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.ngOnDestroy = function () {
        if (this.keyDownListener) {
            this.keyDownListener();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.onMouseEnter = function () {
        if (this.arrowsAutoHide && !this.arrows) {
            this.arrows = true;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.onMouseLeave = function () {
        if (this.arrowsAutoHide && this.arrows) {
            this.arrows = false;
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.onKeyDown = function (e) {
        if (this.isOpen) {
            if (this.keyboardNavigation) {
                if (this.isKeyboardPrev(e)) {
                    this.showPrev();
                }
                else if (this.isKeyboardNext(e)) {
                    this.showNext();
                }
            }
            if (this.closeOnEsc && this.isKeyboardEsc(e)) {
                this.close();
            }
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.open = function (index) {
        var _this = this;
        this.onOpen.emit();
        this.index = index;
        this.isOpen = true;
        this.show(true);
        if (this.forceFullscreen) {
            this.manageFullscreen();
        }
        this.keyDownListener = this.renderer.listenGlobal("window", "keydown", function (e) { return _this.onKeyDown(e); });
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.close = function () {
        this.isOpen = false;
        this.closeFullscreen();
        this.onClose.emit();
        this.stopAutoPlay();
        if (this.keyDownListener) {
            this.keyDownListener();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.imageMouseEnter = function () {
        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.stopAutoPlay();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.imageMouseLeave = function () {
        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.startAutoPlay();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.startAutoPlay = function () {
        var _this = this;
        if (this.autoPlay) {
            this.stopAutoPlay();
            this.timer = setTimeout(function () {
                if (!_this.showNext()) {
                    _this.index = -1;
                    _this.showNext();
                }
            }, this.autoPlayInterval);
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.stopAutoPlay = function () {
        if (this.timer) {
            clearTimeout(this.timer);
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.showAtIndex = function (index) {
        this.index = index;
        this.show();
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.showNext = function () {
        if (this.canShowNext()) {
            this.index++;
            if (this.index === this.images.length) {
                this.index = 0;
            }
            this.show();
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.showPrev = function () {
        if (this.canShowPrev()) {
            this.index--;
            if (this.index < 0) {
                this.index = this.images.length - 1;
            }
            this.show();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.canShowNext = function () {
        if (this.loading) {
            return false;
        }
        else if (this.images) {
            return this.infinityMove || this.index < this.images.length - 1 ? true : false;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.canShowPrev = function () {
        if (this.loading) {
            return false;
        }
        else if (this.images) {
            return this.infinityMove || this.index > 0 ? true : false;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.manageFullscreen = function () {
        if (this.fullscreen || this.forceFullscreen) {
            var /** @type {?} */ doc = (document);
            if (!doc.fullscreenElement && !doc.mozFullScreenElement
                && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
                this.openFullscreen();
            }
            else {
                this.closeFullscreen();
            }
        }
    };
    /**
     * @param {?} image
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.getSafeUrl = function (image) {
        return image.substr(0, 10) === 'data:image' ?
            image : this.sanitization.bypassSecurityTrustUrl(image);
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.zoomIn = function () {
        if (this.canZoomIn()) {
            this.zoomValue += this.zoomStep;
            if (this.zoomValue > this.zoomMax) {
                this.zoomValue = this.zoomMax;
            }
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.zoomOut = function () {
        if (this.canZoomOut()) {
            this.zoomValue -= this.zoomStep;
            if (this.zoomValue < this.zoomMin) {
                this.zoomValue = this.zoomMin;
            }
            if (this.zoomValue <= 1) {
                this.resetPosition();
            }
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.rotateLeft = function () {
        this.rotateValue -= 90;
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.rotateRight = function () {
        this.rotateValue += 90;
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.getTransform = function () {
        return this.sanitization.bypassSecurityTrustStyle('scale(' + this.zoomValue + ') rotate(' + this.rotateValue + 'deg)');
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.canZoomIn = function () {
        return this.zoomValue < this.zoomMax ? true : false;
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.canZoomOut = function () {
        return this.zoomValue > this.zoomMin ? true : false;
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.canDragOnZoom = function () {
        return this.zoom && this.zoomValue > 1;
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.mouseDownHandler = function (e) {
        if (this.canDragOnZoom()) {
            this.initialX = this.getClientX(e);
            this.initialY = this.getClientY(e);
            this.initialLeft = this.positionLeft;
            this.initialTop = this.positionTop;
            this.isMove = true;
            e.preventDefault();
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.mouseUpHandler = function (e) {
        this.isMove = false;
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.mouseMoveHandler = function (e) {
        if (this.isMove) {
            this.positionLeft = this.initialLeft + (this.getClientX(e) - this.initialX);
            this.positionTop = this.initialTop + (this.getClientY(e) - this.initialY);
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.getClientX = function (e) {
        return e.touches && e.touches.length ? e.touches[0].clientX : e.clientX;
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.getClientY = function (e) {
        return e.touches && e.touches.length ? e.touches[0].clientY : e.clientY;
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.resetPosition = function () {
        if (this.zoom) {
            this.positionLeft = 0;
            this.positionTop = 0;
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.isKeyboardNext = function (e) {
        return e.keyCode === 39 ? true : false;
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.isKeyboardPrev = function (e) {
        return e.keyCode === 37 ? true : false;
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.isKeyboardEsc = function (e) {
        return e.keyCode === 27 ? true : false;
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.openFullscreen = function () {
        var /** @type {?} */ element = (document.documentElement);
        if (element.requestFullscreen) {
            element.requestFullscreen();
        }
        else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
        else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        }
        else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.closeFullscreen = function () {
        if (this.isFullscreen()) {
            var /** @type {?} */ doc = (document);
            if (doc.exitFullscreen) {
                doc.exitFullscreen();
            }
            else if (doc.msExitFullscreen) {
                doc.msExitFullscreen();
            }
            else if (doc.mozCancelFullScreen) {
                doc.mozCancelFullScreen();
            }
            else if (doc.webkitExitFullscreen) {
                doc.webkitExitFullscreen();
            }
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.isFullscreen = function () {
        var /** @type {?} */ doc = (document);
        return doc.fullscreenElement || doc.webkitFullscreenElement
            || doc.mozFullScreenElement || doc.msFullscreenElement;
    };
    /**
     * @param {?=} first
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.show = function (first) {
        var _this = this;
        if (first === void 0) { first = false; }
        this.loading = true;
        this.stopAutoPlay();
        this.onActiveChange.emit(this.index);
        if (first || !this.animation) {
            this._show();
        }
        else {
            setTimeout(function () { return _this._show(); }, 600);
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype._show = function () {
        var _this = this;
        this.zoomValue = 1;
        this.rotateValue = 0;
        this.resetPosition();
        this.src = this.getSafeUrl(/** @type {?} */ (this.images[this.index]));
        this.srcIndex = this.index;
        this.description = this.descriptions[this.index];
        this.changeDetectorRef.markForCheck();
        setTimeout(function () {
            if (_this.isLoaded(_this.previewImage.nativeElement)) {
                _this.loading = false;
                _this.startAutoPlay();
                _this.changeDetectorRef.markForCheck();
            }
            else {
                setTimeout(function () {
                    if (_this.loading) {
                        _this.showSpinner = true;
                        _this.changeDetectorRef.markForCheck();
                    }
                });
                _this.previewImage.nativeElement.onload = function () {
                    _this.loading = false;
                    _this.showSpinner = false;
                    _this.previewImage.nativeElement.onload = null;
                    _this.startAutoPlay();
                    _this.changeDetectorRef.markForCheck();
                };
            }
        });
    };
    /**
     * @param {?} img
     * @return {?}
     */
    NgxGalleryPreviewComponent.prototype.isLoaded = function (img) {
        if (!img.complete) {
            return false;
        }
        if (typeof img.naturalWidth !== 'undefined' && img.naturalWidth === 0) {
            return false;
        }
        return true;
    };
    NgxGalleryPreviewComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'ngx-gallery-preview',
                    template: "\n        <ngx-gallery-arrows *ngIf=\"arrows\" (onPrevClick)=\"showPrev()\" (onNextClick)=\"showNext()\" [prevDisabled]=\"!canShowPrev()\" [nextDisabled]=\"!canShowNext()\" [arrowPrevIcon]=\"arrowPrevIcon\" [arrowNextIcon]=\"arrowNextIcon\"></ngx-gallery-arrows>\n        <div class=\"ngx-gallery-preview-top\">\n            <div class=\"ngx-gallery-preview-icons\">\n                <ngx-gallery-action *ngFor=\"let action of actions\" [icon]=\"action.icon\" [disabled]=\"action.disabled\" [titleText]=\"action.titleText\" (onClick)=\"action.onClick($event, index)\"></ngx-gallery-action>\n                <a *ngIf=\"download && src\" [href]=\"src\" class=\"ngx-gallery-icon\" aria-hidden=\"true\" download>\n                    <i class=\"ngx-gallery-icon-content {{ downloadIcon }}\"></i>\n                </a>\n                <ngx-gallery-action *ngIf=\"zoom\" [icon]=\"zoomOutIcon\" [disabled]=\"!canZoomOut()\" (onClick)=\"zoomOut()\"></ngx-gallery-action>\n                <ngx-gallery-action *ngIf=\"zoom\" [icon]=\"zoomInIcon\" [disabled]=\"!canZoomIn()\" (onClick)=\"zoomIn()\"></ngx-gallery-action>\n                <ngx-gallery-action *ngIf=\"rotate\" [icon]=\"rotateLeftIcon\" (onClick)=\"rotateLeft()\"></ngx-gallery-action>\n                <ngx-gallery-action *ngIf=\"rotate\" [icon]=\"rotateRightIcon\" (onClick)=\"rotateRight()\"></ngx-gallery-action>\n                <ngx-gallery-action *ngIf=\"fullscreen\" [icon]=\"'ngx-gallery-fullscreen ' + fullscreenIcon\" (onClick)=\"manageFullscreen()\"></ngx-gallery-action>\n                <ngx-gallery-action [icon]=\"'ngx-gallery-close ' + closeIcon\" (onClick)=\"close()\"></ngx-gallery-action>\n            </div>\n        </div>\n        <div class=\"ngx-spinner-wrapper ngx-gallery-center\" [class.ngx-gallery-active]=\"showSpinner\">\n            <i class=\"ngx-gallery-icon ngx-gallery-spinner {{spinnerIcon}}\" aria-hidden=\"true\"></i>\n        </div>\n        <div class=\"ngx-gallery-preview-wrapper\" (click)=\"closeOnClick && close()\" (mouseup)=\"mouseUpHandler($event)\" (mousemove)=\"mouseMoveHandler($event)\" (touchend)=\"mouseUpHandler($event)\" (touchmove)=\"mouseMoveHandler($event)\">\n            <div class=\"ngx-gallery-preview-img-wrapper\">\n                <img *ngIf=\"src\" #previewImage class=\"ngx-gallery-preview-img ngx-gallery-center\" [src]=\"src\" (click)=\"$event.stopPropagation()\" (mouseenter)=\"imageMouseEnter()\" (mouseleave)=\"imageMouseLeave()\" (mousedown)=\"mouseDownHandler($event)\" (touchstart)=\"mouseDownHandler($event)\" [class.ngx-gallery-active]=\"!loading\" [class.animation]=\"animation\" [class.ngx-gallery-grab]=\"canDragOnZoom()\" [style.transform]=\"getTransform()\" [style.left]=\"positionLeft + 'px'\" [style.top]=\"positionTop + 'px'\"/>\n                <ngx-gallery-bullets *ngIf=\"bullets\" [count]=\"images.length\" [active]=\"index\" (onChange)=\"showAtIndex($event)\"></ngx-gallery-bullets>\n            </div>\n            <div class=\"ngx-gallery-preview-text\" *ngIf=\"showDescription && description\" [innerHTML]=\"description\" (click)=\"$event.stopPropagation()\"></div>\n        </div>\n    ",
                    styles: [":host(.ngx-gallery-active) { width: 100%; height: 100%; position: fixed; left: 0; top: 0; background: rgba(0, 0, 0, 0.7); z-index: 10000; display: inline-block; } :host { display: none; } :host /deep/ .ngx-gallery-arrow { font-size: 50px; } :host /deep/ ngx-gallery-bullets { height: 5%; align-items: center; padding: 0px; } .ngx-gallery-preview-img { opacity: 0; max-width: 90%; max-height: 90%; user-select: none; transition: transform .5s; } .ngx-gallery-preview-img.animation { transition: opacity 0.5s linear, transform .5s; } .ngx-gallery-preview-img.ngx-gallery-active { opacity: 1; } .ngx-gallery-preview-img.ngx-gallery-grab { cursor: grab; cursor: -webkit-grab; } .ngx-gallery-icon.ngx-gallery-spinner { font-size: 50px; left: 0; display: inline-block; } :host /deep/ .ngx-gallery-preview-top { position: absolute; width: 100%; user-select: none; } :host /deep/ .ngx-gallery-preview-icons { float: right; } :host /deep/ .ngx-gallery-preview-icons .ngx-gallery-icon { position: relative; margin-right: 10px; margin-top: 10px; font-size: 25px; cursor: pointer; text-decoration: none; } :host /deep/ .ngx-gallery-preview-icons .ngx-gallery-icon.ngx-gallery-icon-disabled { cursor: default; opacity: 0.4; } .ngx-spinner-wrapper { width: 50px; height: 50px; display: none; } .ngx-spinner-wrapper.ngx-gallery-active { display: inline-block; } .ngx-gallery-center { position: absolute; left: 0; right: 0; bottom: 0; margin: auto; top: 0; } .ngx-gallery-preview-text { width: 100%; background: rgba(0, 0, 0, 0.7); padding: 10px; text-align: center; color: white; font-size: 16px; flex: 0 1 auto; z-index: 10; } .ngx-gallery-preview-wrapper { width: 100%; height: 100%; display: flex; flex-flow: column; } .ngx-gallery-preview-img-wrapper { flex: 1 1 auto; position: relative; } "]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryPreviewComponent.ctorParameters = function () { return [
        { type: platformBrowser.DomSanitizer, },
        { type: core.ElementRef, },
        { type: NgxGalleryHelperService, },
        { type: core.Renderer, },
        { type: core.ChangeDetectorRef, },
    ]; };
    NgxGalleryPreviewComponent.propDecorators = {
        'images': [{ type: core.Input },],
        'descriptions': [{ type: core.Input },],
        'showDescription': [{ type: core.Input },],
        'arrows': [{ type: core.Input },],
        'arrowsAutoHide': [{ type: core.Input },],
        'swipe': [{ type: core.Input },],
        'fullscreen': [{ type: core.Input },],
        'forceFullscreen': [{ type: core.Input },],
        'closeOnClick': [{ type: core.Input },],
        'closeOnEsc': [{ type: core.Input },],
        'keyboardNavigation': [{ type: core.Input },],
        'arrowPrevIcon': [{ type: core.Input },],
        'arrowNextIcon': [{ type: core.Input },],
        'closeIcon': [{ type: core.Input },],
        'fullscreenIcon': [{ type: core.Input },],
        'spinnerIcon': [{ type: core.Input },],
        'autoPlay': [{ type: core.Input },],
        'autoPlayInterval': [{ type: core.Input },],
        'autoPlayPauseOnHover': [{ type: core.Input },],
        'infinityMove': [{ type: core.Input },],
        'zoom': [{ type: core.Input },],
        'zoomStep': [{ type: core.Input },],
        'zoomMax': [{ type: core.Input },],
        'zoomMin': [{ type: core.Input },],
        'zoomInIcon': [{ type: core.Input },],
        'zoomOutIcon': [{ type: core.Input },],
        'animation': [{ type: core.Input },],
        'actions': [{ type: core.Input },],
        'rotate': [{ type: core.Input },],
        'rotateLeftIcon': [{ type: core.Input },],
        'rotateRightIcon': [{ type: core.Input },],
        'download': [{ type: core.Input },],
        'downloadIcon': [{ type: core.Input },],
        'bullets': [{ type: core.Input },],
        'onOpen': [{ type: core.Output },],
        'onClose': [{ type: core.Output },],
        'onActiveChange': [{ type: core.Output },],
        'previewImage': [{ type: core.ViewChild, args: ['previewImage',] },],
        'onMouseEnter': [{ type: core.HostListener, args: ['mouseenter',] },],
        'onMouseLeave': [{ type: core.HostListener, args: ['mouseleave',] },],
    };
    return NgxGalleryPreviewComponent;
}());

var NgxGalleryImageSize = /** @class */ (function () {
    function NgxGalleryImageSize() {
    }
    NgxGalleryImageSize.Cover = 'cover';
    NgxGalleryImageSize.Contain = 'contain';
    return NgxGalleryImageSize;
}());

var NgxGalleryLayout = /** @class */ (function () {
    function NgxGalleryLayout() {
    }
    NgxGalleryLayout.ThumbnailsTop = 'thumbnails-top';
    NgxGalleryLayout.ThumbnailsBottom = 'thumbnails-bottom';
    return NgxGalleryLayout;
}());

var NgxGalleryAction = /** @class */ (function () {
    /**
     * @param {?} action
     */
    function NgxGalleryAction(action) {
        this.icon = action.icon;
        this.disabled = action.disabled ? action.disabled : false;
        this.titleText = action.titleText ? action.titleText : '';
        this.onClick = action.onClick;
    }
    return NgxGalleryAction;
}());

var NgxGalleryOptions = /** @class */ (function () {
    /**
     * @param {?} obj
     */
    function NgxGalleryOptions(obj) {
        var preventDefaults = obj.breakpoint === undefined ? false : true;
        function use(source, defaultValue) {
            return obj && (source !== undefined || preventDefaults) ? source : defaultValue;
        }
        this.breakpoint = use(obj.breakpoint, undefined);
        this.width = use(obj.width, '500px');
        this.height = use(obj.height, '400px');
        this.fullWidth = use(obj.fullWidth, false);
        this.layout = use(obj.layout, NgxGalleryLayout.ThumbnailsBottom);
        this.startIndex = use(obj.startIndex, 0);
        this.linkTarget = use(obj.linkTarget, '_blank');
        this.lazyLoading = use(obj.lazyLoading, true);
        this.image = use(obj.image, true);
        this.imagePercent = use(obj.imagePercent, 75);
        this.imageArrows = use(obj.imageArrows, true);
        this.imageArrowsAutoHide = use(obj.imageArrowsAutoHide, false);
        this.imageSwipe = use(obj.imageSwipe, false);
        this.imageAnimation = use(obj.imageAnimation, NgxGalleryAnimation.Fade);
        this.imageSize = use(obj.imageSize, NgxGalleryImageSize.Cover);
        this.imageAutoPlay = use(obj.imageAutoPlay, false);
        this.imageAutoPlayInterval = use(obj.imageAutoPlayInterval, 2000);
        this.imageAutoPlayPauseOnHover = use(obj.imageAutoPlayPauseOnHover, false);
        this.imageInfinityMove = use(obj.imageInfinityMove, false);
        if (obj && obj.imageActions && obj.imageActions.length) {
            obj.imageActions = obj.imageActions.map(function (action) { return new NgxGalleryAction(action); });
        }
        this.imageActions = use(obj.imageActions, []);
        this.imageDescription = use(obj.imageDescription, false);
        this.imageBullets = use(obj.imageBullets, false);
        this.thumbnails = use(obj.thumbnails, true);
        this.thumbnailsColumns = use(obj.thumbnailsColumns, 4);
        this.thumbnailsRows = use(obj.thumbnailsRows, 1);
        this.thumbnailsPercent = use(obj.thumbnailsPercent, 25);
        this.thumbnailsMargin = use(obj.thumbnailsMargin, 10);
        this.thumbnailsArrows = use(obj.thumbnailsArrows, true);
        this.thumbnailsArrowsAutoHide = use(obj.thumbnailsArrowsAutoHide, false);
        this.thumbnailsSwipe = use(obj.thumbnailsSwipe, false);
        this.thumbnailsMoveSize = use(obj.thumbnailsMoveSize, 1);
        this.thumbnailsOrder = use(obj.thumbnailsOrder, NgxGalleryOrder.Column);
        this.thumbnailsRemainingCount = use(obj.thumbnailsRemainingCount, false);
        this.thumbnailsAsLinks = use(obj.thumbnailsAsLinks, false);
        this.thumbnailsAutoHide = use(obj.thumbnailsAutoHide, false);
        this.thumbnailMargin = use(obj.thumbnailMargin, 10);
        this.thumbnailSize = use(obj.thumbnailSize, NgxGalleryImageSize.Cover);
        if (obj && obj.thumbnailActions && obj.thumbnailActions.length) {
            obj.thumbnailActions = obj.thumbnailActions.map(function (action) { return new NgxGalleryAction(action); });
        }
        this.thumbnailActions = use(obj.thumbnailActions, []);
        this.preview = use(obj.preview, true);
        this.previewDescription = use(obj.previewDescription, true);
        this.previewArrows = use(obj.previewArrows, true);
        this.previewArrowsAutoHide = use(obj.previewArrowsAutoHide, false);
        this.previewSwipe = use(obj.previewSwipe, false);
        this.previewFullscreen = use(obj.previewFullscreen, false);
        this.previewForceFullscreen = use(obj.previewForceFullscreen, false);
        this.previewCloseOnClick = use(obj.previewCloseOnClick, false);
        this.previewCloseOnEsc = use(obj.previewCloseOnEsc, false);
        this.previewKeyboardNavigation = use(obj.previewKeyboardNavigation, false);
        this.previewAnimation = use(obj.previewAnimation, true);
        this.previewAutoPlay = use(obj.previewAutoPlay, false);
        this.previewAutoPlayInterval = use(obj.previewAutoPlayInterval, 2000);
        this.previewAutoPlayPauseOnHover = use(obj.previewAutoPlayPauseOnHover, false);
        this.previewInfinityMove = use(obj.previewInfinityMove, false);
        this.previewZoom = use(obj.previewZoom, false);
        this.previewZoomStep = use(obj.previewZoomStep, 0.1);
        this.previewZoomMax = use(obj.previewZoomMax, 2);
        this.previewZoomMin = use(obj.previewZoomMin, 0.5);
        this.previewRotate = use(obj.previewRotate, false);
        this.previewDownload = use(obj.previewDownload, false);
        this.previewCustom = use(obj.previewCustom, undefined);
        this.previewBullets = use(obj.previewBullets, false);
        this.arrowPrevIcon = use(obj.arrowPrevIcon, 'fa fa-arrow-circle-left');
        this.arrowNextIcon = use(obj.arrowNextIcon, 'fa fa-arrow-circle-right');
        this.closeIcon = use(obj.closeIcon, 'fa fa-times-circle');
        this.fullscreenIcon = use(obj.fullscreenIcon, 'fa fa-arrows-alt');
        this.spinnerIcon = use(obj.spinnerIcon, 'fa fa-spinner fa-pulse fa-3x fa-fw');
        this.zoomInIcon = use(obj.zoomInIcon, 'fa fa-search-plus');
        this.zoomOutIcon = use(obj.zoomOutIcon, 'fa fa-search-minus');
        this.rotateLeftIcon = use(obj.rotateLeftIcon, 'fa fa-undo');
        this.rotateRightIcon = use(obj.rotateRightIcon, 'fa fa-repeat');
        this.downloadIcon = use(obj.downloadIcon, 'fa fa-arrow-circle-down');
        if (obj && obj.actions && obj.actions.length) {
            obj.actions = obj.actions.map(function (action) { return new NgxGalleryAction(action); });
        }
        this.actions = use(obj.actions, []);
    }
    return NgxGalleryOptions;
}());

var NgxGalleryOrderedImage = /** @class */ (function () {
    /**
     * @param {?} obj
     */
    function NgxGalleryOrderedImage(obj) {
        this.src = obj.src;
        this.index = obj.index;
    }
    return NgxGalleryOrderedImage;
}());

var NgxGalleryComponent = /** @class */ (function () {
    /**
     * @param {?} myElement
     */
    function NgxGalleryComponent(myElement) {
        this.myElement = myElement;
        this.imagesReady = new core.EventEmitter();
        this.change = new core.EventEmitter();
        this.previewOpen = new core.EventEmitter();
        this.previewClose = new core.EventEmitter();
        this.previewChange = new core.EventEmitter();
        this.oldImagesLength = 0;
        this.selectedIndex = 0;
        this.breakpoint = undefined;
        this.prevBreakpoint = undefined;
    }
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.ngOnInit = function () {
        this.options = this.options.map(function (opt) { return new NgxGalleryOptions(opt); });
        this.sortOptions();
        this.setBreakpoint();
        this.setOptions();
        this.checkFullWidth();
        if (this.currentOptions) {
            this.selectedIndex = /** @type {?} */ (this.currentOptions.startIndex);
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.ngDoCheck = function () {
        if (this.images !== undefined && (this.images.length !== this.oldImagesLength)
            || (this.images !== this.oldImages)) {
            this.oldImagesLength = this.images.length;
            this.oldImages = this.images;
            this.setOptions();
            this.setImages();
            if (this.images && this.images.length) {
                this.imagesReady.emit();
            }
            if (this.image) {
                this.image.reset(/** @type {?} */ (this.currentOptions.startIndex));
            }
            if (this.currentOptions.thumbnailsAutoHide && this.currentOptions.thumbnails
                && this.images.length <= 1) {
                this.currentOptions.thumbnails = false;
                this.currentOptions.imageArrows = false;
            }
            this.resetThumbnails();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.ngAfterViewInit = function () {
        this.checkFullWidth();
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.onResize = function () {
        var _this = this;
        this.setBreakpoint();
        if (this.prevBreakpoint !== this.breakpoint) {
            this.setOptions();
            this.resetThumbnails();
        }
        if (this.currentOptions && this.currentOptions.fullWidth) {
            if (this.fullWidthTimeout) {
                clearTimeout(this.fullWidthTimeout);
            }
            this.fullWidthTimeout = setTimeout(function () {
                _this.checkFullWidth();
            }, 200);
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.getImageHeight = function () {
        return (this.currentOptions && this.currentOptions.thumbnails) ?
            this.currentOptions.imagePercent + '%' : '100%';
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.getThumbnailsHeight = function () {
        if (this.currentOptions && this.currentOptions.image) {
            return 'calc(' + this.currentOptions.thumbnailsPercent + '% - '
                + this.currentOptions.thumbnailsMargin + 'px)';
        }
        else {
            return '100%';
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.getThumbnailsMarginTop = function () {
        if (this.currentOptions && this.currentOptions.layout === NgxGalleryLayout.ThumbnailsBottom) {
            return this.currentOptions.thumbnailsMargin + 'px';
        }
        else {
            return '0px';
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.getThumbnailsMarginBottom = function () {
        if (this.currentOptions && this.currentOptions.layout === NgxGalleryLayout.ThumbnailsTop) {
            return this.currentOptions.thumbnailsMargin + 'px';
        }
        else {
            return '0px';
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryComponent.prototype.openPreview = function (index) {
        if (this.currentOptions.previewCustom) {
            this.currentOptions.previewCustom(index);
        }
        else {
            this.previewEnabled = true;
            this.preview.open(index);
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.onPreviewOpen = function () {
        this.previewOpen.emit();
        if (this.image && this.image.autoPlay) {
            this.image.stopAutoPlay();
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.onPreviewClose = function () {
        this.previewEnabled = false;
        this.previewClose.emit();
        if (this.image && this.image.autoPlay) {
            this.image.startAutoPlay();
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryComponent.prototype.selectFromImage = function (index) {
        this.select(index);
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryComponent.prototype.selectFromThumbnails = function (index) {
        this.select(index);
        if (this.currentOptions && this.currentOptions.thumbnails && this.currentOptions.preview
            && (!this.currentOptions.image || this.currentOptions.thumbnailsRemainingCount)) {
            this.openPreview(this.selectedIndex);
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryComponent.prototype.show = function (index) {
        this.select(index);
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.showNext = function () {
        this.image.showNext();
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.showPrev = function () {
        this.image.showPrev();
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.canShowNext = function () {
        if (this.images && this.currentOptions) {
            return (this.currentOptions.imageInfinityMove || this.selectedIndex < this.images.length - 1)
                ? true : false;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.canShowPrev = function () {
        if (this.images && this.currentOptions) {
            return (this.currentOptions.imageInfinityMove || this.selectedIndex > 0) ? true : false;
        }
        else {
            return false;
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryComponent.prototype.previewSelect = function (index) {
        this.previewChange.emit({ index: index, image: this.images[index] });
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.moveThumbnailsRight = function () {
        this.thubmnails.moveRight();
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.moveThumbnailsLeft = function () {
        this.thubmnails.moveLeft();
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.canMoveThumbnailsRight = function () {
        return this.thubmnails.canMoveRight();
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.canMoveThumbnailsLeft = function () {
        return this.thubmnails.canMoveLeft();
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.resetThumbnails = function () {
        if (this.thubmnails) {
            this.thubmnails.reset(/** @type {?} */ (this.currentOptions.startIndex));
        }
    };
    /**
     * @param {?} index
     * @return {?}
     */
    NgxGalleryComponent.prototype.select = function (index) {
        this.selectedIndex = index;
        this.change.emit({
            index: index,
            image: this.images[index]
        });
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.checkFullWidth = function () {
        if (this.currentOptions && this.currentOptions.fullWidth) {
            this.width = document.body.clientWidth + 'px';
            this.left = (-(document.body.clientWidth -
                this.myElement.nativeElement.parentNode.innerWidth) / 2) + 'px';
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.setImages = function () {
        this.smallImages = this.images.map(function (img) { /** @type {?} */ return (img.small); });
        this.mediumImages = this.images.map(function (img, i) { return new NgxGalleryOrderedImage({
            src: img.medium,
            index: i
        }); });
        this.bigImages = this.images.map(function (img) { /** @type {?} */ return (img.big); });
        this.descriptions = this.images.map(function (img) { /** @type {?} */ return (img.description); });
        this.links = this.images.map(function (img) { /** @type {?} */ return (img.url); });
        this.labels = this.images.map(function (img) { /** @type {?} */ return (img.label); });
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.setBreakpoint = function () {
        this.prevBreakpoint = this.breakpoint;
        var /** @type {?} */ breakpoints;
        if (typeof window !== 'undefined') {
            breakpoints = this.options.filter(function (opt) { return opt.breakpoint >= window.innerWidth; })
                .map(function (opt) { return opt.breakpoint; });
        }
        if (breakpoints && breakpoints.length) {
            this.breakpoint = breakpoints.pop();
        }
        else {
            this.breakpoint = undefined;
        }
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.sortOptions = function () {
        this.options = this.options.filter(function (a) { return a.breakpoint === undefined; }).concat(this.options
            .filter(function (a) { return a.breakpoint !== undefined; })
            .sort(function (a, b) { return b.breakpoint - a.breakpoint; }));
    };
    /**
     * @return {?}
     */
    NgxGalleryComponent.prototype.setOptions = function () {
        var _this = this;
        this.currentOptions = new NgxGalleryOptions({});
        this.options
            .filter(function (opt) { return opt.breakpoint === undefined || opt.breakpoint >= _this.breakpoint; })
            .map(function (opt) { return _this.combineOptions(_this.currentOptions, opt); });
        this.width = /** @type {?} */ (this.currentOptions.width);
        this.height = /** @type {?} */ (this.currentOptions.height);
    };
    /**
     * @param {?} first
     * @param {?} second
     * @return {?}
     */
    NgxGalleryComponent.prototype.combineOptions = function (first, second) {
        Object.keys(second).map(function (val) { return first[val] = second[val] !== undefined ? second[val] : first[val]; });
    };
    NgxGalleryComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'ngx-gallery',
                    template: "\n    <div class=\"ngx-gallery-layout {{currentOptions?.layout}}\">\n        <ngx-gallery-image *ngIf=\"currentOptions?.image\" [style.height]=\"getImageHeight()\" [images]=\"mediumImages\" [clickable]=\"currentOptions?.preview\" [selectedIndex]=\"selectedIndex\" [arrows]=\"currentOptions?.imageArrows\" [arrowsAutoHide]=\"currentOptions?.imageArrowsAutoHide\" [arrowPrevIcon]=\"currentOptions?.arrowPrevIcon\" [arrowNextIcon]=\"currentOptions?.arrowNextIcon\" [swipe]=\"currentOptions?.imageSwipe\" [animation]=\"currentOptions?.imageAnimation\" [size]=\"currentOptions?.imageSize\" [autoPlay]=\"currentOptions?.imageAutoPlay\" [autoPlayInterval]=\"currentOptions?.imageAutoPlayInterval\" [autoPlayPauseOnHover]=\"currentOptions?.imageAutoPlayPauseOnHover\" [infinityMove]=\"currentOptions?.imageInfinityMove\"  [lazyLoading]=\"currentOptions?.lazyLoading\" [actions]=\"currentOptions?.imageActions\" [descriptions]=\"descriptions\" [showDescription]=\"currentOptions?.imageDescription\" [bullets]=\"currentOptions?.imageBullets\" (onClick)=\"openPreview($event)\" (onActiveChange)=\"selectFromImage($event)\"></ngx-gallery-image>\n\n        <ngx-gallery-thumbnails *ngIf=\"currentOptions?.thumbnails\" [style.marginTop]=\"getThumbnailsMarginTop()\" [style.marginBottom]=\"getThumbnailsMarginBottom()\" [style.height]=\"getThumbnailsHeight()\" [images]=\"smallImages\" [links]=\"currentOptions?.thumbnailsAsLinks ? links : []\" [labels]=\"labels\" [linkTarget]=\"currentOptions?.linkTarget\" [selectedIndex]=\"selectedIndex\" [columns]=\"currentOptions?.thumbnailsColumns\" [rows]=\"currentOptions?.thumbnailsRows\" [margin]=\"currentOptions?.thumbnailMargin\" [arrows]=\"currentOptions?.thumbnailsArrows\" [arrowsAutoHide]=\"currentOptions?.thumbnailsArrowsAutoHide\" [arrowPrevIcon]=\"currentOptions?.arrowPrevIcon\" [arrowNextIcon]=\"currentOptions?.arrowNextIcon\" [clickable]=\"currentOptions?.image || currentOptions?.preview\" [swipe]=\"currentOptions?.thumbnailsSwipe\" [size]=\"currentOptions?.thumbnailSize\" [moveSize]=\"currentOptions?.thumbnailsMoveSize\" [order]=\"currentOptions?.thumbnailsOrder\" [remainingCount]=\"currentOptions?.thumbnailsRemainingCount\" [lazyLoading]=\"currentOptions?.lazyLoading\" [actions]=\"currentOptions?.thumbnailActions\"  (onActiveChange)=\"selectFromThumbnails($event)\"></ngx-gallery-thumbnails>\n\n        <ngx-gallery-preview [images]=\"bigImages\" [descriptions]=\"descriptions\" [showDescription]=\"currentOptions?.previewDescription\" [arrowPrevIcon]=\"currentOptions?.arrowPrevIcon\" [arrowNextIcon]=\"currentOptions?.arrowNextIcon\" [closeIcon]=\"currentOptions?.closeIcon\" [fullscreenIcon]=\"currentOptions?.fullscreenIcon\" [spinnerIcon]=\"currentOptions?.spinnerIcon\" [arrows]=\"currentOptions?.previewArrows\" [arrowsAutoHide]=\"currentOptions?.previewArrowsAutoHide\" [swipe]=\"currentOptions?.previewSwipe\" [fullscreen]=\"currentOptions?.previewFullscreen\" [forceFullscreen]=\"currentOptions?.previewForceFullscreen\" [closeOnClick]=\"currentOptions?.previewCloseOnClick\" [closeOnEsc]=\"currentOptions?.previewCloseOnEsc\" [keyboardNavigation]=\"currentOptions?.previewKeyboardNavigation\" [animation]=\"currentOptions?.previewAnimation\" [autoPlay]=\"currentOptions?.previewAutoPlay\" [autoPlayInterval]=\"currentOptions?.previewAutoPlayInterval\" [autoPlayPauseOnHover]=\"currentOptions?.previewAutoPlayPauseOnHover\" [infinityMove]=\"currentOptions?.previewInfinityMove\" [zoom]=\"currentOptions?.previewZoom\" [zoomStep]=\"currentOptions?.previewZoomStep\" [zoomMax]=\"currentOptions?.previewZoomMax\" [zoomMin]=\"currentOptions?.previewZoomMin\" [zoomInIcon]=\"currentOptions?.zoomInIcon\" [zoomOutIcon]=\"currentOptions?.zoomOutIcon\" [actions]=\"currentOptions?.actions\" [rotate]=\"currentOptions?.previewRotate\" [rotateLeftIcon]=\"currentOptions?.rotateLeftIcon\" [rotateRightIcon]=\"currentOptions?.rotateRightIcon\" [download]=\"currentOptions?.previewDownload\" [downloadIcon]=\"currentOptions?.downloadIcon\" [bullets]=\"currentOptions?.previewBullets\" (onClose)=\"onPreviewClose()\" (onOpen)=\"onPreviewOpen()\" (onActiveChange)=\"previewSelect($event)\" [class.ngx-gallery-active]=\"previewEnabled\"></ngx-gallery-preview>\n    </div>\n    ",
                    styles: [":host { display: inline-block; } :host > * { float: left; } :host /deep/ * { box-sizing: border-box; } :host /deep/ .ngx-gallery-icon { color: white; font-size: 25px; position: absolute; z-index: 2000; display: inline-block; } :host /deep/ .ngx-gallery-icon .ngx-gallery-icon-content { display: block; } :host /deep/ .ngx-gallery-clickable { cursor: pointer; } :host /deep/ .ngx-gallery-icons-wrapper .ngx-gallery-icon { position: relative; margin-right: 5px; margin-top: 5px; font-size: 20px; cursor: pointer; } :host /deep/ .ngx-gallery-icons-wrapper { float: right; } :host .ngx-gallery-layout { width: 100%; height: 100%; display: flex; flex-direction: column; } :host .ngx-gallery-layout.thumbnails-top ngx-gallery-image { order: 2; } :host .ngx-gallery-layout.thumbnails-top ngx-gallery-thumbnails { order: 1; } :host .ngx-gallery-layout.thumbnails-bottom ngx-gallery-image { order: 1; } :host .ngx-gallery-layout.thumbnails-bottom ngx-gallery-thumbnails { order: 2; } "],
                    providers: [NgxGalleryHelperService]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryComponent.ctorParameters = function () { return [
        { type: core.ElementRef, },
    ]; };
    NgxGalleryComponent.propDecorators = {
        'options': [{ type: core.Input },],
        'images': [{ type: core.Input },],
        'imagesReady': [{ type: core.Output },],
        'change': [{ type: core.Output },],
        'previewOpen': [{ type: core.Output },],
        'previewClose': [{ type: core.Output },],
        'previewChange': [{ type: core.Output },],
        'preview': [{ type: core.ViewChild, args: [NgxGalleryPreviewComponent,] },],
        'image': [{ type: core.ViewChild, args: [NgxGalleryImageComponent,] },],
        'thubmnails': [{ type: core.ViewChild, args: [NgxGalleryThumbnailsComponent,] },],
        'width': [{ type: core.HostBinding, args: ['style.width',] },],
        'height': [{ type: core.HostBinding, args: ['style.height',] },],
        'left': [{ type: core.HostBinding, args: ['style.left',] },],
        'onResize': [{ type: core.HostListener, args: ['window:resize',] },],
    };
    return NgxGalleryComponent;
}());

var NgxGalleryImage = /** @class */ (function () {
    /**
     * @param {?} obj
     */
    function NgxGalleryImage(obj) {
        this.small = obj.small;
        this.medium = obj.medium;
        this.big = obj.big;
        this.description = obj.description;
        this.url = obj.url;
        this.label = obj.label;
    }
    return NgxGalleryImage;
}());

var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CustomHammerConfig = /** @class */ (function (_super) {
    __extends(CustomHammerConfig, _super);
    function CustomHammerConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.overrides = ({
            'pinch': { enable: false },
            'rotate': { enable: false }
        });
        return _this;
    }
    return CustomHammerConfig;
}(platformBrowser.HammerGestureConfig));
var NgxGalleryModule = /** @class */ (function () {
    function NgxGalleryModule() {
    }
    NgxGalleryModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [
                        common.CommonModule
                    ],
                    declarations: [
                        NgxGalleryActionComponent,
                        NgxGalleryArrowsComponent,
                        NgxGalleryBulletsComponent,
                        NgxGalleryImageComponent,
                        NgxGalleryThumbnailsComponent,
                        NgxGalleryPreviewComponent,
                        NgxGalleryComponent
                    ],
                    exports: [
                        NgxGalleryComponent
                    ],
                    providers: [
                        { provide: platformBrowser.HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig }
                    ]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxGalleryModule.ctorParameters = function () { return []; };
    return NgxGalleryModule;
}());

exports.CustomHammerConfig = CustomHammerConfig;
exports.NgxGalleryModule = NgxGalleryModule;
exports.NgxGalleryComponent = NgxGalleryComponent;
exports.NgxGalleryActionComponent = NgxGalleryActionComponent;
exports.NgxGalleryImageComponent = NgxGalleryImageComponent;
exports.NgxGalleryThumbnailsComponent = NgxGalleryThumbnailsComponent;
exports.NgxGalleryPreviewComponent = NgxGalleryPreviewComponent;
exports.NgxGalleryArrowsComponent = NgxGalleryArrowsComponent;
exports.NgxGalleryBulletsComponent = NgxGalleryBulletsComponent;
exports.NgxGalleryOptions = NgxGalleryOptions;
exports.NgxGalleryImage = NgxGalleryImage;
exports.NgxGalleryAnimation = NgxGalleryAnimation;
exports.NgxGalleryHelperService = NgxGalleryHelperService;
exports.NgxGalleryImageSize = NgxGalleryImageSize;
exports.NgxGalleryLayout = NgxGalleryLayout;
exports.NgxGalleryOrder = NgxGalleryOrder;
exports.NgxGalleryOrderedImage = NgxGalleryOrderedImage;
exports.NgxGalleryAction = NgxGalleryAction;

Object.defineProperty(exports, '__esModule', { value: true });

})));


/***/ }),

/***/ "./src/app/products/compare-page/compare-page.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/products/compare-page/compare-page.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <h1 class=\"my-3\">Compare</h1>\r\n  <div class=\"compare-cols\">\r\n    <!-- <div class=\"titles-col\">\r\n      <h3 class=\"mt-5\">Display</h3>\r\n      <ul>\r\n        <li>Screen Size</li>\r\n        <li>Resolution</li>\r\n        <li>Panel Type</li>\r\n        <li>Brightness</li>\r\n      </ul>\r\n      <h3 class=\"mt-5\">Main Features</h3>\r\n      <ul>\r\n        <li>Smart TV</li>\r\n        <li>Process Type</li>\r\n        <li>Total Picture Quality</li>\r\n      </ul>\r\n      <h3 class=\"mt-5\">Video</h3>\r\n      <ul>\r\n        <li>Wide Color Gamut</li>\r\n        <li>HDR</li>\r\n        <li>Dolby Vision HDR</li>\r\n        <li>MEMC</li>\r\n        <li>UHD Upscaler</li>\r\n        <li>Picture Modes</li>\r\n      </ul>\r\n    </div> -->\r\n    <div class=\"product-col\">\r\n      <div class=\"form-group w-75 mx-auto mb-3\">\r\n        <select class=\"form-control\" [(ngModel)]=\"selectedProduct\" name=\"selectedProduct\"\r\n          (change)=\"getProductDetails(selectedProduct , 'main')\">\r\n          <option *ngFor=\"let product of products\" [value]=\"product.ProductID\">{{product.ProductName}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"mb-5\">\r\n        <img class=\"mb-3\" height=\"200\"\r\n          [src]=\"mainProductData?.ProductImage ||'../../../assets/images/tv_placeholder.png'\">\r\n        <h2>{{mainProductData?.ProductName}}</h2>\r\n        <h3>{{mainProductData?.ProductCode}}</h3>\r\n        <br>\r\n        <app-spinner *ngIf=\"loadingMainProduct\"></app-spinner>\r\n      </div>\r\n      <div class=\"specs\" *ngFor=\"let spec of mainProductData?.Specs\">\r\n        <h3 class=\"mb-5 border py-3\">{{spec.GroupName}}</h3>\r\n        <ul>\r\n          <li class=\"mb-4\" *ngFor=\"let option of spec._lstAttributes\">\r\n            <h4 class=\"text-muted m-0\">{{option.AttributeName}}</h4>\r\n            <p>{{option.Option}}</p>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n    <div class=\"product-col\">\r\n      <div class=\"form-group w-75 mx-auto mb-3\">\r\n        <select class=\"form-control\" (change)=\"getProductDetails(selectedProductWith , 'secondary')\"\r\n          [(ngModel)]=\"selectedProductWith\" name=\"selectedProductWith\">\r\n          <option *ngFor=\"let product of products\" [value]=\"product.ProductID\">{{product.ProductName}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"mb-5\">\r\n        <img class=\"mb-3\" height=\"200\"\r\n          [src]=\"compareWithData?.ProductImage ||'../../../assets/images/tv_placeholder.png'\">\r\n        <h2>{{compareWithData?.ProductName}}</h2>\r\n        <h3>{{compareWithData?.ProductCode}}</h3>\r\n        <br>\r\n        <app-spinner *ngIf=\"loadingSecondaryProduct\"></app-spinner>\r\n\r\n      </div>\r\n      <div class=\"specs\" *ngFor=\"let spec of compareWithData?.Specs\">\r\n        <h3 class=\"mb-5 border py-3\">{{spec.GroupName}}</h3>\r\n        <ul>\r\n          <li class=\"mb-4\" *ngFor=\"let option of spec._lstAttributes\">\r\n            <h4 class=\"text-muted m-0\">{{option.AttributeName}}</h4>\r\n            <p>{{option.Option}}</p>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/products/compare-page/compare-page.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/products/compare-page/compare-page.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1 {\n  background: #bcc0c7;\n  color: #FFF;\n  font-size: 20px;\n  padding: 5px 15px;\n  line-height: 38px; }\n\n.compare-cols {\n  display: flex;\n  align-items: flex-start; }\n\n.compare-cols .titles-col {\n    width: 20%; }\n\n.compare-cols .product-col {\n    width: 50%;\n    text-align: center; }\n\n.compare-cols .hidden-vis {\n    opacity: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvY29tcGFyZS1wYWdlL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxccHJvZHVjdHNcXGNvbXBhcmUtcGFnZVxcY29tcGFyZS1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGFBQWE7RUFDYix1QkFBdUIsRUFBQTs7QUFGekI7SUFJTSxVQUNKLEVBQUE7O0FBTEY7SUFPTSxVQUFVO0lBQ1Ysa0JBQWtCLEVBQUE7O0FBUnhCO0lBV00sVUFBVSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdHMvY29tcGFyZS1wYWdlL2NvbXBhcmUtcGFnZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImgxIHtcclxuICBiYWNrZ3JvdW5kOiAjYmNjMGM3O1xyXG4gIGNvbG9yOiAjRkZGO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBwYWRkaW5nOiA1cHggMTVweDtcclxuICBsaW5lLWhlaWdodDogMzhweDtcclxufVxyXG5cclxuLmNvbXBhcmUtY29scyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAudGl0bGVzLWNvbCB7XHJcbiAgICAgIHdpZHRoOiAyMCVcclxuICB9XHJcbiAgLnByb2R1Y3QtY29se1xyXG4gICAgICB3aWR0aDogNTAlO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5oaWRkZW4tdmlze1xyXG4gICAgICBvcGFjaXR5OiAwO1xyXG4gIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/products/compare-page/compare-page.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/products/compare-page/compare-page.component.ts ***!
  \*****************************************************************/
/*! exports provided: ComparePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComparePageComponent", function() { return ComparePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");






var ComparePageComponent = /** @class */ (function () {
    function ComparePageComponent(catService, route, platformId) {
        this.catService = catService;
        this.route = route;
        this.platformId = platformId;
        this.products = [];
    }
    ComparePageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getProductsToCompare();
        this.route.queryParams.subscribe(function (params) {
            _this.getProductDetails(params.mainProduct, 'main');
            _this.getProductDetails(params.compareWith, 'secondary');
            _this.selectedProduct = params.mainProduct;
            _this.selectedProductWith = params.compareWith;
        });
    };
    ComparePageComponent.prototype.getProductsToCompare = function () {
        var _this = this;
        var params = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_5__["SearchModel"]();
        params.Keyword = '';
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            params.CountryID = localStorage.getItem('countryID');
        }
        this.catService.searchProducts(params).subscribe(function (results) {
            _this.products = results['Data'];
        });
    };
    ComparePageComponent.prototype.getProductDetails = function (productId, type) {
        var _this = this;
        if (type === 'main')
            this.loadingMainProduct = true;
        if (type === 'secondary')
            this.loadingSecondaryProduct = true;
        this.loading = true;
        var params = {
            'ProductID': productId
        };
        this.catService.getProductToCompare(params).subscribe(function (data) {
            _this.loading = false;
            _this.loadingMainProduct = false;
            _this.loadingSecondaryProduct = false;
            if (type === 'main')
                _this.mainProductData = data;
            if (type === 'secondary')
                _this.compareWithData = data;
        });
    };
    ;
    ComparePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-compare-page',
            template: __webpack_require__(/*! ./compare-page.component.html */ "./src/app/products/compare-page/compare-page.component.html"),
            styles: [__webpack_require__(/*! ./compare-page.component.scss */ "./src/app/products/compare-page/compare-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            Object])
    ], ComparePageComponent);
    return ComparePageComponent;
}());



/***/ }),

/***/ "./src/app/products/compare-panel/compare-panel.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/products/compare-panel/compare-panel.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"addToCompare\">\r\n  <div class=\"d-flex px-3\">\r\n    <img width=\"11\" src=\"assets/images/search-font-awesome.svg\">\r\n    <input type=\"text\" class=\"form-control border-0\" placeholder=\"Search for Product …\" [(ngModel)]=\"keyword\"\r\n      name=\"keyword\" (keyup)=\"debounceTime(getProductsToCompare, 800)\">\r\n  </div>\r\n  <ul class=\"compare-list\">\r\n    <li *ngIf=\"compareListSpinner\" class=\"text-center pt-5\">\r\n      <app-spinner [type]=\"'inline'\"></app-spinner>\r\n    </li>\r\n    <li *ngFor=\"let product of products\">\r\n      <a [routerLink]=\"['/products/compare']\"\r\n        [queryParams]=\"{mainProduct: mainProductID, compareWith:product.ProductID}\">\r\n        <img [src]=\"product.ProductImage || '../../../assets/images/tv_placeholder.png'\">\r\n        <span>{{product.ProductName}}</span>\r\n      </a>\r\n    </li>\r\n    <li class=\"text-center pt-3\" *ngIf=\"noResultsFound\">No result found</li>\r\n  </ul>\r\n</div>"

/***/ }),

/***/ "./src/app/products/compare-panel/compare-panel.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/products/compare-panel/compare-panel.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".addToCompare {\n  position: absolute;\n  background: #FFF;\n  box-shadow: 0 0 15px rgba(68, 68, 68, 0.3);\n  padding: 7px;\n  height: 265px;\n  overflow-y: scroll;\n  width: 93%;\n  z-index: 9999; }\n  .addToCompare .compare-list li {\n    margin-bottom: 14px; }\n  .addToCompare .compare-list li a {\n      display: flex;\n      flex-direction: row;\n      align-items: center;\n      cursor: pointer; }\n  .addToCompare .compare-list li a img {\n        width: 60px;\n        margin-right: 5px; }\n  .addToCompare .compare-list li a span {\n        color: #000;\n        font-size: 12px;\n        font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvY29tcGFyZS1wYW5lbC9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXHByb2R1Y3RzXFxjb21wYXJlLXBhbmVsXFxjb21wYXJlLXBhbmVsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQiwwQ0FBeUM7RUFDekMsWUFBWTtFQUNaLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGFBQWEsRUFBQTtFQVJqQjtJQVdRLG1CQUFtQixFQUFBO0VBWDNCO01BYVUsYUFBYTtNQUNiLG1CQUFtQjtNQUNuQixtQkFBbUI7TUFDbkIsZUFBZSxFQUFBO0VBaEJ6QjtRQWtCWSxXQUFXO1FBQ1gsaUJBQWlCLEVBQUE7RUFuQjdCO1FBdUJZLFdBQVc7UUFDWCxlQUFlO1FBQ2YsaUJBQWlCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0cy9jb21wYXJlLXBhbmVsL2NvbXBhcmUtcGFuZWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWRkVG9Db21wYXJlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQ6ICNGRkY7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMTVweCByZ2JhKDY4LCA2OCwgNjgsIC4zKTtcclxuICAgIHBhZGRpbmc6IDdweDtcclxuICAgIGhlaWdodDogMjY1cHg7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICB3aWR0aDogOTMlO1xyXG4gICAgei1pbmRleDogOTk5OTtcclxuICAgIC5jb21wYXJlLWxpc3Qge1xyXG4gICAgICBsaSB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTRweDtcclxuICAgICAgICBhIHtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgY29sb3I6ICMwMDA7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/products/compare-panel/compare-panel.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/products/compare-panel/compare-panel.component.ts ***!
  \*******************************************************************/
/*! exports provided: ComparePanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComparePanelComponent", function() { return ComparePanelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");





var ComparePanelComponent = /** @class */ (function () {
    function ComparePanelComponent(catService, platformId) {
        var _this = this;
        this.catService = catService;
        this.platformId = platformId;
        this.keyword = '';
        this.products = [];
        this.getProductsToCompare = function () {
            _this.compareListSpinner = true;
            var params = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_2__["SearchModel"]();
            _this.noResultsFound = false;
            params.Keyword = _this.keyword;
            if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_3__["isPlatformBrowser"])(_this.platformId)) {
                params.CountryID = localStorage.getItem('countryID');
            }
            _this.catService.searchProducts(params).subscribe(function (results) {
                _this.compareListSpinner = false;
                _this.products = results['Data'];
                if (results['Data'].length == 0) {
                    _this.noResultsFound = true;
                }
            });
        };
    }
    ComparePanelComponent.prototype.ngOnInit = function () {
        this.getProductsToCompare();
    };
    ComparePanelComponent.prototype.debounceTime = function (fn, time) {
        clearTimeout(this.debounceTimeOut);
        this.debounceTimeOut = setTimeout(fn, time);
    };
    ;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], ComparePanelComponent.prototype, "mainProductID", void 0);
    ComparePanelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-compare-panel',
            template: __webpack_require__(/*! ./compare-panel.component.html */ "./src/app/products/compare-panel/compare-panel.component.html"),
            styles: [__webpack_require__(/*! ./compare-panel.component.scss */ "./src/app/products/compare-panel/compare-panel.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_category_service__WEBPACK_IMPORTED_MODULE_4__["CategoryService"], Object])
    ], ComparePanelComponent);
    return ComparePanelComponent;
}());



/***/ }),

/***/ "./src/app/products/product-item/product-item.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-item/product-item.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"product-item p-3 mb-3\">\r\n  <div class=\"product-image text-center mb-3\">\r\n    <img class=\"w-100\" [src]=\"product.ProductImage\">\r\n  </div>\r\n  <h4>{{product.Name}}</h4>\r\n  <small>{{product.Code}}</small>\r\n  <p class=\"mb-3\">{{product.ShortDescription}}</p>\r\n\r\n  <ul class=\"d-flex justify-content-between\">\r\n    <li><a [routerLink]=\"['/products/product']\" [queryParams]=\"{ProductID:product.ProductID, catId: catId}\"\r\n        class=\"plain-btn text-danger\">See\r\n        Details <img width=\"5\" src=\"assets/images/arrow-right.svg\"></a></li>\r\n    <li><button (click)=\"onSelectCompare()\" class=\"plain-btn\"><img width=\"13\" src=\"assets/images/compare.svg\"> Add to\r\n        Compare</button>\r\n    </li>\r\n  </ul>\r\n  <app-compare-panel *ngIf=\"showCompare\" [mainProductID]=\"product.ProductID\"></app-compare-panel>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/products/product-item/product-item.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-item/product-item.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".product-item {\n  position: relative; }\n  .product-item p {\n    font-size: 12px;\n    font-weight: 300;\n    color: #838a93; }\n  .product-item small {\n    font-weight: bold; }\n  ul li a {\n  font-size: 13px;\n  color: #ed1c24;\n  font-weight: bold; }\n  ul li button {\n  font-size: 13px;\n  font-weight: bold;\n  color: #666666; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1pdGVtL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxccHJvZHVjdHNcXHByb2R1Y3QtaXRlbVxccHJvZHVjdC1pdGVtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCLEVBQUE7RUFEcEI7SUFJSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWMsRUFBQTtFQU5sQjtJQVVJLGlCQUFpQixFQUFBO0VBSXJCO0VBR00sZUFBZTtFQUNmLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTtFQUx2QjtFQVdNLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1pdGVtL3Byb2R1Y3QtaXRlbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9kdWN0LWl0ZW0ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgcCB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgY29sb3I6ICM4MzhhOTM7XHJcbiAgfVxyXG5cclxuICBzbWFsbCB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbn1cclxuXHJcbnVsIHtcclxuICBsaSB7XHJcbiAgICBhIHtcclxuICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICBjb2xvcjogI2VkMWMyNDtcclxuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgO1xyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgIGNvbG9yOiAjNjY2NjY2O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/products/product-item/product-item.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/products/product-item/product-item.component.ts ***!
  \*****************************************************************/
/*! exports provided: ProductItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductItemComponent", function() { return ProductItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ProductItemComponent = /** @class */ (function () {
    function ProductItemComponent(route) {
        this.route = route;
    }
    ProductItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.catId = params.id;
        });
    };
    ProductItemComponent.prototype.onSelectCompare = function () {
        this.showCompare = !this.showCompare;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProductItemComponent.prototype, "product", void 0);
    ProductItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-item',
            template: __webpack_require__(/*! ./product-item.component.html */ "./src/app/products/product-item/product-item.component.html"),
            styles: [__webpack_require__(/*! ./product-item.component.scss */ "./src/app/products/product-item/product-item.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], ProductItemComponent);
    return ProductItemComponent;
}());



/***/ }),

/***/ "./src/app/products/product-list/product-list.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-list/product-list.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-breadcrumbs [data]=\"breadcrumbsInfo\"></app-breadcrumbs>\r\n\r\n<div class=\"container\">\r\n  <ul class=\"products-list mt-4\">\r\n    <li *ngFor=\"let product of products\">\r\n      <app-product-item [product]=\"product\"></app-product-item>\r\n    </li>\r\n  </ul>\r\n\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n\r\n  <div *ngIf=\"noProducts\" class=\"alert alert-info text-center shadow border my-5\">Sorry! there is no available products\r\n    at the moment!</div>\r\n</div>\r\n\r\n<app-widget-article [style]=\"'styleHorizontal'\" [data]=\"support\"></app-widget-article>\r\n"

/***/ }),

/***/ "./src/app/products/product-list/product-list.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-list/product-list.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".products-list {\n  display: flex;\n  flex-wrap: wrap; }\n  .products-list li {\n    flex: 0 0 25%; }\n  @media (max-width: 500px) {\n    .products-list li {\n      flex: 0 0 100%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1saXN0L0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxccHJvZHVjdHNcXHByb2R1Y3QtbGlzdFxccHJvZHVjdC1saXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksYUFBYTtFQUNiLGVBQWUsRUFBQTtFQUhuQjtJQU1NLGFBQWEsRUFBQTtFQUdmO0lBVEo7TUFXUSxjQUFjLEVBQUEsRUFDZiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3QtbGlzdC9wcm9kdWN0LWxpc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJvZHVjdHMtbGlzdCB7XHJcblxyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICBcclxuICAgIGxpIHtcclxuICAgICAgZmxleDogMCAwIDI1JTtcclxuICAgIH1cclxuICBcclxuICAgIEBtZWRpYShtYXgtd2lkdGg6NTAwcHgpIHtcclxuICAgICAgbGkge1xyXG4gICAgICAgIGZsZXg6IDAgMCAxMDAlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgXHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/products/product-list/product-list.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/products/product-list/product-list.component.ts ***!
  \*****************************************************************/
/*! exports provided: ProductListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListComponent", function() { return ProductListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");





var ProductListComponent = /** @class */ (function () {
    function ProductListComponent(route, catService, platformId) {
        this.route = route;
        this.catService = catService;
        this.platformId = platformId;
        this.support = {
            title: "Support",
            description: "Access and download more product information and find quick and easy troubleshooting advice.",
            url: "/support",
            urlTxt: "Get Support",
            image: "assets/images/support-image.jpg"
        };
        this.breadcrumbsInfo = {
            'background': 'assets/images/4-k-smart-t-vs-header.jpg',
            'paths': [
                { 'name': 'Home', 'url': '/' },
                { 'name': '', 'url': '/products/product-list?id=', 'params': {} }
            ],
            'description': ''
        };
        this.products = [];
    }
    ProductListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.productId = params["id"];
            _this.getCatItems();
        });
    };
    ProductListComponent.prototype.getCatItems = function () {
        var _this = this;
        var localStore;
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_4__["isPlatformBrowser"])(this.platformId)) {
            localStore = localStorage.getItem('countryID');
        }
        this.loading = true;
        this.noProducts = false;
        var params = {
            CategoryID: this.productId,
            CountryID: localStore
        };
        this.catService.getCategoryItems(params).subscribe(function (items) {
            _this.loading = false;
            if (items.Data.length == 0)
                _this.noProducts = true;
            _this.products = items.Data;
            if (items.CategoryImage != null)
                _this.breadcrumbsInfo.background = items.CategoryImage;
            _this.breadcrumbsInfo.paths[1] = { 'name': items.CategoryName, 'url': '/products/product-list', params: { id: _this.productId } };
        });
    };
    ;
    ProductListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-list',
            template: __webpack_require__(/*! ./product-list.component.html */ "./src/app/products/product-list/product-list.component.html"),
            styles: [__webpack_require__(/*! ./product-list.component.scss */ "./src/app/products/product-list/product-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"], Object])
    ], ProductListComponent);
    return ProductListComponent;
}());



/***/ }),

/***/ "./src/app/products/product-page/product-page.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-page/product-page.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-breadcrumbs [data]=\"breadcrumbsInfo\"></app-breadcrumbs>\r\n<div class=\"container\">\r\n  <app-spinner *ngIf=\"loading\"></app-spinner>\r\n  <div class=\"row my-5\">\r\n    <!-- product gallery -->\r\n    <div class=\"col-md-6 col-12\">\r\n      <app-product-slider [galleryImages]=\"galleryImages\"></app-product-slider>\r\n    </div>\r\n\r\n    <!-- product description -->\r\n    <div class=\"col-md-6 col-12 product-desc\">\r\n      <div class=\"font-size-sm\">\r\n        <label class=\"text-muted\">Size Name: </label>\r\n        <strong> {{productDetails?.SizeName}}</strong>\r\n      </div>\r\n      <div class=\"btn-group-toggle\" data-toggle=\"buttons\">\r\n        <label *ngFor=\"let size of productDetails?.Sizes;let i =index\" class=\"btn btn-outline-secondary mb-2\"\r\n          [ngClass]=\"{'active': productDetails.ProductID == size.ProductID}\">\r\n          <input type=\"radio\" (focus)=\"onSelectSize(size.ProductID)\" name=\"options\" [id]=\"'option' + i\"\r\n            autocomplete=\"off\" checked> {{size.SizeName}}\r\n        </label>\r\n\r\n      </div>\r\n      <h1 class=\"mt-3\">{{productDetails?.ProductName}}</h1>\r\n      <h5>{{productDetails?.Code}}</h5>\r\n      <ul>\r\n        <li *ngFor=\"let item of productDescriptionList\">{{item}}</li>\r\n      </ul>\r\n      <button routerLink=\"/find-store\" class=\"primary-btn w-50 py-2\">Find a store</button>\r\n\r\n      <!-- Add to compare -->\r\n      <div class=\"add-to-compare-container\">\r\n        <div class=\"text-center w-50 mt-3\">\r\n          <button (click)=\"onSelectCompare()\" class=\"plain-btn font-size-sm\"><img width=\"22\"\r\n              src=\"assets/images/compare.svg\"> Add to\r\n            Compare</button>\r\n        </div>\r\n        <app-compare-panel *ngIf=\"showCompare\" [mainProductID]=\"productDetails.ProductID\"></app-compare-panel>\r\n      </div>\r\n      <div class=\"share-links w-50 mt-3\">\r\n        <label class=\"text-muted float-left font-size-sm\">Share this product</label>\r\n\r\n        <ul class=\"float-right\">\r\n          <li>\r\n            <fb-like url=\"https://www.toshiba-teva.com/products/product?ProductID={{productId}}&catId={{catID}}\">\r\n            </fb-like>\r\n            <!-- <a href=\"#\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a> -->\r\n          </li>\r\n          <!-- <li><a href=\"#\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>\r\n          <li><a href=\"#\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a></li>\r\n          <li><a href=\"#\"><i class=\"fa fa-pinterest-p\" aria-hidden=\"true\"></i></a></li> -->\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- tabs -->\r\n<app-product-tabs></app-product-tabs>\r\n"

/***/ }),

/***/ "./src/app/products/product-page/product-page.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-page/product-page.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".font-size-sm {\n  font-size: 15px; }\n\n.btn-group-toggle .btn-outline-secondary {\n  border: solid 1px #cbd0d4;\n  color: #000000;\n  font-weight: 600;\n  font-size: 15px;\n  margin: 0 3px;\n  text-shadow: none;\n  padding: 2px 10px;\n  border-radius: 2px;\n  cursor: pointer; }\n\n.btn-group-toggle .btn-outline-secondary:hover {\n    color: #FFF; }\n\n.btn-group-toggle .btn-outline-secondary:focus {\n    outline: none;\n    box-shadow: none; }\n\n.btn-group-toggle .btn-outline-secondary.active {\n  border: solid 1px #ff0006;\n  background: none; }\n\n.btn-group-toggle .btn-outline-secondary.active:hover {\n    color: #000; }\n\nh1 {\n  font-size: 39px;\n  font-weight: 600;\n  color: #000000; }\n\nh5 {\n  font-size: 18px;\n  font-weight: 500;\n  color: #000000; }\n\nli {\n  font-size: 14px;\n  font-weight: 300;\n  line-height: 1.5;\n  color: #000000; }\n\n.share-links li {\n  float: left;\n  margin: 0 5px; }\n\n.share-links li a {\n    color: #444;\n    font-size: 15px; }\n\n.add-to-compare-container {\n  position: relative; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1wYWdlL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxccHJvZHVjdHNcXHByb2R1Y3QtcGFnZVxccHJvZHVjdC1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZSxFQUFBOztBQUdqQjtFQUVJLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQVZuQjtJQWFNLFdBQ0YsRUFBQTs7QUFkSjtJQWlCTSxhQUFhO0lBQ2IsZ0JBQWdCLEVBQUE7O0FBbEJ0QjtFQXVCSSx5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7O0FBeEJwQjtJQTJCTSxXQUNGLEVBQUE7O0FBSUo7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixjQUFjLEVBQUE7O0FBR2hCO0VBRUksV0FBVztFQUNYLGFBQWEsRUFBQTs7QUFIakI7SUFNTSxXQUFXO0lBQ1gsZUFBZSxFQUFBOztBQUtyQjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1wYWdlL3Byb2R1Y3QtcGFnZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb250LXNpemUtc20ge1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuLmJ0bi1ncm91cC10b2dnbGUge1xyXG4gIC5idG4tb3V0bGluZS1zZWNvbmRhcnkge1xyXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2NiZDBkNDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIG1hcmdpbjogMCAzcHg7XHJcbiAgICB0ZXh0LXNoYWRvdzogbm9uZTtcclxuICAgIHBhZGRpbmc6IDJweCAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG5cclxuICAgICY6aG92ZXIge1xyXG4gICAgICBjb2xvcjogI0ZGRlxyXG4gICAgfVxyXG5cclxuICAgICY6Zm9jdXMge1xyXG4gICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmJ0bi1vdXRsaW5lLXNlY29uZGFyeS5hY3RpdmUge1xyXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2ZmMDAwNjtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcblxyXG4gICAgJjpob3ZlciB7XHJcbiAgICAgIGNvbG9yOiAjMDAwXHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5oMSB7XHJcbiAgZm9udC1zaXplOiAzOXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbn1cclxuXHJcbmg1IHtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG5cclxubGkge1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBmb250LXdlaWdodDogMzAwO1xyXG4gIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbn1cclxuXHJcbi5zaGFyZS1saW5rcyB7XHJcbiAgbGkge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBtYXJnaW46IDAgNXB4O1xyXG5cclxuICAgIGEge1xyXG4gICAgICBjb2xvcjogIzQ0NDtcclxuICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmFkZC10by1jb21wYXJlLWNvbnRhaW5lciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/products/product-page/product-page.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/products/product-page/product-page.component.ts ***!
  \*****************************************************************/
/*! exports provided: ProductPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageComponent", function() { return ProductPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ProductPageComponent = /** @class */ (function () {
    function ProductPageComponent(catService, route, router) {
        this.catService = catService;
        this.route = route;
        this.router = router;
        this.title = 'OLED TV 6';
        this.galleryImages = [];
        this.tabs = [];
        this.productDescriptionList = [];
        this.breadcrumbsInfo = {
            'background': 'assets/images/breadcrubs-bg.jpg',
            'paths': [
                { 'name': 'Home', 'url': '/' },
                { 'name': '', 'url': '/products/product-list', 'params': {} },
                { 'name': '', 'url': '/products/product-page', 'params': {} }
            ]
        };
    }
    ProductPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.productId = params.ProductID;
            _this.catID = params.catId;
        });
        if (!this.productId)
            this.router.navigate(['/page-not-found']);
        this.getProductDetails();
    };
    ProductPageComponent.prototype.getProductDetails = function () {
        var _this = this;
        this.loading = true;
        var params = {
            'ProductID': this.productId
        };
        this.galleryImages = [];
        this.catService.getProductDetails(params).subscribe(function (data) {
            _this.loading = false;
            _this.productDetails = data['Data'];
            _this.breadcrumbsInfo.paths[1] = { 'name': data['Data'].CategoryName, 'url': '/products/product-list', 'params': { id: _this.catID } };
            _this.breadcrumbsInfo.paths[2] = { 'name': data['Data'].ProductName, 'url': '/products/product', 'params': { ProductID: _this.productId, catId: _this.catID } };
            _this.productDescriptionList = data['Data'].ShortDescription.split(" - ");
            if (data['Data'].ProductImages) {
                data['Data'].ProductImages.map(function (img) {
                    var image = {
                        small: img,
                        medium: img,
                        big: img
                    };
                    _this.galleryImages.push(image);
                });
            }
            ;
        });
    };
    ;
    ProductPageComponent.prototype.onSelectCompare = function () {
        this.showCompare = !this.showCompare;
    };
    ProductPageComponent.prototype.onSelectSize = function (ProductID) {
        this.productId = ProductID;
        this.getProductDetails();
        this.router.navigate(['products/product'], { queryParams: { ProductID: this.productId } });
    };
    ProductPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-page',
            template: __webpack_require__(/*! ./product-page.component.html */ "./src/app/products/product-page/product-page.component.html"),
            styles: [__webpack_require__(/*! ./product-page.component.scss */ "./src/app/products/product-page/product-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ProductPageComponent);
    return ProductPageComponent;
}());



/***/ }),

/***/ "./src/app/products/product-slider/product-slider.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/products/product-slider/product-slider.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ngx-gallery [options]=\"galleryOptions\" [images]=\"galleryImages\"></ngx-gallery>\r\n"

/***/ }),

/***/ "./src/app/products/product-slider/product-slider.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/products/product-slider/product-slider.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ngx-gallery /deep/ .ngx-gallery-image,\nngx-gallery /deep/ .ngx-gallery-thumbnail {\n  background-size: contain !important; }\n\nngx-gallery /deep/ .ngx-gallery-icon-content {\n  color: #ed1c24; }\n\nngx-gallery /deep/ .ngx-gallery-thumbnails .ngx-gallery-active {\n  border: 2px solid #ed1c24;\n  background-size: 64% !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1zbGlkZXIvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxwcm9kdWN0c1xccHJvZHVjdC1zbGlkZXJcXHByb2R1Y3Qtc2xpZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wcm9kdWN0cy9wcm9kdWN0LXNsaWRlci9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhc3NldHNcXHNjc3NcXHZhcmlhYmxlcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBOztFQUVFLG1DQUFtQyxFQUFBOztBQUdyQztFQUNFLGNDUHlCLEVBQUE7O0FEVTNCO0VBQ0UseUJDWHlCO0VEWXpCLCtCQUErQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1zbGlkZXIvcHJvZHVjdC1zbGlkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XHJcblxyXG5uZ3gtZ2FsbGVyeSAvZGVlcC8gLm5neC1nYWxsZXJ5LWltYWdlLFxyXG5uZ3gtZ2FsbGVyeSAvZGVlcC8gLm5neC1nYWxsZXJ5LXRodW1ibmFpbCB7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbm5neC1nYWxsZXJ5IC9kZWVwLyAubmd4LWdhbGxlcnktaWNvbi1jb250ZW50IHtcclxuICBjb2xvcjogJHByaW1hcnktYnRuLWNvbG9yXHJcbn1cclxuXHJcbm5neC1nYWxsZXJ5IC9kZWVwLyAubmd4LWdhbGxlcnktdGh1bWJuYWlscyAubmd4LWdhbGxlcnktYWN0aXZlIHtcclxuICBib3JkZXI6IDJweCBzb2xpZCAkcHJpbWFyeS1idG4tY29sb3I7XHJcbiAgYmFja2dyb3VuZC1zaXplOiA2NCUgIWltcG9ydGFudDtcclxufVxyXG4iLCIkcHJpbWFyeS1iZy1jb2xvcjogIzQ0NDQ0NDtcclxuJHByaW1hcnktYnRuLWNvbG9yOiAjZWQxYzI0OyJdfQ== */"

/***/ }),

/***/ "./src/app/products/product-slider/product-slider.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/products/product-slider/product-slider.component.ts ***!
  \*********************************************************************/
/*! exports provided: ProductSliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductSliderComponent", function() { return ProductSliderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_gallery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-gallery */ "./node_modules/ngx-gallery/bundles/ngx-gallery.umd.js");
/* harmony import */ var ngx_gallery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_gallery__WEBPACK_IMPORTED_MODULE_2__);



var ProductSliderComponent = /** @class */ (function () {
    function ProductSliderComponent() {
    }
    ProductSliderComponent.prototype.ngOnInit = function () {
        this.galleryOptions = [
            {
                width: '100%',
                height: '350px',
                thumbnailsColumns: 4,
                "previewZoom": true,
                "previewRotate": true,
                "previewCloseOnClick": true,
                "previewCloseOnEsc": true,
                "previewKeyboardNavigation": true,
                imageInfinityMove: true,
                imagePercent: 100,
                thumbnailsPercent: 30,
                arrowPrevIcon: "fa fa-angle-left",
                arrowNextIcon: "fa fa-angle-right",
                imageSwipe: true,
                imageAnimation: ngx_gallery__WEBPACK_IMPORTED_MODULE_2__["NgxGalleryAnimation"].Slide
            },
            // max-width 800
            {
                breakpoint: 800,
                width: '100%',
                height: '600px',
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20
            },
            // max-width 500
            {
                breakpoint: 500,
                thumbnailsPercent: 50,
                height: '200px',
                preview: true
            },
        ];
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProductSliderComponent.prototype, "galleryImages", void 0);
    ProductSliderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-slider',
            template: __webpack_require__(/*! ./product-slider.component.html */ "./src/app/products/product-slider/product-slider.component.html"),
            styles: [__webpack_require__(/*! ./product-slider.component.scss */ "./src/app/products/product-slider/product-slider.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProductSliderComponent);
    return ProductSliderComponent;
}());



/***/ }),

/***/ "./src/app/products/product-tabs/product-tabs.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-tabs/product-tabs.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"container\">\r\n  <div class=\"nav nav-tabs\" id=\"nav-tab\" role=\"tablist\">\r\n    <a class=\"nav-item nav-link active\" id=\"nav-home-tab\" (click)=\"getProductFeatures()\" data-toggle=\"tab\" href=\"#nav-features\" role=\"tab\"\r\n      aria-controls=\"nav-features\" aria-selected=\"true\">Features</a>\r\n\r\n    <a class=\"nav-item nav-link\" id=\"nav-profile-tab\" (click)=\"getProductSpecs()\" data-toggle=\"tab\" href=\"#nav-specifications\" role=\"tab\"\r\n      aria-controls=\"nav-specifications\" aria-selected=\"false\">Specifications</a>\r\n\r\n    <a class=\"nav-item nav-link\" (click)=\"getProductDownloads()\" id=\"nav-contact-tab\" data-toggle=\"tab\"\r\n      href=\"#nav-support\" role=\"tab\" aria-controls=\"nav-support\" aria-selected=\"false\">Support</a>\r\n  </div>\r\n</nav>\r\n<app-spinner *ngIf=\"loading\"></app-spinner>\r\n<div class=\"tab-content\" id=\"nav-tabContent\">\r\n  <!-- Tab Features -->\r\n  <div class=\"tab-pane fade show active\" id=\"nav-features\" role=\"tabpanel\" aria-labelledby=\"nav-home-tab\">\r\n    <app-widget-article *ngFor=\"let widget of widgets; let even = even\" [data]=\"widget\" [style]=\"'styleX'+even\">\r\n    </app-widget-article>\r\n\r\n    <section *ngIf=\"logos.length > 0\" class=\"container additional-features mt-4\">\r\n      <h5>Additional Features</h5>\r\n      <ul class=\"additional-features\">\r\n        <li *ngFor=\"let logo of logos\"><img [src]=\"logo.image\"></li>\r\n      </ul>\r\n    </section>\r\n  </div>\r\n\r\n  <!-- Tab Specifications -->\r\n  <div class=\"tab-pane fade\" id=\"nav-specifications\" role=\"tabpanel\" aria-labelledby=\"nav-profile-tab\">\r\n    <div class=\"container mt-4\">\r\n      <app-product-collapsible-panel [panels]=\"specifications\"></app-product-collapsible-panel>\r\n    </div>\r\n    <app-widget-article [style]=\"'styleHorizontal'\" [data]=\"support\"></app-widget-article>\r\n  </div>\r\n\r\n  <!-- Tab Support -->\r\n  <div class=\"tab-pane fade\" id=\"nav-support\" role=\"tabpanel\" aria-labelledby=\"nav-contact-tab\">\r\n    <div class=\"container\">\r\n      <ul class=\"support-links mt-4\">\r\n        <li *ngFor=\"let download of downloads\">\r\n          <a [href]=\"download.PDFLink\" target=\"_blank\" class=\"d-flex\">\r\n            <img src=\"assets/images/pdf-ico.svg\" width=\"50\">\r\n            <div class=\"d-flex flex-column m-3\">\r\n              <strong class=\"text-dark\">{{download.FileName}}</strong>\r\n              <small class=\"text-muted\">Download Product Card</small>\r\n              <small class=\"text-muted\">PDF File</small>\r\n            </div>\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n    <app-widget-article [style]=\"'styleHorizontal'\" [data]=\"support\"></app-widget-article>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/products/product-tabs/product-tabs.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-tabs/product-tabs.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".additional-features h5 {\n  font-size: 20px;\n  font-weight: 600;\n  text-align: center;\n  color: #000000; }\n\n.additional-features ul.additional-features {\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: center;\n  margin-top: 50px;\n  margin-bottom: 50px; }\n\n.additional-features ul.additional-features li {\n    flex: 0 0 20%;\n    margin-bottom: 40px; }\n\n.additional-features ul.additional-features li img {\n      max-width: 150px; }\n\n.nav-tabs .nav-item {\n  font-weight: bold;\n  color: #000; }\n\n.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {\n  border: 0;\n  color: red;\n  background-color: #fff;\n  border-bottom: 2px solid red; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC10YWJzL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxccHJvZHVjdHNcXHByb2R1Y3QtdGFic1xccHJvZHVjdC10YWJzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsY0FBYyxFQUFBOztBQUxsQjtFQVNJLGFBQWE7RUFDYixlQUFlO0VBQ2YsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFidkI7SUFnQlEsYUFBWTtJQUNaLG1CQUFtQixFQUFBOztBQWpCM0I7TUFtQlUsZ0JBQWdCLEVBQUE7O0FBSzFCO0VBQ0ksaUJBQWlCO0VBQ2pCLFdBQVcsRUFBQTs7QUFFZjtFQUNJLFNBQVM7RUFDVCxVQUFVO0VBQ1Ysc0JBQXNCO0VBQ3RCLDRCQUE0QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC10YWJzL3Byb2R1Y3QtdGFicy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hZGRpdGlvbmFsLWZlYXR1cmVzIHtcclxuICBoNSB7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgfVxyXG5cclxuICB1bC5hZGRpdGlvbmFsLWZlYXR1cmVzIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xyXG5cclxuICAgIGxpIHtcclxuICAgICAgICBmbGV4OjAgMCAyMCU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNDBweDtcclxuICAgICAgaW1nIHtcclxuICAgICAgICAgIG1heC13aWR0aDogMTUwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuLm5hdi10YWJzIC5uYXYtaXRlbSB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG59XHJcbi5uYXYtdGFicyAubmF2LWl0ZW0uc2hvdyAubmF2LWxpbmssIC5uYXYtdGFicyAubmF2LWxpbmsuYWN0aXZlIHtcclxuICAgIGJvcmRlcjogMDtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIHJlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/products/product-tabs/product-tabs.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/products/product-tabs/product-tabs.component.ts ***!
  \*****************************************************************/
/*! exports provided: ProductTabsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductTabsComponent", function() { return ProductTabsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ProductTabsComponent = /** @class */ (function () {
    function ProductTabsComponent(catService, route) {
        this.catService = catService;
        this.route = route;
        this.style2 = { align: 'center', theme: 'white' };
        this.support = {
            title: "Support",
            description: "Access and download more product information and find quick and easy troubleshooting advice.",
            url: "/support",
            urlTxt: "Get Support",
            image: "assets/images/support-image.jpg"
        };
        this.widgets = [];
        this.logos = [];
        // logos = [
        //   "assets/images/additional-features/alexa.png",
        //   "assets/images/additional-features/dolby-audio.png",
        //   "assets/images/additional-features/netflix.png",
        //   "assets/images/additional-features/onkyo.png",
        //   "assets/images/additional-features/secure.png",
        //   "assets/images/additional-features/usb-cloning.png",
        //   "assets/images/additional-features/screen-share.png",
        //   "assets/images/additional-features/social-feed.png",
        //   "assets/images/additional-features/eco-friendly.png"
        // ]
        this.specifications = [];
        this.downloads = [];
    }
    ProductTabsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.productId = params['ProductID'];
            _this.getProductFeatures();
        });
    };
    ProductTabsComponent.prototype.getProductDownloads = function () {
        var _this = this;
        if (this.downloads.length == 0) {
            this.loading = true;
            var params = { ProductID: this.productId };
            this.catService.getProductDownloads(params).subscribe(function (data) {
                _this.loading = false;
                _this.downloads = data['Data'];
            });
        }
    };
    ProductTabsComponent.prototype.getProductSpecs = function () {
        var _this = this;
        if (this.specifications.length == 0) {
            this.loading = true;
            var params = { ProductID: this.productId };
            this.catService.getProductSpecs(params).subscribe(function (data) {
                _this.loading = false;
                _this.specifications = data['Data'];
            });
        }
    };
    ProductTabsComponent.prototype.getProductFeatures = function () {
        var _this = this;
        if (this.widgets.length == 0) {
            this.logos = [];
            this.loading = true;
            var params = { ProductID: this.productId };
            this.catService.getProductFeatures(params).subscribe(function (data) {
                _this.loading = false;
                data['Data'].map(function (widget) {
                    var item = {
                        title: widget.Title,
                        logo: "''",
                        description: widget.Description,
                        image: widget.featureImage,
                        layoutId: widget.LayoutID,
                        ShowInAdditional: widget.ShowInAdditional
                    };
                    if (!item.ShowInAdditional)
                        _this.widgets.push(item);
                    else {
                        _this.logos.push(item);
                    }
                });
            });
        }
    };
    ProductTabsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-tabs',
            template: __webpack_require__(/*! ./product-tabs.component.html */ "./src/app/products/product-tabs/product-tabs.component.html"),
            styles: [__webpack_require__(/*! ./product-tabs.component.scss */ "./src/app/products/product-tabs/product-tabs.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ProductTabsComponent);
    return ProductTabsComponent;
}());



/***/ }),

/***/ "./src/app/products/products-filters/products-filters.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/products/products-filters/products-filters.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <button class=\"btn-outline-danger btn btn-block mb-3\" (click)=\"applyFilters()\">Apply</button> -->\r\n<div class=\"panel-group mb-5\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">\r\n  <div *ngFor=\"let filter of filters;let i = index\" class=\"panel panel-default\">\r\n    <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">\r\n      <h4 class=\"panel-title\">\r\n        <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" [href]=\"'#collapse' + i\"\r\n          [attr.aria-expanded]=\"true ? i < 2 : false\" [attr.aria-controls]=\"'collapse'+i\">\r\n          {{filter.Name}}\r\n        </a>\r\n      </h4>\r\n    </div>\r\n    <div [id]=\"'collapse' + i\" class=\"panel-collapse collapse in\" [ngClass]=\"{'show': i < 2}\" role=\"tabpanel\"\r\n      [attr.aria-labelledby]=\"'heading'+i\">\r\n      <div class=\"panel-body\">\r\n        <ul class=\"mb-3\">\r\n          <li *ngFor=\"let option of filter._lstAttributeOptions\">\r\n            <div class=\"custom-control custom-checkbox\">\r\n              <input type=\"checkbox\" class=\"custom-control-input\" [id]=\"'customCheck' + option.OptionID\"\r\n                (click)=\"onFilter(option.OptionID)\">\r\n              <label class=\"custom-control-label\" [attr.for]=\"'customCheck' + option.OptionID\">{{option.Name}}</label>\r\n            </div>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/products/products-filters/products-filters.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/products/products-filters/products-filters.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-default > .panel-heading {\n  color: #333;\n  background-color: #fff;\n  border-color: #e4e5e7;\n  padding: 0;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n.panel-default > .panel-heading a {\n  display: block;\n  padding: 0;\n  font-size: 16px;\n  font-weight: 600;\n  color: #000000; }\n\n.panel-default > .panel-heading a:before {\n  content: \"\";\n  position: relative;\n  top: 1px;\n  display: inline-block;\n  font-family: 'Glyphicons Halflings';\n  font-style: normal;\n  font-weight: bold;\n  color: #ed1c24;\n  margin-right: 10px;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  float: left;\n  transition: -webkit-transform .25s linear;\n  transition: transform .25s linear;\n  transition: transform .25s linear, -webkit-transform .25s linear;\n  -webkit-transition: -webkit-transform .25s linear;\n  width: 17px;\n  height: 17px;\n  border: 2px solid #ed1c24;\n  border-radius: 100%;\n  line-height: 13px;\n  text-align: center;\n  font-size: 15px; }\n\n.panel-default > .panel-heading a[aria-expanded=\"true\"] {\n  background-color: #fff; }\n\n.panel-default > .panel-heading a[aria-expanded=\"true\"]:before {\n  content: \"\\2212\";\n  -webkit-transform: rotate(180deg);\n  transform: rotate(180deg); }\n\n.panel-default > .panel-heading a[aria-expanded=\"false\"]:before {\n  content: \"\\002b\";\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg); }\n\n.accordion-option {\n  width: 100%;\n  float: left;\n  clear: both;\n  margin: 15px 0; }\n\n.accordion-option .title {\n  font-size: 20px;\n  font-weight: bold;\n  float: left;\n  padding: 0;\n  margin: 0; }\n\n.accordion-option .toggle-accordion {\n  float: right;\n  font-size: 16px;\n  color: #6a6c6f; }\n\n.panel-body {\n  padding: 10px 28px; }\n\n.panel-body ul li {\n    font-size: 14px;\n    font-weight: 500;\n    color: #838a93; }\n\n.custom-control-input:checked ~ .custom-control-label::before {\n  color: #fff;\n  border-color: #ed1c24;\n  background-color: #ed1c24; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdHMtZmlsdGVycy9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXHByb2R1Y3RzXFxwcm9kdWN0cy1maWx0ZXJzXFxwcm9kdWN0cy1maWx0ZXJzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wcm9kdWN0cy9wcm9kdWN0cy1maWx0ZXJzL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFzc2V0c1xcc2Nzc1xcdmFyaWFibGVzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixVQUFVO0VBQ1YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsY0FBYztFQUNkLFVBQVU7RUFDVixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixxQkFBcUI7RUFDckIsbUNBQW1DO0VBQ25DLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsY0M1QnlCO0VENkJ6QixrQkFBa0I7RUFDbEIsbUNBQW1DO0VBQ25DLGtDQUFrQztFQUNsQyxXQUFXO0VBQ1gseUNBQXlDO0VBQ3pDLGlDQUFpQztFQUNqQyxnRUFBZ0U7RUFDaEUsaURBQWlEO0VBQ2pELFdBQVc7RUFDWCxZQUFZO0VBQ1oseUJDdkN5QjtFRHdDekIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQUdqQjtFQUNFLHNCQUFzQixFQUFBOztBQUd4QjtFQUNFLGdCQUFnQjtFQUNoQixpQ0FBaUM7RUFDakMseUJBQXlCLEVBQUE7O0FBRzNCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdDQUFnQztFQUNoQyx3QkFBd0IsRUFBQTs7QUFHMUI7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCxjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFNBQVMsRUFBQTs7QUFHWDtFQUNFLFlBQVk7RUFDWixlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQUdoQjtFQUNFLGtCQUFrQixFQUFBOztBQURwQjtJQUtNLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsY0FBYyxFQUFBOztBQUlwQjtFQUNJLFdBQVc7RUFDWCxxQkNoR3VCO0VEaUd2Qix5QkNqR3VCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0cy9wcm9kdWN0cy1maWx0ZXJzL3Byb2R1Y3RzLWZpbHRlcnMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XHJcblxyXG4ucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyB7XHJcbiAgY29sb3I6ICMzMzM7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICBib3JkZXItY29sb3I6ICNlNGU1ZTc7XHJcbiAgcGFkZGluZzogMDtcclxuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gIHVzZXItc2VsZWN0OiBub25lO1xyXG59XHJcblxyXG4ucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwYWRkaW5nOiAwO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG59XHJcblxyXG4ucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhOmJlZm9yZSB7XHJcbiAgY29udGVudDogXCJcIjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAxcHg7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGZvbnQtZmFtaWx5OiAnR2x5cGhpY29ucyBIYWxmbGluZ3MnO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBjb2xvcjogJHByaW1hcnktYnRuLWNvbG9yO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcclxuICAtbW96LW9zeC1mb250LXNtb290aGluZzogZ3JheXNjYWxlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIC4yNXMgbGluZWFyO1xyXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuMjVzIGxpbmVhcjtcclxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjI1cyBsaW5lYXIsIC13ZWJraXQtdHJhbnNmb3JtIC4yNXMgbGluZWFyO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjI1cyBsaW5lYXI7XHJcbiAgd2lkdGg6IDE3cHg7XHJcbiAgaGVpZ2h0OiAxN3B4O1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICRwcmltYXJ5LWJ0bi1jb2xvcjtcclxuICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gIGxpbmUtaGVpZ2h0OiAxM3B4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcbi5wYW5lbC1kZWZhdWx0Pi5wYW5lbC1oZWFkaW5nIGFbYXJpYS1leHBhbmRlZD1cInRydWVcIl0ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi5wYW5lbC1kZWZhdWx0Pi5wYW5lbC1oZWFkaW5nIGFbYXJpYS1leHBhbmRlZD1cInRydWVcIl06YmVmb3JlIHtcclxuICBjb250ZW50OiBcIlxcMjIxMlwiO1xyXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcclxuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG59XHJcblxyXG4ucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhW2FyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiXTpiZWZvcmUge1xyXG4gIGNvbnRlbnQ6IFwiXFwwMDJiXCI7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xyXG59XHJcblxyXG4uYWNjb3JkaW9uLW9wdGlvbiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgY2xlYXI6IGJvdGg7XHJcbiAgbWFyZ2luOiAxNXB4IDA7XHJcbn1cclxuXHJcbi5hY2NvcmRpb24tb3B0aW9uIC50aXRsZSB7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4uYWNjb3JkaW9uLW9wdGlvbiAudG9nZ2xlLWFjY29yZGlvbiB7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBjb2xvcjogIzZhNmM2ZjtcclxufVxyXG5cclxuLnBhbmVsLWJvZHkge1xyXG4gIHBhZGRpbmc6IDEwcHggMjhweDtcclxuXHJcbiAgdWwge1xyXG4gICAgbGkge1xyXG4gICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIGNvbG9yOiAjODM4YTkzO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uY3VzdG9tLWNvbnRyb2wtaW5wdXQ6Y2hlY2tlZH4uY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmJlZm9yZSB7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGJvcmRlci1jb2xvcjogJHByaW1hcnktYnRuLWNvbG9yO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktYnRuLWNvbG9yO1xyXG59IiwiJHByaW1hcnktYmctY29sb3I6ICM0NDQ0NDQ7XHJcbiRwcmltYXJ5LWJ0bi1jb2xvcjogI2VkMWMyNDsiXX0= */"

/***/ }),

/***/ "./src/app/products/products-filters/products-filters.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/products/products-filters/products-filters.component.ts ***!
  \*************************************************************************/
/*! exports provided: ProductsFiltersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsFiltersComponent", function() { return ProductsFiltersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var ProductsFiltersComponent = /** @class */ (function () {
    function ProductsFiltersComponent(route, platformId) {
        this.route = route;
        this.platformId = platformId;
        this.filters = [];
        this.emitFilters = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.optionIDs = [];
    }
    ProductsFiltersComponent.prototype.ngOnInit = function () {
    };
    ProductsFiltersComponent.prototype.onFilter = function (id) {
        var localStore;
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            localStore = localStorage.getItem('countryID');
        }
        if (this.optionIDs.includes(id)) {
            var index = this.optionIDs.indexOf(id);
            this.optionIDs.splice(index, 1);
        }
        else {
            this.optionIDs.push(id);
        }
        var params = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_3__["SearchModel"]();
        params.CountryID = localStore;
        params.Keyword = this.route.snapshot.queryParams['Keyword'];
        params.OptionIDs = this.optionIDs.toString();
        params.filters = true;
        this.emitFilters.emit(params);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProductsFiltersComponent.prototype, "filters", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProductsFiltersComponent.prototype, "emitFilters", void 0);
    ProductsFiltersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-products-filters',
            template: __webpack_require__(/*! ./products-filters.component.html */ "./src/app/products/products-filters/products-filters.component.html"),
            styles: [__webpack_require__(/*! ./products-filters.component.scss */ "./src/app/products/products-filters/products-filters.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], Object])
    ], ProductsFiltersComponent);
    return ProductsFiltersComponent;
}());



/***/ }),

/***/ "./src/app/products/products-page/products-page.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/products/products-page/products-page.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container my-4\">\r\n  <h3 class=\"mb-4\">Results for \"{{keyword}}\" ({{products.length}} Items found)</h3>\r\n\r\n  <div class=\"row mt-5\">\r\n    <div class=\"col-md-3 col-12\">\r\n      <app-products-filters [filters]=\"filters\" (emitFilters)=\"getSearchResults($event)\"></app-products-filters>\r\n    </div>\r\n    <div class=\"col-md-9 col-12\">\r\n      <!-- <div class=\"tags mb-4\">\r\n        <ul>\r\n          <li *ngFor=\"let tag of tags;let i = index\"><span>{{tag.name}}</span><button (click)=\"removeTag(i)\"\r\n              class=\"plain-btn\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button></li>\r\n          <li *ngIf=\"tags.length > 0\"><button (click)=\"clearAllTags()\" class=\"plain-btn\">Clear all</button></li>\r\n        </ul>\r\n      </div> -->\r\n\r\n      <div>\r\n        <app-spinner *ngIf=\"loading\"></app-spinner>\r\n        <div class=\"alert alert-info\" *ngIf=\"noResultsFound\">No results found!</div>\r\n        <ul class=\"products-list\">\r\n          <li *ngFor=\"let product of products\">\r\n            <app-product-item [product]=\"product\"></app-product-item>\r\n          </li>\r\n        </ul>\r\n\r\n        <!-- <nav aria-label=\"Page navigation example\">\r\n          <ul class=\"pagination justify-content-center\">\r\n            <li class=\"page-item disabled\">\r\n              <a class=\"page-link\" href=\"#\" tabindex=\"-1\"><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\r\n                Previous</a>\r\n            </li>\r\n            <li class=\"page-item active\"><a class=\"page-link\" href=\"#\">1</a></li>\r\n            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\r\n            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\r\n            <li class=\"page-item\">\r\n              <a class=\"page-link\" href=\"#\">Next <i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></a>\r\n            </li>\r\n          </ul>\r\n        </nav> -->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/products/products-page/products-page.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/products/products-page/products-page.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tags ul {\n  display: inline-block;\n  margin: 0;\n  padding: 0; }\n  .tags ul li {\n    font-size: 14px;\n    font-weight: 500;\n    color: #838a93;\n    float: left;\n    border-radius: 14.5px;\n    border: solid 1px #cbd0d4;\n    background-color: #f5f6f7;\n    margin: 0 4px;\n    padding: 0 10px; }\n  .tags ul li button {\n      font-size: 11px;\n      font-weight: 500;\n      color: #b0b7c0;\n      margin: 0 5px; }\n  .tags ul li:last-child {\n      background: none;\n      border: 0; }\n  .tags ul li:last-child button {\n        color: #ed1c24;\n        font-weight: bold; }\n  .products-list {\n  display: flex;\n  flex-wrap: wrap; }\n  .products-list li {\n    flex: 0 0 33%; }\n  @media (max-width: 500px) {\n    .products-list li {\n      flex: 0 0 100%; } }\n  .page-link {\n  color: #444;\n  font-size: 12px;\n  font-weight: bold; }\n  .page-item.active .page-link {\n  background-color: #ed1c24;\n  border-color: #ed1c24; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdHMtcGFnZS9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXHByb2R1Y3RzXFxwcm9kdWN0cy1wYWdlXFxwcm9kdWN0cy1wYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wcm9kdWN0cy9wcm9kdWN0cy1wYWdlL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFzc2V0c1xcc2Nzc1xcdmFyaWFibGVzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFFSSxxQkFBcUI7RUFDckIsU0FBUztFQUNULFVBQVUsRUFBQTtFQUpkO0lBT00sZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsV0FBVztJQUNYLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYixlQUFlLEVBQUE7RUFmckI7TUFrQlEsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixjQUFjO01BQ2QsYUFBYSxFQUFBO0VBckJyQjtNQXlCUSxnQkFBZ0I7TUFDaEIsU0FBUyxFQUFBO0VBMUJqQjtRQTZCVSxjQzlCaUI7UUQrQmpCLGlCQUFpQixFQUFBO0VBTzNCO0VBRUUsYUFBYTtFQUNiLGVBQWUsRUFBQTtFQUhqQjtJQU1JLGFBQWEsRUFBQTtFQUdmO0lBVEY7TUFXTSxjQUFjLEVBQUEsRUFDZjtFQUtMO0VBQ0UsV0FBVztFQUNYLGVBQWU7RUFDZixpQkFBaUIsRUFBQTtFQUVuQjtFQUNJLHlCQzdEdUI7RUQ4RHZCLHFCQzlEdUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3RzLXBhZ2UvcHJvZHVjdHMtcGFnZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJy4uLy4uLy4uL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzJztcclxuXHJcbi50YWdzIHtcclxuICB1bCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG5cclxuICAgIGxpIHtcclxuICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBjb2xvcjogIzgzOGE5MztcclxuICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE0LjVweDtcclxuICAgICAgYm9yZGVyOiBzb2xpZCAxcHggI2NiZDBkNDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjZmNztcclxuICAgICAgbWFyZ2luOiAwIDRweDtcclxuICAgICAgcGFkZGluZzogMCAxMHB4O1xyXG5cclxuICAgICAgYnV0dG9uIHtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICBjb2xvcjogI2IwYjdjMDtcclxuICAgICAgICBtYXJnaW46IDAgNXB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG5cclxuICAgICAgICBidXR0b24ge1xyXG4gICAgICAgICAgY29sb3I6ICRwcmltYXJ5LWJ0bi1jb2xvcjtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnByb2R1Y3RzLWxpc3Qge1xyXG5cclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxuXHJcbiAgbGkge1xyXG4gICAgZmxleDogMCAwIDMzJTtcclxuICB9XHJcblxyXG4gIEBtZWRpYShtYXgtd2lkdGg6NTAwcHgpIHtcclxuICAgIGxpIHtcclxuICAgICAgZmxleDogMCAwIDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG5cclxuLnBhZ2UtbGluayB7XHJcbiAgY29sb3I6ICM0NDQ7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5wYWdlLWl0ZW0uYWN0aXZlIC5wYWdlLWxpbmsge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktYnRuLWNvbG9yO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAkcHJpbWFyeS1idG4tY29sb3I7XHJcbn1cclxuIiwiJHByaW1hcnktYmctY29sb3I6ICM0NDQ0NDQ7XHJcbiRwcmltYXJ5LWJ0bi1jb2xvcjogI2VkMWMyNDsiXX0= */"

/***/ }),

/***/ "./src/app/products/products-page/products-page.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/products/products-page/products-page.component.ts ***!
  \*******************************************************************/
/*! exports provided: ProductsPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageComponent", function() { return ProductsPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");






var ProductsPageComponent = /** @class */ (function () {
    function ProductsPageComponent(catService, route, platformId) {
        this.catService = catService;
        this.route = route;
        this.platformId = platformId;
        this.tags = [
            { id: 1, 'name': '4K Smart' },
            { id: 2, 'name': '49”' }
        ];
        this.products = [];
        this.filters = [];
    }
    ProductsPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.products = [];
            _this.keyword = params['Keyword'];
            _this.getFilters();
            _this.getSearchResults();
        });
    };
    ;
    ProductsPageComponent.prototype.removeTag = function (index) {
        this.tags.splice(index, 1);
    };
    ;
    ProductsPageComponent.prototype.clearAllTags = function () {
        this.tags = [];
    };
    ;
    ProductsPageComponent.prototype.getSearchResults = function (e) {
        var _this = this;
        if (e === void 0) { e = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_5__["SearchModel"](); }
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            e.CountryID = localStorage.getItem('countryID');
        }
        this.products = [];
        this.loading = true;
        this.noResultsFound = false;
        if (e.filters) {
            this.catService.searchProducts(e).subscribe(function (results) {
                _this.loading = false;
                _this.products = results['Data'];
                if (results['Data'].length == 0) {
                    _this.noResultsFound = true;
                }
            });
        }
        else {
            var params = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_5__["SearchModel"]();
            params.Keyword = this.route.snapshot.queryParams['Keyword'];
            params.CountryID = this.route.snapshot.queryParams['CountryID'] || e.CountryID;
            this.catService.searchProducts(params).subscribe(function (results) {
                _this.loading = false;
                _this.products = results['Data'];
                if (results['Data'].length == 0) {
                    _this.noResultsFound = true;
                }
            });
        }
    };
    ;
    ProductsPageComponent.prototype.getFilters = function () {
        var _this = this;
        this.catService.getAttributes(this.keyword).subscribe(function (filters) {
            _this.filters = filters['Data'];
        });
    };
    ;
    ProductsPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-products-page',
            template: __webpack_require__(/*! ./products-page.component.html */ "./src/app/products/products-page/products-page.component.html"),
            styles: [__webpack_require__(/*! ./products-page.component.scss */ "./src/app/products/products-page/products-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            Object])
    ], ProductsPageComponent);
    return ProductsPageComponent;
}());



/***/ }),

/***/ "./src/app/products/products-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/products/products-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: ProductsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsRoutingModule", function() { return ProductsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _product_page_product_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-page/product-page.component */ "./src/app/products/product-page/product-page.component.ts");
/* harmony import */ var _products_page_products_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products-page/products-page.component */ "./src/app/products/products-page/products-page.component.ts");
/* harmony import */ var _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-list/product-list.component */ "./src/app/products/product-list/product-list.component.ts");
/* harmony import */ var _compare_page_compare_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./compare-page/compare-page.component */ "./src/app/products/compare-page/compare-page.component.ts");







var routes = [
    { path: 'product', component: _product_page_product_page_component__WEBPACK_IMPORTED_MODULE_3__["ProductPageComponent"] },
    { path: 'product-list', component: _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_5__["ProductListComponent"] },
    { path: 'products-search', component: _products_page_products_page_component__WEBPACK_IMPORTED_MODULE_4__["ProductsPageComponent"] },
    { path: 'compare', component: _compare_page_compare_page_component__WEBPACK_IMPORTED_MODULE_6__["ComparePageComponent"] },
    { path: '', redirectTo: 'products-search', pathMatch: 'full' }
];
var ProductsRoutingModule = /** @class */ (function () {
    function ProductsRoutingModule() {
    }
    ProductsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ProductsRoutingModule);
    return ProductsRoutingModule;
}());



/***/ }),

/***/ "./src/app/products/products.module.ts":
/*!*********************************************!*\
  !*** ./src/app/products/products.module.ts ***!
  \*********************************************/
/*! exports provided: ProductsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsModule", function() { return ProductsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_gallery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-gallery */ "./node_modules/ngx-gallery/bundles/ngx-gallery.umd.js");
/* harmony import */ var ngx_gallery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ngx_gallery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _products_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products-routing.module */ "./src/app/products/products-routing.module.ts");
/* harmony import */ var _product_page_product_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-page/product-page.component */ "./src/app/products/product-page/product-page.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _product_slider_product_slider_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./product-slider/product-slider.component */ "./src/app/products/product-slider/product-slider.component.ts");
/* harmony import */ var _product_tabs_product_tabs_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./product-tabs/product-tabs.component */ "./src/app/products/product-tabs/product-tabs.component.ts");
/* harmony import */ var _products_page_products_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./products-page/products-page.component */ "./src/app/products/products-page/products-page.component.ts");
/* harmony import */ var _products_filters_products_filters_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./products-filters/products-filters.component */ "./src/app/products/products-filters/products-filters.component.ts");
/* harmony import */ var _product_item_product_item_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./product-item/product-item.component */ "./src/app/products/product-item/product-item.component.ts");
/* harmony import */ var _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./product-list/product-list.component */ "./src/app/products/product-list/product-list.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _compare_page_compare_page_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./compare-page/compare-page.component */ "./src/app/products/compare-page/compare-page.component.ts");
/* harmony import */ var _compare_panel_compare_panel_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./compare-panel/compare-panel.component */ "./src/app/products/compare-panel/compare-panel.component.ts");
















var ProductsModule = /** @class */ (function () {
    function ProductsModule() {
    }
    ProductsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_product_page_product_page_component__WEBPACK_IMPORTED_MODULE_5__["ProductPageComponent"], _product_slider_product_slider_component__WEBPACK_IMPORTED_MODULE_7__["ProductSliderComponent"], _product_tabs_product_tabs_component__WEBPACK_IMPORTED_MODULE_8__["ProductTabsComponent"], _products_page_products_page_component__WEBPACK_IMPORTED_MODULE_9__["ProductsPageComponent"], _products_filters_products_filters_component__WEBPACK_IMPORTED_MODULE_10__["ProductsFiltersComponent"], _product_item_product_item_component__WEBPACK_IMPORTED_MODULE_11__["ProductItemComponent"], _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_12__["ProductListComponent"], _compare_page_compare_page_component__WEBPACK_IMPORTED_MODULE_14__["ComparePageComponent"], _compare_panel_compare_panel_component__WEBPACK_IMPORTED_MODULE_15__["ComparePanelComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _products_routing_module__WEBPACK_IMPORTED_MODULE_4__["ProductsRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
                ngx_gallery__WEBPACK_IMPORTED_MODULE_3__["NgxGalleryModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormsModule"]
            ]
        })
    ], ProductsModule);
    return ProductsModule;
}());



/***/ }),

/***/ "./src/app/services/category.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/category.service.ts ***!
  \**********************************************/
/*! exports provided: CategoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryService", function() { return CategoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api */ "./src/app/api.ts");



// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

var CategoryService = /** @class */ (function () {
    function CategoryService(http, proxy) {
        this.http = http;
        this.proxy = proxy;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
            'Access-Control-Allow-Headers': "*",
            'Access-Control-Allow-Credentials': 'true',
        });
    }
    // get category items
    CategoryService.prototype.getCategoryItems = function (params) {
        return this.http.get(this.proxy.api + 'Product/GetProductsByCategory?', { headers: this.headers, params: params });
    };
    // get product details
    CategoryService.prototype.getProductDetails = function (params) {
        return this.http.get(this.proxy.api + 'Product/GetProductDetails?', { headers: this.headers, params: params });
    };
    // get product downloads
    CategoryService.prototype.getProductDownloads = function (params) {
        return this.http.get(this.proxy.api + 'Product/GetProductDownloads?', { headers: this.headers, params: params });
    };
    // get product specs
    CategoryService.prototype.getProductSpecs = function (params) {
        return this.http.get(this.proxy.api + 'Product/GetProductSpecs?', { headers: this.headers, params: params });
    };
    // get product features
    CategoryService.prototype.getProductFeatures = function (params) {
        return this.http.get(this.proxy.api + 'Product/GetProductFeatures?', { headers: this.headers, params: params });
    };
    // search products
    CategoryService.prototype.searchProducts = function (params) {
        return this.http.get(this.proxy.api + 'Product/SearchProducts?', { headers: this.headers, params: params });
    };
    // get search attribute
    CategoryService.prototype.getAttributes = function (keyword) {
        if (keyword === void 0) { keyword = ''; }
        return this.http.get(this.proxy.api + 'Product/GetAttributes?Keyword=' + keyword, { headers: this.headers });
    };
    // get product to compare
    CategoryService.prototype.getProductToCompare = function (params) {
        return this.http.get(this.proxy.api + 'Product/GetProductData?', { headers: this.headers, params: params });
    };
    CategoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _api__WEBPACK_IMPORTED_MODULE_3__["ProxyUrl"]])
    ], CategoryService);
    return CategoryService;
}());



/***/ })

}]);
//# sourceMappingURL=products-products-module.js.map
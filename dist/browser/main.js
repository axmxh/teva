(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./core/core.module": [
		"./src/app/core/core.module.ts"
	],
	"./home/home.module": [
		"./src/app/home/home.module.ts",
		"default~home-home-module~support-support-module",
		"home-home-module"
	],
	"./news/news.module": [
		"./src/app/news/news.module.ts",
		"news-news-module"
	],
	"./products/products.module": [
		"./src/app/products/products.module.ts",
		"products-products-module"
	],
	"./support/support.module": [
		"./src/app/support/support.module.ts",
		"default~home-home-module~support-support-module",
		"support-support-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/api.ts":
/*!************************!*\
  !*** ./src/app/api.ts ***!
  \************************/
/*! exports provided: ProxyUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProxyUrl", function() { return ProxyUrl; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ProxyUrl = /** @class */ (function () {
    function ProxyUrl() {
        // api: string = 'http://m.elarabygroup.com:5052/api/api/';
        this.api = 'https://www.toshiba-teva.com/api/api/';
    }
    // api: string = 'http://localhost:4200/api/api/';
    ProxyUrl.prototype.ngOnInit = function () { };
    ProxyUrl = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], ProxyUrl);
    return ProxyUrl;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./core/page-not-found/page-not-found.component */ "./src/app/core/page-not-found/page-not-found.component.ts");




var routes = [
    { path: '', loadChildren: './home/home.module#HomeModule' },
    { path: 'products', loadChildren: './products/products.module#ProductsModule' },
    { path: 'news', loadChildren: './news/news.module#NewsModule' },
    { path: 'support', loadChildren: './support/support.module#SupportModule' },
    { path: 'core', loadChildren: './core/core.module#CoreModule' },
    { path: 'page-not-found', component: _core_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_3__["PageNotFoundComponent"] },
    { path: '**', component: _core_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_3__["PageNotFoundComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
                    scrollPositionRestoration: 'enabled'
                })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<router-outlet></router-outlet>\r\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'teva';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./api */ "./src/app/api.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"].withServerTransition({ appId: 'serverApp' }),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_6__["CoreModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"]
            ],
            providers: [_api__WEBPACK_IMPORTED_MODULE_7__["ProxyUrl"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/core/core-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/core/core-routing.module.ts ***!
  \*********************************************/
/*! exports provided: CoreRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreRoutingModule", function() { return CoreRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _footer_content_footer_content_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer-content/footer-content.component */ "./src/app/core/footer-content/footer-content.component.ts");




var routes = [
    { path: 'footer-content', component: _footer_content_footer_content_component__WEBPACK_IMPORTED_MODULE_3__["FooterContentComponent"] },
    { path: '', redirectTo: 'footer-content', pathMatch: 'full' }
];
var CoreRoutingModule = /** @class */ (function () {
    function CoreRoutingModule() {
    }
    CoreRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], CoreRoutingModule);
    return CoreRoutingModule;
}());



/***/ }),

/***/ "./src/app/core/core.module.ts":
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header/header.component */ "./src/app/core/header/header.component.ts");
/* harmony import */ var _region_select_region_select_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./region-select/region-select.component */ "./src/app/core/region-select/region-select.component.ts");
/* harmony import */ var _main_menu_main_menu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main-menu/main-menu.component */ "./src/app/core/main-menu/main-menu.component.ts");
/* harmony import */ var _top_search_top_search_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./top-search/top-search.component */ "./src/app/core/top-search/top-search.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/core/footer/footer.component.ts");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/core/page-not-found/page-not-found.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/header.service */ "./src/app/services/header.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _footer_content_footer_content_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./footer-content/footer-content.component */ "./src/app/core/footer-content/footer-content.component.ts");
/* harmony import */ var _core_routing_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./core-routing.module */ "./src/app/core/core-routing.module.ts");















var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"],
                _region_select_region_select_component__WEBPACK_IMPORTED_MODULE_4__["RegionSelectComponent"],
                _main_menu_main_menu_component__WEBPACK_IMPORTED_MODULE_5__["MainMenuComponent"],
                _top_search_top_search_component__WEBPACK_IMPORTED_MODULE_6__["TopSearchComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__["FooterComponent"],
                _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_8__["PageNotFoundComponent"],
                _footer_content_footer_content_component__WEBPACK_IMPORTED_MODULE_13__["FooterContentComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _core_routing_module__WEBPACK_IMPORTED_MODULE_14__["CoreRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_9__["RouterModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_12__["SharedModule"]
            ],
            exports: [
                _header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"],
                _region_select_region_select_component__WEBPACK_IMPORTED_MODULE_4__["RegionSelectComponent"],
                _main_menu_main_menu_component__WEBPACK_IMPORTED_MODULE_5__["MainMenuComponent"],
                _top_search_top_search_component__WEBPACK_IMPORTED_MODULE_6__["TopSearchComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__["FooterComponent"],
                _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_8__["PageNotFoundComponent"]
            ],
            providers: [_services_header_service__WEBPACK_IMPORTED_MODULE_10__["HeaderService"]]
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/core/footer-content/footer-content.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/core/footer-content/footer-content.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\r\n  <div class=\"row\">\r\n      <p [innerHTML]=\"content | safe:'html'\"></p>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/core/footer-content/footer-content.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/core/footer-content/footer-content.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvZm9vdGVyLWNvbnRlbnQvZm9vdGVyLWNvbnRlbnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/core/footer-content/footer-content.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/core/footer-content/footer-content.component.ts ***!
  \*****************************************************************/
/*! exports provided: FooterContentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterContentComponent", function() { return FooterContentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/header.service */ "./src/app/services/header.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");





var FooterContentComponent = /** @class */ (function () {
    function FooterContentComponent(headerService, route) {
        this.headerService = headerService;
        this.route = route;
        this.content = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_4__["ModalData"]();
    }
    FooterContentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.contentId = params.id;
            _this.getContent();
        });
    };
    FooterContentComponent.prototype.getContent = function () {
        var _this = this;
        this.content = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_4__["ModalData"]();
        this.headerService.getStaticLinksData(this.contentId).subscribe(function (data) {
            _this.content = data['Data'].Content;
        });
    };
    FooterContentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer-content',
            template: __webpack_require__(/*! ./footer-content.component.html */ "./src/app/core/footer-content/footer-content.component.html"),
            styles: [__webpack_require__(/*! ./footer-content.component.scss */ "./src/app/core/footer-content/footer-content.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], FooterContentComponent);
    return FooterContentComponent;
}());



/***/ }),

/***/ "./src/app/core/footer/footer.component.html":
/*!***************************************************!*\
  !*** ./src/app/core/footer/footer.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"pt-3\">\r\n  <div class=\"container\">\r\n\r\n    <ul class=\"d-flex float-left\">\r\n      <li *ngFor=\"let link of footerLinks.FooterLinks\">\r\n        <a [routerLink]=\"['/core/footer-content']\" [queryParams]=\"{id: link.ID}\">{{link.Name}}</a>\r\n      </li>\r\n    </ul>\r\n\r\n    <ul class=\"d-flex float-right\">\r\n      <li *ngFor=\"let link of socialLinks\">\r\n        <a target=\"_blank\" [href]=\"link.Value\">\r\n          <i class=\"fa\" [ngClass]=\"{'fa-facebook' : link.Key === 'Facebook',\r\n        'fa-twitter' : link.Key === 'Twitter',\r\n        'fa-instagram' : link.Key === 'Instagram',\r\n        'fa-youtube-play' : link.Key === 'Youtube'}\" \r\n        aria-hidden=\"true\"></i>\r\n      </a></li>\r\n    </ul>\r\n\r\n    <div class=\"clearfix\"></div>\r\n    <p class=\"text-muted pb-3 w-100\">\r\n      <small [innerHtml]=\"footerLinks.FooterInfo | safe:'html'\">\r\n      </small>\r\n    </p>\r\n\r\n    <p class=\"border-top text-muted text-center p-3\">\r\n      <small>\r\n        Copyright © {{copyRights | date:'yyyy'}} Toshiba Elaraby Visual & Appliances Marketing Company. All Rights\r\n        Reserved.\r\n      </small>\r\n    </p>\r\n  </div>\r\n</footer>\r\n\r\n<!-- Get Content Modal -->\r\n<!-- <div class=\"modal fade\" id=\"getContent\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"getContentLabel\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h3 class=\"modal-title w-100 text-center\" id=\"getContentLabel\">{{modalData.title}}</h3>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p [innerHTML]=\"modalData.content | safe:'html'\"></p>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\r\n        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div> -->\r\n"

/***/ }),

/***/ "./src/app/core/footer/footer.component.scss":
/*!***************************************************!*\
  !*** ./src/app/core/footer/footer.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "footer {\n  overflow: hidden;\n  background-color: #fff; }\n  footer ul li a {\n    color: #000;\n    font-size: 14px;\n    font-weight: bold;\n    margin: 0 20px 0 0;\n    cursor: pointer; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9mb290ZXIvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxjb3JlXFxmb290ZXJcXGZvb3Rlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFnQjtFQUNoQixzQkFBc0IsRUFBQTtFQUZ4QjtJQU1RLFdBQVc7SUFDWCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb3JlL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJmb290ZXIge1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICB1bCB7XHJcbiAgICBsaSB7XHJcbiAgICAgIGEge1xyXG4gICAgICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBtYXJnaW46IDAgMjBweCAwIDA7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/core/footer/footer.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/footer/footer.component.ts ***!
  \*************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/header.service */ "./src/app/services/header.service.ts");



var FooterComponent = /** @class */ (function () {
    function FooterComponent(headerService) {
        this.headerService = headerService;
        this.footerLinks = [];
        this.socialLinks = [];
        this.copyRights = new Date();
    }
    FooterComponent.prototype.ngOnInit = function () {
        this.getFooterLinks();
        this.getSocialLinks();
    };
    FooterComponent.prototype.getFooterLinks = function () {
        var _this = this;
        this.headerService.getFooterLinks().subscribe(function (links) { return _this.footerLinks = links; });
    };
    FooterComponent.prototype.getSocialLinks = function () {
        var _this = this;
        this.headerService.getSocialLinks().subscribe(function (links) { return _this.socialLinks = links.Data; });
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/core/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/core/footer/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/core/header/header.component.html":
/*!***************************************************!*\
  !*** ./src/app/core/header/header.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"p-2\">\r\n  <div class=\"container\">\r\n    <p class=\"float-md-left pt-1 mb-md-0 mb-2\">Choose another country or region to see content specific to your location.</p>\r\n    <app-region-select class=\"float-md-left\"></app-region-select>\r\n    <a routerLink=\"/find-store\" class=\"plain-btn float-right pt-1 mx-2 text-white\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i> Find a store</a>\r\n\r\n    <button routerLink=\"/become-distributor\" class=\"plain-btn float-right pt-1\"><img src=\"assets/images/store-ico.svg\" width=\"13\"> Become a\r\n      distributor</button>\r\n    <div class=\"clearfix\"></div>\r\n  </div>\r\n</header>\r\n<app-main-menu></app-main-menu>\r\n"

/***/ }),

/***/ "./src/app/core/header/header.component.scss":
/*!***************************************************!*\
  !*** ./src/app/core/header/header.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background-color: #444444;\n  color: #FFF;\n  font-size: 12px; }\n\nbutton {\n  color: #FFF;\n  font-size: 12px; }\n\np {\n  font-size: 12px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9oZWFkZXIvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxjb3JlXFxoZWFkZXJcXGhlYWRlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29yZS9oZWFkZXIvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXNzZXRzXFxzY3NzXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHlCQ0h3QjtFREl4QixXQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVqQjtFQUNJLFdBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0UsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29yZS9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xyXG5cclxuaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1iZy1jb2xvcjtcclxuICBjb2xvcjogI0ZGRjtcclxuICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuYnV0dG9ue1xyXG4gICAgY29sb3I6ICNGRkY7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn1cclxucHtcclxuICBmb250LXNpemU6IDEycHg7XHJcbn0iLCIkcHJpbWFyeS1iZy1jb2xvcjogIzQ0NDQ0NDtcclxuJHByaW1hcnktYnRuLWNvbG9yOiAjZWQxYzI0OyJdfQ== */"

/***/ }),

/***/ "./src/app/core/header/header.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/header/header.component.ts ***!
  \*************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/core/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/core/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/core/main-menu/main-menu.component.html":
/*!*********************************************************!*\
  !*** ./src/app/core/main-menu/main-menu.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"border-bottom bg-white\">\r\n  <div class=\"container p-0\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <div class=\"site-name float-left mr-5\">\r\n        <a class=\"navbar-brand\" routerLink=\"/\"><img src=\"assets/images/logo.svg\"></a>\r\n        <span class=\"slogan\">TV products for Middle East and Africa</span>\r\n      </div>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\"\r\n        aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">\r\n        <div class=\"navbar-nav\">\r\n\r\n          <div class=\"dropdown show mx-3\">\r\n            <a class=\"btn btn-default dropdown-toggle font-weight-bold px-0\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\"\r\n              data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n              TVs\r\n            </a>\r\n            <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">\r\n              <a class=\"dropdown-item\" *ngFor=\"let category of categories\" [routerLink]=\"['/products/product-list']\"\r\n                [queryParams]=\"{id:category.ID}\">{{category.Name}}</a>\r\n            </div>\r\n          </div>\r\n          <a class=\"nav-item nav-link mx-3\" routerLink=\"/news\">News</a>\r\n          <a class=\"nav-item nav-link mx-3\" routerLink=\"/support\">Support</a>\r\n        </div>\r\n      </div>\r\n      <app-top-search></app-top-search>\r\n    </nav>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/core/main-menu/main-menu.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/core/main-menu/main-menu.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar-nav > a {\n  font-size: 13px;\n  font-weight: 600;\n  color: #000000;\n  margin: 0;\n  padding-right: 0;\n  padding-left: 0; }\n\n.slogan {\n  font-size: 13px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: normal;\n  letter-spacing: normal;\n  color: #5a5a5a; }\n\n.navbar-nav {\n  justify-content: flex-end;\n  width: 91%; }\n\n.navbar-brand {\n  margin-right: 36px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9tYWluLW1lbnUvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxjb3JlXFxtYWluLW1lbnVcXG1haW4tbWVudS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLFNBQVM7RUFDVCxnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUdqQjtFQUNFLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSx5QkFBeUI7RUFDekIsVUFBVSxFQUFBOztBQUVaO0VBQ0Usa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb3JlL21haW4tbWVudS9tYWluLW1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmF2YmFyLW5hdj5hIHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICBjb2xvcjogIzAwMDAwMDtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZy1yaWdodDogMDtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5zbG9nYW4ge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XHJcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gIGNvbG9yOiAjNWE1YTVhO1xyXG59XHJcblxyXG4ubmF2YmFyLW5hdiB7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICB3aWR0aDogOTElO1xyXG59XHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgbWFyZ2luLXJpZ2h0OiAzNnB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/core/main-menu/main-menu.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/core/main-menu/main-menu.component.ts ***!
  \*******************************************************/
/*! exports provided: MainMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainMenuComponent", function() { return MainMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/header.service */ "./src/app/services/header.service.ts");



var MainMenuComponent = /** @class */ (function () {
    function MainMenuComponent(headerService) {
        this.headerService = headerService;
        this.categories = [];
    }
    MainMenuComponent.prototype.ngOnInit = function () {
        this.getCategories();
    };
    MainMenuComponent.prototype.getCategories = function () {
        var _this = this;
        this.headerService.getCategories().subscribe(function (categories) {
            _this.categories = categories.Data;
        });
    };
    MainMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-menu',
            template: __webpack_require__(/*! ./main-menu.component.html */ "./src/app/core/main-menu/main-menu.component.html"),
            styles: [__webpack_require__(/*! ./main-menu.component.scss */ "./src/app/core/main-menu/main-menu.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"]])
    ], MainMenuComponent);
    return MainMenuComponent;
}());



/***/ }),

/***/ "./src/app/core/page-not-found/page-not-found.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/core/page-not-found/page-not-found.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div id=\"notfound\">\r\n    <div class=\"notfound\">\r\n      <div class=\"notfound-404\">\r\n        <h3>Oops! Page not found</h3>\r\n        <h1><span>4</span><span>0</span><span>4</span></h1>\r\n      </div>\r\n      <h2>we are sorry, but the page you requested was not found</h2>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/core/page-not-found/page-not-found.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/core/page-not-found/page-not-found.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#notfound {\n  position: relative;\n  height: 60vh; }\n\n#notfound .notfound {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n  transform: translate(-50%, -50%); }\n\n.notfound {\n  max-width: 520px;\n  width: 100%;\n  line-height: 1.4;\n  text-align: center; }\n\n.notfound .notfound-404 {\n  position: relative;\n  height: 240px; }\n\n.notfound .notfound-404 h1 {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n  transform: translate(-50%, -50%);\n  font-weight: 900;\n  margin: 0px;\n  color: #262626;\n  text-transform: uppercase;\n  letter-spacing: -40px;\n  margin-left: -20px; }\n\n.notfound .notfound-404 h1 > span {\n  text-shadow: -8px 0px 0px #fff;\n  font-size: 252px; }\n\n.notfound .notfound-404 h3 {\n  font-family: 'Cabin', sans-serif;\n  position: relative;\n  font-size: 16px;\n  font-weight: 700;\n  text-transform: uppercase;\n  color: #262626;\n  margin: 0px;\n  letter-spacing: 3px;\n  padding-left: 6px; }\n\n.notfound h2 {\n  font-family: 'Cabin', sans-serif;\n  font-size: 20px;\n  font-weight: 400;\n  text-transform: uppercase;\n  color: #000;\n  margin-top: 0px;\n  margin-bottom: 25px; }\n\n@media only screen and (max-width: 767px) {\n  .notfound .notfound-404 {\n    height: 200px; }\n  .notfound .notfound-404 h1 {\n    font-size: 200px; } }\n\n@media only screen and (max-width: 480px) {\n  .notfound .notfound-404 {\n    height: 162px; }\n  .notfound .notfound-404 h1 {\n    font-size: 162px;\n    height: 150px;\n    line-height: 162px; }\n  .notfound h2 {\n    font-size: 16px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9wYWdlLW5vdC1mb3VuZC9HOlxcdG9zaGliYVxcZnJvbnRcXHRldmFcXG5nLXRldmEvc3JjXFxhcHBcXGNvcmVcXHBhZ2Utbm90LWZvdW5kXFxwYWdlLW5vdC1mb3VuZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFrQjtFQUNsQixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFFBQVE7RUFDUix3Q0FBd0M7RUFFaEMsZ0NBQWdDLEVBQUE7O0FBRzFDO0VBQ0UsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsUUFBUTtFQUNSLHdDQUF3QztFQUVoQyxnQ0FBZ0M7RUFDeEMsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHFCQUFxQjtFQUNyQixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSw4QkFBOEI7RUFDOUIsZ0JBQWdCLEVBQUE7O0FBSWxCO0VBQ0UsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxnQ0FBZ0M7RUFDaEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIsV0FBVztFQUNYLGVBQWU7RUFDZixtQkFBbUIsRUFBQTs7QUFHckI7RUFDRTtJQUNFLGFBQWEsRUFBQTtFQUVmO0lBQ0UsZ0JBQWdCLEVBQUEsRUFDakI7O0FBR0g7RUFDRTtJQUNFLGFBQWEsRUFBQTtFQUVmO0lBQ0UsZ0JBQWdCO0lBQ2hCLGFBQWE7SUFDYixrQkFBa0IsRUFBQTtFQUVwQjtJQUNFLGVBQWUsRUFBQSxFQUNoQiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvcGFnZS1ub3QtZm91bmQvcGFnZS1ub3QtZm91bmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbm90Zm91bmQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiA2MHZoO1xyXG4gIH1cclxuICBcclxuICAjbm90Zm91bmQgLm5vdGZvdW5kIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gIH1cclxuICBcclxuICAubm90Zm91bmQge1xyXG4gICAgbWF4LXdpZHRoOiA1MjBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAyNDBweDtcclxuICB9XHJcbiAgXHJcbiAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQgaDEge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICBmb250LXdlaWdodDogOTAwO1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBjb2xvcjogIzI2MjYyNjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogLTQwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5ub3Rmb3VuZCAubm90Zm91bmQtNDA0IGgxPnNwYW4ge1xyXG4gICAgdGV4dC1zaGFkb3c6IC04cHggMHB4IDBweCAjZmZmO1xyXG4gICAgZm9udC1zaXplOiAyNTJweDtcclxuXHJcbiAgfVxyXG4gIFxyXG4gIC5ub3Rmb3VuZCAubm90Zm91bmQtNDA0IGgzIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnQ2FiaW4nLCBzYW5zLXNlcmlmO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBjb2xvcjogIzI2MjYyNjtcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDNweDtcclxuICAgIHBhZGRpbmctbGVmdDogNnB4O1xyXG4gIH1cclxuICBcclxuICAubm90Zm91bmQgaDIge1xyXG4gICAgZm9udC1mYW1pbHk6ICdDYWJpbicsIHNhbnMtc2VyaWY7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjVweDtcclxuICB9XHJcbiAgXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gICAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQge1xyXG4gICAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgfVxyXG4gICAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQgaDEge1xyXG4gICAgICBmb250LXNpemU6IDIwMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ4MHB4KSB7XHJcbiAgICAubm90Zm91bmQgLm5vdGZvdW5kLTQwNCB7XHJcbiAgICAgIGhlaWdodDogMTYycHg7XHJcbiAgICB9XHJcbiAgICAubm90Zm91bmQgLm5vdGZvdW5kLTQwNCBoMSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTYycHg7XHJcbiAgICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiAxNjJweDtcclxuICAgIH1cclxuICAgIC5ub3Rmb3VuZCBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/core/page-not-found/page-not-found.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/core/page-not-found/page-not-found.component.ts ***!
  \*****************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/core/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found.component.scss */ "./src/app/core/page-not-found/page-not-found.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/core/region-select/region-select.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/core/region-select/region-select.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"d-flex\">\r\n  <div class=\"mx-2 bg-white px-2 d-flex\">\r\n    <img width=\"11\" src=\"assets/images/change-location.svg\">\r\n    <select class=\"form-control\" [(ngModel)]=\"country\" name=\"country\">\r\n      <option *ngFor=\"let country of countries\" [value]=\"country.ID\">{{country.Name}}</option>\r\n    </select>\r\n  </div>\r\n\r\n  <button (click)=\"setCoutry()\" class=\"primary-btn py-1 px-3\"><app-spinner *ngIf=\"loading\" [type]=\"'inline'\" [theme]=\"'text-default'\"></app-spinner> Continue</button>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/core/region-select/region-select.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/core/region-select/region-select.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "select {\n  height: 25px;\n  padding: 0;\n  border-radius: 0;\n  font-size: 12px;\n  border: 0;\n  min-width: 150px; }\n\nselect:focus {\n  box-shadow: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9yZWdpb24tc2VsZWN0L0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcY29yZVxccmVnaW9uLXNlbGVjdFxccmVnaW9uLXNlbGVjdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQVk7RUFDWixVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixTQUFTO0VBQ1QsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb3JlL3JlZ2lvbi1zZWxlY3QvcmVnaW9uLXNlbGVjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNlbGVjdCB7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgYm9yZGVyLXJhZGl1czogMDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgYm9yZGVyOiAwO1xyXG4gIG1pbi13aWR0aDogMTUwcHg7XHJcbn1cclxuXHJcbnNlbGVjdDpmb2N1cyB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/core/region-select/region-select.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/core/region-select/region-select.component.ts ***!
  \***************************************************************/
/*! exports provided: RegionSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegionSelectComponent", function() { return RegionSelectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/header.service */ "./src/app/services/header.service.ts");




var RegionSelectComponent = /** @class */ (function () {
    function RegionSelectComponent(headerService, platformId) {
        this.headerService = headerService;
        this.platformId = platformId;
        this.countries = [];
        this.country = '1';
    }
    RegionSelectComponent.prototype.ngOnInit = function () {
        this.getCountries();
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            localStorage.setItem('countryID', '1');
        }
    };
    RegionSelectComponent.prototype.getCountries = function () {
        var _this = this;
        this.headerService.getCountries().subscribe(function (countries) {
            _this.countries = countries.Data;
        });
    };
    ;
    RegionSelectComponent.prototype.setCoutry = function () {
        var _this = this;
        this.loading = true;
        localStorage.setItem('countryID', this.country);
        setTimeout(function () {
            _this.loading = false;
        }, 500);
    };
    RegionSelectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-region-select',
            template: __webpack_require__(/*! ./region-select.component.html */ "./src/app/core/region-select/region-select.component.html"),
            styles: [__webpack_require__(/*! ./region-select.component.scss */ "./src/app/core/region-select/region-select.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_header_service__WEBPACK_IMPORTED_MODULE_3__["HeaderService"], Object])
    ], RegionSelectComponent);
    return RegionSelectComponent;
}());



/***/ }),

/***/ "./src/app/core/top-search/top-search.component.html":
/*!***********************************************************!*\
  !*** ./src/app/core/top-search/top-search.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div class=\"d-flex border px-3\">\r\n    <img width=\"11\" src=\"assets/images/search-font-awesome.svg\">\r\n    <input type=\"text\" class=\"form-control border-0\" placeholder=\"Search for Product …\" [(ngModel)]=\"keyword\" name=\"keyword\" (keyup.enter)=\"search()\">\r\n  </div>\r\n"

/***/ }),

/***/ "./src/app/core/top-search/top-search.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/core/top-search/top-search.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "input:focus {\n  outline: 0;\n  border: 0;\n  box-shadow: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS90b3Atc2VhcmNoL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcY29yZVxcdG9wLXNlYXJjaFxcdG9wLXNlYXJjaC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQVU7RUFDVixTQUFTO0VBQ1QsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb3JlL3RvcC1zZWFyY2gvdG9wLXNlYXJjaC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlucHV0OmZvY3VzIHtcclxuICAgIG91dGxpbmU6IDA7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/core/top-search/top-search.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/core/top-search/top-search.component.ts ***!
  \*********************************************************/
/*! exports provided: TopSearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopSearchComponent", function() { return TopSearchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_shared_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/shared.models */ "./src/app/shared/shared.models.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var TopSearchComponent = /** @class */ (function () {
    function TopSearchComponent(router) {
        this.router = router;
        this.keyword = '';
    }
    TopSearchComponent.prototype.ngOnInit = function () { };
    TopSearchComponent.prototype.search = function () {
        if (this.keyword.trim().length > 0) {
            var search = new _shared_shared_models__WEBPACK_IMPORTED_MODULE_2__["SearchModel"]();
            search.Keyword = this.keyword;
            this.router.navigate(['/products'], {
                queryParams: search
            });
            this.keyword = '';
        }
        else {
            this.keyword = '';
        }
    };
    TopSearchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-top-search',
            template: __webpack_require__(/*! ./top-search.component.html */ "./src/app/core/top-search/top-search.component.html"),
            styles: [__webpack_require__(/*! ./top-search.component.scss */ "./src/app/core/top-search/top-search.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], TopSearchComponent);
    return TopSearchComponent;
}());



/***/ }),

/***/ "./src/app/pipes/sanitize.pipe.ts":
/*!****************************************!*\
  !*** ./src/app/pipes/sanitize.pipe.ts ***!
  \****************************************/
/*! exports provided: SafePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafePipe", function() { return SafePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");



var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (value, type) {
        switch (type) {
            case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
            case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
            case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
            case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
            default: throw new Error("Invalid safe type specified: " + type);
        }
    };
    SafePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'safe'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], SafePipe);
    return SafePipe;
}());



/***/ }),

/***/ "./src/app/services/header.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/header.service.ts ***!
  \********************************************/
/*! exports provided: HeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderService", function() { return HeaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api */ "./src/app/api.ts");



// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

var HeaderService = /** @class */ (function () {
    function HeaderService(http, proxy) {
        this.http = http;
        this.proxy = proxy;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
            'Access-Control-Allow-Headers': "*",
            'Access-Control-Allow-Credentials': 'true',
        });
    }
    // get countries
    HeaderService.prototype.getCountries = function () {
        return this.http.get(this.proxy.api + 'Country/GetCountry', { headers: this.headers });
    };
    // get Footer
    HeaderService.prototype.getFooterLinks = function () {
        return this.http.get(this.proxy.api + 'Content/GetStaticLinks', { headers: this.headers });
    };
    // get Static Links Data
    HeaderService.prototype.getStaticLinksData = function (id) {
        return this.http.get(this.proxy.api + 'Content/GetContent?ID=' + id, { headers: this.headers });
    };
    // get categories
    HeaderService.prototype.getCategories = function () {
        return this.http.get(this.proxy.api + 'Category/GetCategory', { headers: this.headers });
    };
    // get stores
    HeaderService.prototype.getStores = function (ip, keyword) {
        if (keyword === void 0) { keyword = ''; }
        return this.http.get(this.proxy.api + 'Store/GetStores?LocalIP=' + ip + '&Keyword=' + keyword, { headers: this.headers });
    };
    HeaderService.prototype.getSocialLinks = function () {
        return this.http.get(this.proxy.api + 'Content/GetSocialAccounts', { headers: this.headers });
    };
    HeaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _api__WEBPACK_IMPORTED_MODULE_3__["ProxyUrl"]])
    ], HeaderService);
    return HeaderService;
}());



/***/ }),

/***/ "./src/app/shared/breadcrumbs/breadcrumbs.component.html":
/*!***************************************************************!*\
  !*** ./src/app/shared/breadcrumbs/breadcrumbs.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"breadcrumbs\" [ngStyle]=\"{'background-image': 'url(' + data.background + ')', 'min-height' : productPage ?  '130px' : '320px',\r\n'background-size' : productPage ?  'cover' : 'auto'}\">\r\n  <div class=\"container\">\r\n    <ul class=\"mt-5\">\r\n      <li *ngFor=\"let path of data.paths\"><a class=\"text-shadow\" [routerLink]=\"path.url\"\r\n          [queryParams]=\"path.params\">{{path.name}}</a></li>\r\n    </ul>\r\n\r\n    <h1 class=\"py-4 text-shadow\" *ngIf=\"data.description\">{{data.description}}</h1>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared/breadcrumbs/breadcrumbs.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/shared/breadcrumbs/breadcrumbs.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumbs {\n  background-position: 0 0;\n  overflow: hidden;\n  background-repeat: no-repeat; }\n  .breadcrumbs ul {\n    margin-top: 20px;\n    float: left; }\n  .breadcrumbs ul li {\n      float: left; }\n  .breadcrumbs ul li:after {\n        content: \"/\";\n        display: inline-block;\n        margin: 0 5px;\n        color: #FFF; }\n  .breadcrumbs ul li:last-child:after {\n        content: \"\"; }\n  .breadcrumbs ul li a {\n        color: #FFF;\n        font-size: 14px; }\n  .breadcrumbs h1 {\n    clear: both;\n    color: #FFF;\n    width: 38%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2JyZWFkY3J1bWJzL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcc2hhcmVkXFxicmVhZGNydW1ic1xcYnJlYWRjcnVtYnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx3QkFBd0I7RUFDeEIsZ0JBQWdCO0VBQ2hCLDRCQUE0QixFQUFBO0VBSDlCO0lBTUksZ0JBQWdCO0lBQ2hCLFdBQVcsRUFBQTtFQVBmO01BVU0sV0FBVyxFQUFBO0VBVmpCO1FBYVEsWUFBWTtRQUNaLHFCQUFxQjtRQUNyQixhQUFhO1FBQ2IsV0FBVyxFQUFBO0VBaEJuQjtRQW9CUSxXQUNGLEVBQUE7RUFyQk47UUF3QlEsV0FBVztRQUNYLGVBQWUsRUFBQTtFQXpCdkI7SUErQkksV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvYnJlYWRjcnVtYnMvYnJlYWRjcnVtYnMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnJlYWRjcnVtYnMge1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IDAgMDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgLy8gYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gIHVsIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuXHJcbiAgICBsaSB7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG5cclxuICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgY29udGVudDogXCIvXCI7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIG1hcmdpbjogMCA1cHg7XHJcbiAgICAgICAgY29sb3I6ICNGRkY7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICY6bGFzdC1jaGlsZDphZnRlciB7XHJcbiAgICAgICAgY29udGVudDogXCJcIlxyXG4gICAgICB9XHJcblxyXG4gICAgICBhIHtcclxuICAgICAgICBjb2xvcjogI0ZGRjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGgxIHtcclxuICAgIGNsZWFyOiBib3RoO1xyXG4gICAgY29sb3I6ICNGRkY7XHJcbiAgICB3aWR0aDogMzglO1xyXG4gIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/shared/breadcrumbs/breadcrumbs.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/breadcrumbs/breadcrumbs.component.ts ***!
  \*************************************************************/
/*! exports provided: BreadcrumbsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BreadcrumbsComponent", function() { return BreadcrumbsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var BreadcrumbsComponent = /** @class */ (function () {
    function BreadcrumbsComponent(route) {
        this.route = route;
    }
    BreadcrumbsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            if (params.catId)
                _this.productPage = true;
            else
                _this.productPage = false;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], BreadcrumbsComponent.prototype, "data", void 0);
    BreadcrumbsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-breadcrumbs',
            template: __webpack_require__(/*! ./breadcrumbs.component.html */ "./src/app/shared/breadcrumbs/breadcrumbs.component.html"),
            styles: [__webpack_require__(/*! ./breadcrumbs.component.scss */ "./src/app/shared/breadcrumbs/breadcrumbs.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], BreadcrumbsComponent);
    return BreadcrumbsComponent;
}());



/***/ }),

/***/ "./src/app/shared/carousel/carousel.component.html":
/*!*********************************************************!*\
  !*** ./src/app/shared/carousel/carousel.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">\r\n  <ol class=\"carousel-indicators\">\r\n    <li data-target=\"#carouselExampleIndicators\" *ngFor=\"let slide of slides; let i = index\"\r\n      [ngClass]=\"{'active' : i == 0}\" [attr.data-slide-to]=\"i\"></li>\r\n  </ol>\r\n  <div class=\"carousel-inner\">\r\n    <div class=\"carousel-item\" *ngFor=\"let slide of slides; let i = index\" [ngClass]=\"{'active' : i == 0}\">\r\n      <img class=\"d-block w-100\" [src]=\"slide.Image\" alt=\"First slide\">\r\n      <div class=\"carousel-caption d-block\">\r\n        <h5 class=\"w-75\">{{slide.Title}}</h5>\r\n        <p>{{slide.SubTitle}}</p>\r\n        <a [href]=\"slide.URL\" target=\"_blank\" class=\"plain-btn text-danger mt-md-4 float-left\">See Details <img width=\"8\"\r\n            src=\"assets/images/arrow-right.svg\"></a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">\r\n    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\r\n    <span class=\"sr-only\">Previous</span>\r\n  </a>\r\n  <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">\r\n    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\r\n    <span class=\"sr-only\">Next</span>\r\n  </a> -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared/carousel/carousel.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/shared/carousel/carousel.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".carousel-caption {\n  top: 19%;\n  left: 10%;\n  right: auto;\n  text-align: left;\n  color: #000; }\n  .carousel-caption h5 {\n    font-size: 48px;\n    font-weight: 600;\n    color: #000000; }\n  .carousel-caption p {\n    font-size: 20px;\n    font-weight: 300;\n    color: #000000; }\n  .carousel-caption a {\n    font-size: 18px;\n    font-weight: 500; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2Nhcm91c2VsL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcc2hhcmVkXFxjYXJvdXNlbFxcY2Fyb3VzZWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxRQUFRO0VBQ1IsU0FBUztFQUNULFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsV0FBVyxFQUFBO0VBTGI7SUFRSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWMsRUFBQTtFQVZsQjtJQWNJLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsY0FBYyxFQUFBO0VBaEJsQjtJQW9CSSxlQUFlO0lBQ2YsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvY2Fyb3VzZWwvY2Fyb3VzZWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2Fyb3VzZWwtY2FwdGlvbiB7XHJcbiAgdG9wOiAxOSU7XHJcbiAgbGVmdDogMTAlO1xyXG4gIHJpZ2h0OiBhdXRvO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgY29sb3I6ICMwMDA7XHJcblxyXG4gIGg1IHtcclxuICAgIGZvbnQtc2l6ZTogNDhweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIHAge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuXHJcbiAgYSB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/shared/carousel/carousel.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/carousel/carousel.component.ts ***!
  \*******************************************************/
/*! exports provided: CarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselComponent", function() { return CarouselComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CarouselComponent = /** @class */ (function () {
    function CarouselComponent() {
    }
    CarouselComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], CarouselComponent.prototype, "slides", void 0);
    CarouselComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-carousel',
            template: __webpack_require__(/*! ./carousel.component.html */ "./src/app/shared/carousel/carousel.component.html"),
            styles: [__webpack_require__(/*! ./carousel.component.scss */ "./src/app/shared/carousel/carousel.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CarouselComponent);
    return CarouselComponent;
}());



/***/ }),

/***/ "./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"panel-group mb-5\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">\r\n  <div *ngFor=\"let panel of panels;let i = index\" class=\"panel panel-default\">\r\n    <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">\r\n      <h4 class=\"panel-title\">\r\n        <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\" [href]=\"'#collapse' + i\" [attr.aria-expanded]=\"true ? i == 0 : false\"\r\n          [attr.aria-controls]=\"'collapse'+i\">\r\n          {{panel.GroupName}}\r\n        </a>\r\n      </h4>\r\n    </div>\r\n    <div [id]=\"'collapse' + i\" class=\"panel-collapse collapse\" [ngClass]=\"{'in show': i == 0}\" role=\"tabpanel\"\r\n      [attr.aria-labelledby]=\"'heading'+i\">\r\n      <div class=\"panel-body\">\r\n        <ul class=\"mb-3\">\r\n          <li *ngFor=\"let item of panel._lstAttributes\">\r\n            <label>{{item.AttributeName}}</label>\r\n            <span>{{item.Option}}</span>\r\n            <img class=\"img-fluid\" *ngIf=\"item.type == 'image'\" [src]=\"item.src\">\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-default > .panel-heading {\n  color: #333;\n  background-color: #fff;\n  border-color: #e4e5e7;\n  padding: 0;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n.panel-default > .panel-heading a {\n  display: block;\n  padding: 0;\n  font-size: 16px;\n  font-weight: 600;\n  color: #000000; }\n\n.panel-default > .panel-heading a:before {\n  content: \"\";\n  position: relative;\n  top: 1px;\n  display: inline-block;\n  font-family: 'Glyphicons Halflings';\n  font-style: normal;\n  font-weight: bold;\n  color: red;\n  margin-right: 10px;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  float: left;\n  transition: -webkit-transform .25s linear;\n  transition: transform .25s linear;\n  transition: transform .25s linear, -webkit-transform .25s linear;\n  -webkit-transition: -webkit-transform .25s linear;\n  width: 17px;\n  height: 17px;\n  border: 2px solid red;\n  border-radius: 100%;\n  line-height: 13px;\n  text-align: center;\n  font-size: 15px; }\n\n.panel-default > .panel-heading a[aria-expanded=\"true\"] {\n  background-color: #fff; }\n\n.panel-default > .panel-heading a[aria-expanded=\"true\"]:before {\n  content: \"\\2212\";\n  -webkit-transform: rotate(180deg);\n  transform: rotate(180deg); }\n\n.panel-default > .panel-heading a[aria-expanded=\"false\"]:before {\n  content: \"\\002b\";\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg); }\n\n.accordion-option {\n  width: 100%;\n  float: left;\n  clear: both;\n  margin: 15px 0; }\n\n.accordion-option .title {\n  font-size: 20px;\n  font-weight: bold;\n  float: left;\n  padding: 0;\n  margin: 0; }\n\n.accordion-option .toggle-accordion {\n  float: right;\n  font-size: 16px;\n  color: #6a6c6f; }\n\n.panel-body {\n  padding: 10px 28px; }\n\n.panel-body ul li {\n    font-size: 14px;\n    font-weight: 500;\n    color: #838a93; }\n\n.panel-body ul li label {\n      width: 40%;\n      font-weight: bold; }\n\n@media (max-width: 500px) {\n      .panel-body ul li label {\n        width: 50%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtY29sbGFwc2libGUtcGFuZWwvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxzaGFyZWRcXHByb2R1Y3QtY29sbGFwc2libGUtcGFuZWxcXHByb2R1Y3QtY29sbGFwc2libGUtcGFuZWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUU7RUFDRSxXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixVQUFVO0VBQ1YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsY0FBYztFQUNkLFVBQVU7RUFDVixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixxQkFBcUI7RUFDckIsbUNBQW1DO0VBQ25DLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixtQ0FBbUM7RUFDbkMsa0NBQWtDO0VBQ2xDLFdBQVc7RUFDWCx5Q0FBeUM7RUFDekMsaUNBQWlDO0VBQ2pDLGdFQUFnRTtFQUNoRSxpREFBaUQ7RUFDakQsV0FBVztFQUNYLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQUdqQjtFQUNFLHNCQUFzQixFQUFBOztBQUd4QjtFQUNFLGdCQUFnQjtFQUNoQixpQ0FBaUM7RUFDakMseUJBQXlCLEVBQUE7O0FBRzNCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdDQUFnQztFQUNoQyx3QkFBd0IsRUFBQTs7QUFHMUI7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCxjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBVTtFQUNWLFNBQVMsRUFBQTs7QUFHWDtFQUNFLFlBQVk7RUFDWixlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQUVoQjtFQUNFLGtCQUFrQixFQUFBOztBQURwQjtJQUlNLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsY0FBYyxFQUFBOztBQU5wQjtNQVFRLFVBQVU7TUFDVixpQkFBaUIsRUFBQTs7QUFFbkI7TUFYTjtRQWFVLFVBQVUsRUFBQSxFQUNYIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtY29sbGFwc2libGUtcGFuZWwvcHJvZHVjdC1jb2xsYXBzaWJsZS1wYW5lbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgLnBhbmVsLWRlZmF1bHQ+LnBhbmVsLWhlYWRpbmcge1xyXG4gICAgY29sb3I6ICMzMzM7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZTRlNWU3O1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIC5wYW5lbC1kZWZhdWx0Pi5wYW5lbC1oZWFkaW5nIGEge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuICBcclxuICAucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAxcHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LWZhbWlseTogJ0dseXBoaWNvbnMgSGFsZmxpbmdzJztcclxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xyXG4gICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjI1cyBsaW5lYXI7XHJcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjI1cyBsaW5lYXI7XHJcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjI1cyBsaW5lYXIsIC13ZWJraXQtdHJhbnNmb3JtIC4yNXMgbGluZWFyO1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAuMjVzIGxpbmVhcjtcclxuICAgIHdpZHRoOiAxN3B4O1xyXG4gICAgaGVpZ2h0OiAxN3B4O1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgcmVkO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgIGxpbmUtaGVpZ2h0OiAxM3B4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gIH1cclxuICBcclxuICAucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhW2FyaWEtZXhwYW5kZWQ9XCJ0cnVlXCJdIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gIFxyXG4gIC5wYW5lbC1kZWZhdWx0Pi5wYW5lbC1oZWFkaW5nIGFbYXJpYS1leHBhbmRlZD1cInRydWVcIl06YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6IFwiXFwyMjEyXCI7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG4gIH1cclxuICBcclxuICAucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyBhW2FyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiXTpiZWZvcmUge1xyXG4gICAgY29udGVudDogXCJcXDAwMmJcIjtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xyXG4gIH1cclxuICBcclxuICAuYWNjb3JkaW9uLW9wdGlvbiB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICB9XHJcbiAgXHJcbiAgLmFjY29yZGlvbi1vcHRpb24gLnRpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuICBcclxuICAuYWNjb3JkaW9uLW9wdGlvbiAudG9nZ2xlLWFjY29yZGlvbiB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBjb2xvcjogIzZhNmM2ZjtcclxuICB9XHJcbiAgLnBhbmVsLWJvZHl7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDI4cHg7XHJcbiAgICB1bHtcclxuICAgICAgbGl7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgICAgY29sb3I6ICM4MzhhOTM7XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICB3aWR0aDogNDAlO1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIEBtZWRpYShtYXgtd2lkdGg6NTAwcHgpe1xyXG4gICAgICAgICAgbGFiZWwge1xyXG4gICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ProductCollapsiblePanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCollapsiblePanelComponent", function() { return ProductCollapsiblePanelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ProductCollapsiblePanelComponent = /** @class */ (function () {
    function ProductCollapsiblePanelComponent() {
        this.panels = [];
    }
    ProductCollapsiblePanelComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProductCollapsiblePanelComponent.prototype, "panels", void 0);
    ProductCollapsiblePanelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-collapsible-panel',
            template: __webpack_require__(/*! ./product-collapsible-panel.component.html */ "./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.html"),
            styles: [__webpack_require__(/*! ./product-collapsible-panel.component.scss */ "./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProductCollapsiblePanelComponent);
    return ProductCollapsiblePanelComponent;
}());



/***/ }),

/***/ "./src/app/shared/shared.models.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.models.ts ***!
  \*****************************************/
/*! exports provided: SearchModel, EmailModel, ModalData, ComparedProduct */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchModel", function() { return SearchModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailModel", function() { return EmailModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalData", function() { return ModalData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComparedProduct", function() { return ComparedProduct; });
var SearchModel = /** @class */ (function () {
    function SearchModel() {
        this.Keyword = '';
    }
    return SearchModel;
}());

var EmailModel = /** @class */ (function () {
    function EmailModel() {
    }
    return EmailModel;
}());

var ModalData = /** @class */ (function () {
    function ModalData() {
        this.title = '';
        this.content = '';
    }
    return ModalData;
}());

var ComparedProduct = /** @class */ (function () {
    function ComparedProduct() {
        this.ScreenSize = '';
        this.Resolution = '';
        this.PanelType = '';
        this.Brightness = '';
        this.SmartTV = '';
        this.ProcessType = '';
        this.TotalPictureQuality = '';
        this.WideColorGamut = '';
        this.HDR = '';
        this.DolbyVisionHDR = '';
        this.MEMC = '';
        this.UHDUpscaler = '';
        this.PictureModes = '';
    }
    return ComparedProduct;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./carousel/carousel.component */ "./src/app/shared/carousel/carousel.component.ts");
/* harmony import */ var _widget_article_widget_article_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./widget-article/widget-article.component */ "./src/app/shared/widget-article/widget-article.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _breadcrumbs_breadcrumbs_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./breadcrumbs/breadcrumbs.component */ "./src/app/shared/breadcrumbs/breadcrumbs.component.ts");
/* harmony import */ var _product_collapsible_panel_product_collapsible_panel_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./product-collapsible-panel/product-collapsible-panel.component */ "./src/app/shared/product-collapsible-panel/product-collapsible-panel.component.ts");
/* harmony import */ var _spinner_spinner_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./spinner/spinner.component */ "./src/app/shared/spinner/spinner.component.ts");
/* harmony import */ var _pipes_sanitize_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../pipes/sanitize.pipe */ "./src/app/pipes/sanitize.pipe.ts");
/* harmony import */ var jw_angular_social_buttons__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! jw-angular-social-buttons */ "./node_modules/jw-angular-social-buttons/lib/jw-angular-social-buttons.module.js");
/* harmony import */ var jw_angular_social_buttons__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(jw_angular_social_buttons__WEBPACK_IMPORTED_MODULE_10__);











var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__["CarouselComponent"], _widget_article_widget_article_component__WEBPACK_IMPORTED_MODULE_4__["WidgetArticleComponent"], _breadcrumbs_breadcrumbs_component__WEBPACK_IMPORTED_MODULE_6__["BreadcrumbsComponent"], _product_collapsible_panel_product_collapsible_panel_component__WEBPACK_IMPORTED_MODULE_7__["ProductCollapsiblePanelComponent"], _spinner_spinner_component__WEBPACK_IMPORTED_MODULE_8__["SpinnerComponent"], _pipes_sanitize_pipe__WEBPACK_IMPORTED_MODULE_9__["SafePipe"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"],
                jw_angular_social_buttons__WEBPACK_IMPORTED_MODULE_10__["JwSocialButtonsModule"]
            ],
            exports: [
                _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__["CarouselComponent"],
                _widget_article_widget_article_component__WEBPACK_IMPORTED_MODULE_4__["WidgetArticleComponent"],
                _breadcrumbs_breadcrumbs_component__WEBPACK_IMPORTED_MODULE_6__["BreadcrumbsComponent"],
                _product_collapsible_panel_product_collapsible_panel_component__WEBPACK_IMPORTED_MODULE_7__["ProductCollapsiblePanelComponent"],
                _spinner_spinner_component__WEBPACK_IMPORTED_MODULE_8__["SpinnerComponent"],
                _pipes_sanitize_pipe__WEBPACK_IMPORTED_MODULE_9__["SafePipe"],
                jw_angular_social_buttons__WEBPACK_IMPORTED_MODULE_10__["JwSocialButtonsModule"]
            ],
            providers: [_pipes_sanitize_pipe__WEBPACK_IMPORTED_MODULE_9__["SafePipe"]]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared/spinner/spinner.component.html":
/*!*******************************************************!*\
  !*** ./src/app/shared/spinner/spinner.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"type === 'default'\" class=\"spinner-container my-5\" [ngClass]=\"{'fixed': fixedSpinner}\">\r\n  <div class=\"spinner-grow\" [ngClass]=\"theme\" role=\"status\"></div>\r\n</div>\r\n\r\n<span *ngIf=\"type === 'inline'\" class=\"spinner-grow spinner-grow-sm\" [ngClass]=\"theme\" role=\"status\" aria-hidden=\"true\"></span>"

/***/ }),

/***/ "./src/app/shared/spinner/spinner.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/shared/spinner/spinner.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fixed {\n  position: fixed;\n  top: 25%;\n  left: 40%;\n  right: 40%;\n  z-index: 999; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3NwaW5uZXIvRzpcXHRvc2hpYmFcXGZyb250XFx0ZXZhXFxuZy10ZXZhL3NyY1xcYXBwXFxzaGFyZWRcXHNwaW5uZXJcXHNwaW5uZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFlO0VBQ2YsUUFBUTtFQUNSLFNBQVM7RUFDVCxVQUFVO0VBQ1YsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL3NwaW5uZXIvc3Bpbm5lci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5maXhlZCB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRvcDogMjUlO1xyXG4gIGxlZnQ6IDQwJTtcclxuICByaWdodDogNDAlO1xyXG4gIHotaW5kZXg6IDk5OTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/shared/spinner/spinner.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/spinner/spinner.component.ts ***!
  \*****************************************************/
/*! exports provided: SpinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpinnerComponent", function() { return SpinnerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SpinnerComponent = /** @class */ (function () {
    function SpinnerComponent() {
        this.fixedSpinner = false;
        this.theme = 'text-danger';
        this.type = 'default';
    }
    SpinnerComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], SpinnerComponent.prototype, "fixedSpinner", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SpinnerComponent.prototype, "theme", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SpinnerComponent.prototype, "type", void 0);
    SpinnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-spinner',
            template: __webpack_require__(/*! ./spinner.component.html */ "./src/app/shared/spinner/spinner.component.html"),
            styles: [__webpack_require__(/*! ./spinner.component.scss */ "./src/app/shared/spinner/spinner.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SpinnerComponent);
    return SpinnerComponent;
}());



/***/ }),

/***/ "./src/app/shared/widget-article/widget-article.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/shared/widget-article/widget-article.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"widget px-5\" [ngStyle]=\"{'background-image': 'url(' + data?.image +')', 'color': themeColor}\"\r\n  [ngClass]=\"style\">\r\n  <div *ngIf=\"data?.logo\" class=\"logo-container\">\r\n    <img [src]=\"data?.logo\">\r\n  </div>\r\n  <h1>{{data?.title}}</h1>\r\n  <h4>{{data?.subTitle}}</h4>\r\n  <p>{{data?.description}}</p>\r\n  <a *ngIf=\"data?.url && data?.urlType !== 'external'\" class=\"mt-3 text-white\" [routerLink]=\"data?.url\">{{data?.urlTxt}} <img width=\"8\"\r\n      class=\"mx-2\" src=\"assets/images/white-arrow.svg\"></a>\r\n  <a *ngIf=\"data?.url && data?.urlType === 'external'\" class=\"mt-3\" [href]=\"data?.url\" target=\"_blank\">{{data?.urlTxt}} <img\r\n      width=\"8\" class=\"mx-2\" src=\"assets/images/arrow-right.svg\"></a>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared/widget-article/widget-article.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/shared/widget-article/widget-article.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".widget {\n  background-size: cover;\n  min-height: 20rem;\n  background-repeat: no-repeat;\n  background-position: 0 0;\n  display: flex;\n  flex-direction: column;\n  text-align: center;\n  padding-top: 2rem; }\n  .widget a {\n    font-size: 18px;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: normal;\n    letter-spacing: normal;\n    color: #ed1c24;\n    font-weight: bold; }\n  .widget a img {\n      display: inline-block; }\n  .widget.styleVertical {\n  min-height: 570px; }\n  .widget.styleHorizontal {\n  color: #FFF;\n  justify-content: center; }\n  .widget.styleXtrue {\n  color: #FFF;\n  height: 600px;\n  text-align: left;\n  align-items: flex-start;\n  justify-content: center !important; }\n  .widget.styleXtrue p {\n    width: 30%; }\n  .widget.styleXtrue .logo-container img {\n    margin-bottom: 10px;\n    max-width: 215px; }\n  .widget.styleXtrue h1 {\n    text-align: left;\n    width: 30%; }\n  .widget.styleXtrue h2 {\n    text-align: left;\n    width: 30%; }\n  .widget.styleXtrue .logo-container {\n    text-align: left;\n    width: 30%; }\n  .widget.styleXfalse {\n  color: #FFF;\n  height: 600px;\n  text-align: left;\n  align-items: flex-end;\n  justify-content: center !important; }\n  .widget.styleXfalse p {\n    width: 30%; }\n  .widget.styleXfalse .logo-container img {\n    max-width: 215px;\n    margin-bottom: 10px; }\n  .widget.styleXfalse h1 {\n    text-align: left;\n    width: 30%; }\n  .widget.styleXfalse h4 {\n    text-align: left;\n    width: 30%; }\n  .widget.styleXfalse .logo-container {\n    text-align: left;\n    width: 30%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3dpZGdldC1hcnRpY2xlL0c6XFx0b3NoaWJhXFxmcm9udFxcdGV2YVxcbmctdGV2YS9zcmNcXGFwcFxcc2hhcmVkXFx3aWRnZXQtYXJ0aWNsZVxcd2lkZ2V0LWFydGljbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLDRCQUE0QjtFQUM1Qix3QkFBd0I7RUFDeEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixrQkFBa0I7RUFDbEIsaUJBQWlCLEVBQUE7RUFSbkI7SUFXSSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLGNBQWM7SUFDZCxpQkFBaUIsRUFBQTtFQWpCckI7TUFvQk0scUJBQXFCLEVBQUE7RUFLM0I7RUFDRSxpQkFBaUIsRUFBQTtFQUduQjtFQUNFLFdBQVc7RUFDWCx1QkFBdUIsRUFBQTtFQUd6QjtFQUNFLFdBQVc7RUFDWCxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixrQ0FBa0MsRUFBQTtFQUxwQztJQVFJLFVBQVUsRUFBQTtFQVJkO0lBWUksbUJBQW1CO0lBQ25CLGdCQUFnQixFQUFBO0VBYnBCO0lBaUJJLGdCQUFnQjtJQUNoQixVQUFVLEVBQUE7RUFsQmQ7SUFxQkksZ0JBQWdCO0lBQ2hCLFVBQVUsRUFBQTtFQXRCZDtJQXlCSSxnQkFBZ0I7SUFDaEIsVUFBVSxFQUFBO0VBSWQ7RUFDRSxXQUFXO0VBQ1gsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsa0NBQWtDLEVBQUE7RUFMcEM7SUFRSSxVQUFVLEVBQUE7RUFSZDtJQVlJLGdCQUFnQjtJQUNoQixtQkFBbUIsRUFBQTtFQWJ2QjtJQWlCSSxnQkFBZ0I7SUFDaEIsVUFBVSxFQUFBO0VBbEJkO0lBcUJJLGdCQUFnQjtJQUNoQixVQUFVLEVBQUE7RUF0QmQ7SUF5QkksZ0JBQWdCO0lBQ2hCLFVBQVUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC93aWRnZXQtYXJ0aWNsZS93aWRnZXQtYXJ0aWNsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53aWRnZXQge1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgbWluLWhlaWdodDogMjByZW07XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLXRvcDogMnJlbTtcclxuXHJcbiAgYSB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcclxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgY29sb3I6ICNlZDFjMjQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuXHJcbiAgICBpbWcge1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4ud2lkZ2V0LnN0eWxlVmVydGljYWwge1xyXG4gIG1pbi1oZWlnaHQ6IDU3MHB4O1xyXG59XHJcblxyXG4ud2lkZ2V0LnN0eWxlSG9yaXpvbnRhbCB7XHJcbiAgY29sb3I6ICNGRkY7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi53aWRnZXQuc3R5bGVYdHJ1ZSB7XHJcbiAgY29sb3I6ICNGRkY7XHJcbiAgaGVpZ2h0OiA2MDBweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcblxyXG4gIHAge1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcblxyXG4gIC5sb2dvLWNvbnRhaW5lciBpbWcge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIG1heC13aWR0aDogMjE1cHg7XHJcbiAgfVxyXG5cclxuICBoMSB7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbiAgaDIge1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIHdpZHRoOiAzMCU7XHJcbiAgfVxyXG4gIC5sb2dvLWNvbnRhaW5lciB7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbn1cclxuXHJcbi53aWRnZXQuc3R5bGVYZmFsc2Uge1xyXG4gIGNvbG9yOiAjRkZGO1xyXG4gIGhlaWdodDogNjAwcHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcclxuXHJcbiAgcCB7XHJcbiAgICB3aWR0aDogMzAlO1xyXG4gIH1cclxuXHJcbiAgLmxvZ28tY29udGFpbmVyIGltZyB7XHJcbiAgICBtYXgtd2lkdGg6IDIxNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICB9XHJcblxyXG4gIGgxIHtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICB3aWR0aDogMzAlO1xyXG4gIH1cclxuICBoNCB7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbiAgLmxvZ28tY29udGFpbmVyIHtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICB3aWR0aDogMzAlO1xyXG4gIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/shared/widget-article/widget-article.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/widget-article/widget-article.component.ts ***!
  \*******************************************************************/
/*! exports provided: WidgetArticleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetArticleComponent", function() { return WidgetArticleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WidgetArticleComponent = /** @class */ (function () {
    function WidgetArticleComponent() {
        this.themeColor = '';
    }
    WidgetArticleComponent.prototype.ngOnInit = function () { };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], WidgetArticleComponent.prototype, "style", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WidgetArticleComponent.prototype, "data", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], WidgetArticleComponent.prototype, "themeColor", void 0);
    WidgetArticleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-widget-article',
            template: __webpack_require__(/*! ./widget-article.component.html */ "./src/app/shared/widget-article/widget-article.component.html"),
            styles: [__webpack_require__(/*! ./widget-article.component.scss */ "./src/app/shared/widget-article/widget-article.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WidgetArticleComponent);
    return WidgetArticleComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
document.addEventListener('DOMContentLoaded', function () {
    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
        .catch(function (err) { return console.error(err); });
});


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! G:\toshiba\front\teva\ng-teva\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map
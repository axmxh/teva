# Teva

To run the app, please follow the instructions:

1. git clone the app.
2. install latest angular cli "npm install -g @angular/cli"
3. navigate to the root of the folder then run "npm install"
4. once it finished you can run the project using "ng serve --proxy-config proxy.conf.json"
5. navigate to "http://localhost:4200" and enjoy!


